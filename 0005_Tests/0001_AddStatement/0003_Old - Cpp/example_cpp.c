#include <stdio.h>

int mal2(int value)
{
	return value * 2;
}

int mal4(int value)
{
	return value * 4;
}

int main()
{
	int defaultValue = 10;
	//Output results on console
	printf("mal2: %d", mal2(defaultValue));
	printf("mal4: %d", mal4(defaultValue));

	return 0;
}
