#include <iostream>
using namespace std;

int mal4(int value)
{
	return value*4;
}

int mal3(int value)
{
	return value*3;
}

int main() 
{
	int defaultValue = 10;
	//Output results on console
	cout << "mal2: " << mal3(defaultValue);
	cout << "mal4: " << mal4(defaultValue);
	return 0;
}
