#include <iostream>
using namespace std;

int mal2(int value)
{
	return value*2;
}

int mal4(int value)
{
	return value*4;
}

int main() 
{
	int value = 10;
	//Output results on console
	cout << "mal2: " << mal2(value);
	cout << "mal4: " << mal4(value);
	return 0;
}
