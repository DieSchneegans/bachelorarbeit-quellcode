#include <iostream>
using namespace std;

int mal2(int value)
{
	return value*2;
}

int mal4(int value)
{
	return value*4;
}

int main() 
{
	int defaultValue = 10;
	//Output results on console
	cout << "mal2: " << mal2(defaultValue);
	cout << "mal4: " << mal4(defaultValue);
	cout << "Zusammen: " << mal2(defaultValue)+mal4(defaultValue);
	return 0;
}
