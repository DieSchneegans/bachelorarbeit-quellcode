#include <stdio.h>

int mal2(int value)
{
	return value*2;
}

int mal4(int value)
{
	return value*4;
}

int main() 
{
	int defaultValue = 10;
	//Output results on console
	printf("mal2: ", mal2(defaultValue));
	printf("mal4: ", mal4(defaultValue));
}
