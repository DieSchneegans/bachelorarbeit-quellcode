#include <stdio.h>

int mal2(int value)
{
	return value*2;
}

int mal4(int value)
{
	return value*4;
}

int main() 
{
	int value = 10;
	//Output results on console
	printf("mal2: ", mal2(value));
	printf("mal4: ", mal4(value));
}
