#include <stdio.h>

int mal2(int value)
{
	return value * 2;
}

int mal4(int value)
{
	return value * 4;
}

int main()
{
	int value = 10;
	//Output results on console
	printf("mal2: %d", mal2(value));
	printf("mal4: %d", mal4(value));

	return 0;
}
