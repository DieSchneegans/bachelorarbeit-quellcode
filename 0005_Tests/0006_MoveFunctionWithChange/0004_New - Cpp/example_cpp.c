#include <stdio.h>

int mal4(int value)
{
	return value * 4;
}

int mal3(int value)
{
	return value * 3;
}

int main()
{
	int defaultValue = 10;
	//Output results on console
	printf("mal3: %d", mal3(defaultValue));
	printf("mal4: %d", mal4(defaultValue));

	return 0;
}
