"""
    Unit-Testcase Match
"""

###################################################################################################
###                                imports                                                      ###
###################################################################################################
import sys

try:
    from CalculationModule.Match import Match
except ModuleNotFoundError:
    print("CalculationModule.Match not installed. Exiting...")
    sys.exit(4)
    
###################################################################################################
###                                constant values                                              ###
###################################################################################################

###################################################################################################
###                                Testcase 18                                                  ###
###################################################################################################
def testcase18(start_old, start_new, length):
    m = Match(start_old, start_new, length)
    
    if m.start == [start_old, start_new]:
        print("PASS testcase 18")
        return 1
    else:
        print("FAIL testcase 18")
        return 0

###################################################################################################
###                                Testcase 19                                                  ###
###################################################################################################
def testcase19(start_old, start_new, length):
    m = Match(start_old, start_new, length)
    
    if m.length == length:
        print("PASS testcase 19")
        return 1
    else:
        print("FAIL testcase 19")
        return 0

###################################################################################################
###                                Testcase 20                                                  ###
###################################################################################################
def testcase20(start_old, start_new, length):
    m = Match(start_old, start_new, length)
    
    if m.__str__() == "({},{},{})".format(start_old, start_new, length):
        print("PASS testcase 20")
        return 1
    else:
        print("FAIL testcase 20")
        return 0

###################################################################################################
###                                Testcase 21                                                  ###
###################################################################################################
def testcase21(start_old, start_new, length):
    m1 = Match(start_old, start_new, length)
    m2 = Match(start_old, start_new, length)
    
    if m1 == m2:
        print("PASS testcase 21")
        return 1
    else:
        print("FAIL testcase 21")
        return 0

###################################################################################################
###                                Testcase 22                                                  ###
###################################################################################################
def testcase22(start_old, start_new, length):
    m = Match(start_old, start_new, length)
    
    if m.end(0) == start_old + length and m.end(1) == start_new + length:
        print("PASS testcase 22")
        return 1
    else:
        print("FAIL testcase 22")
        return 0

###################################################################################################
###                                Testcase 23                                                  ###
###################################################################################################
def testcase23(start_old, start_new, length):
    m = Match(start_old, start_new, length)
    m.reduceBy(Match(start_old, start_new, max(1, length-5)))
    
    if m.length < length:
        print("PASS testcase 23")
        return 1
    else:
        print("FAIL testcase 23")
        return 0

###################################################################################################
###                                Testcase 24                                                  ###
###################################################################################################
def testcase24(start_old, start_new, length):
    m = Match(start_old, start_new, length)
    m.reduceBy(Match(max(start_old+1, start_old+length-2), max(start_new+1, start_new+length-2), length))
    
    if m.length < length:
        print("PASS testcase 24")
        return 1
    else:
        print("FAIL testcase 24")
        return 0
        
###################################################################################################
###                                Testcase 25                                                  ###
###################################################################################################
def testcase25(start_old, start_new, length):
    m = Match(start_old, start_new, length)
    m.reduceBy(Match(start_old, start_new, length))
    
    if m.length == 0:
        print("PASS testcase 25")
        return 1
    else:
        print("FAIL testcase 25")
        return 0
