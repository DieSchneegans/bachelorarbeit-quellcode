"""
    Unit-Testcase Sequence
"""

###################################################################################################
###                                imports                                                      ###
###################################################################################################
import sys

try:
    from CalculationModule.Sequence import Sequence
except ModuleNotFoundError:
    print("CalculationModule.Sequence not installed. Exiting...")
    sys.exit(4)

###################################################################################################
###                                constant values                                              ###
###################################################################################################

###################################################################################################
###                                Testcase 26                                                  ###
###################################################################################################
def testcase26(start, length):
    seq = Sequence(start, length)
    
    if seq.length == length and seq.start == start:
        print("PASS testcase 26")
        return 1
    else:
        print("FAIL testcase 26")
        return 0

###################################################################################################
###                                Testcase 27                                                  ###
###################################################################################################
def testcase27(start, length):
    seq = Sequence(start, length)
    
    if seq.__str__() == "[{},{}[ l={}".format(start, start+length, length):
        print("PASS testcase 27")
        return 1
    else:
        print("FAIL testcase 27")
        return 0

###################################################################################################
###                                Testcase 28                                                  ###
###################################################################################################
def testcase28(start, length):
    seq1 = Sequence(start, length)
    seq2 = Sequence(start, length)
    if seq1 == seq2:
        print("PASS testcase 28")
        return 1
    else:
        print("FAIL testcase 28")
        return 0

###################################################################################################
###                                Testcase 29                                                  ###
###################################################################################################
def testcase29(start, length):
    seq = Sequence(start, length)
    
    if seq.end() == start+length:
        print("PASS testcase 29")
        return 1
    else:
        print("FAIL testcase 29")
        return 0

###################################################################################################
###                                Testcase 30                                                  ###
###################################################################################################
def testcase30(start, length):
    seq = Sequence(start, length)
    seq.subtract(Sequence(start, max(1,length-3)))
    
    if seq.length < length:
        print("PASS testcase 30")
        return 1
    else:
        print("FAIL testcase 30")
        return 0

###################################################################################################
###                                Testcase 31                                                  ###
###################################################################################################
def testcase31(start, length):
    seq = Sequence(start, length)
    seq.subtract(Sequence(max(start+1, start+length-2), length))
    
    if seq.length < length:
        print("PASS testcase 31")
        return 1
    else:
        print("FAIL testcase 31")
        return 0

###################################################################################################
###                                Testcase 32                                                  ###
###################################################################################################
def testcase32(start, length):
    seq = Sequence(start, length)
    seq.subtract(Sequence(start, length))
    
    if seq.length == 0:
        print("PASS testcase 32")
        return 1
    else:
        print("FAIL testcase 32")
        return 0

###################################################################################################
###                                Testcase 33                                                  ###
###################################################################################################
def testcase33(start, length):
    seq = Sequence(start, length)
    seq.subtract(Sequence(start+length, 3))
    
    if seq.length == length:
        print("PASS testcase 33")
        return 1
    else:
        print("FAIL testcase 33")
        return 0

###################################################################################################
###                                Testcase 34                                                  ###
###################################################################################################
def testcase34(start, length):
    seq = Sequence(start, length)
    seq.subtract(Sequence(start-3, 2))
    
    if seq.length == length:
        print("PASS testcase 33")
        return 1
    else:
        print("FAIL testcase 33")
        return 0
