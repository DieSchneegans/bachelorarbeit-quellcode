"""
    Unit-Testcase SourceFile
"""

###################################################################################################
###                                imports                                                      ###
###################################################################################################
import sys

try:
    from sctokenizer.token import TokenType
except ModuleNotFoundError:
    print("sctokenizer.token not installed. Exiting...")
    sys.exit(4)

try:
    from FileModule.SourceFile import SourceFile
except ModuleNotFoundError:
    print("FileModule.SourceFile not installed. Exiting...")
    sys.exit(4)
    
###################################################################################################
###                                Testcase 01                                                  ###
###################################################################################################
def testcase01(filePath):
    sf = SourceFile(filePath)
    
    if sf._SourceFile__filePath == filePath:
        print("PASS testcase 01")
        return 1
    else:
        print("FAIL testcase 01")
        return 0


###################################################################################################
###                                Testcase 02                                                  ###
###################################################################################################
def testcase02(filePath, fileText):
    sf = SourceFile(filePath)
    filetext = sf.readFile()
    
    if filetext == fileText:
        print("PASS testcase 02")
        return 1
    else:
        print("FAIL testcase 02")
        return 0

###################################################################################################
###                                Testcase 03                                                  ###
###################################################################################################
def testcase03(filePath, fileText):
    sf = SourceFile(filePath)
    sf._SourceFile__fileText = fileText
    newFileText = fileText + "AffenSindCool"
    
    if sf.readFile() != newFileText:
        print("PASS testcase 03")
        return 1
    else:
        print("FAIL testcase 03")
        return 0
    

###################################################################################################
###                                Testcase 04                                                  ###
###################################################################################################
def testcase04(filePath):
    sf = SourceFile(filePath)
    sf_name = sf.getFileName()
    name = filePath.name
    
    if name == sf_name:
        print("PASS testcase 04")
        return 1
    else:
        print("FAIL testcase 04")
        return 0

###################################################################################################
###                                Testcase 05                                                  ###
###################################################################################################
def testcase05(filePath):
    sf = SourceFile(filePath)
    sf_path = sf.getFilePath()
    
    if filePath == sf_path:
        print("PASS testcase 05")
        return 1
    else:
        print("FAIL testcase 05")
        return 0
        
###################################################################################################
###                                Testcase 06                                                  ###
###################################################################################################
def testcase06(filePath, newText):
    sf = SourceFile(filePath)
    sf.overwriteFile(newText)
    
    if sf._SourceFile__fileText == newText:
        print("PASS testcase 06")
        return 1
    else:
        print("FAIL testcase 06")
        return 0

###################################################################################################
###                                Testcase 07                                                  ###
###################################################################################################
def testcase07(filePath, oldText, newText):
    sf = SourceFile(filePath)
    sf.appendFile(newText)
    
    text = oldText + newText
    
    if text == sf._SourceFile__fileText:
        print("PASS testcase 07")
        return 1
    else:
        print("FAIL testcase 07")
        return 0

###################################################################################################
###                                Testcase 08                                                  ###
###################################################################################################
def testcase08(filePath, expectedTokens):
    sf = SourceFile(filePath)
    tokens = sf.tokenize()
    
    isEqual = True
    index = 0
    while index < len(tokens):
        token = tokens[index]
        expectedToken = expectedTokens[index]
        
        if token.token_value != expectedToken.token_value or token.token_type != expectedToken.token_type or token.position != expectedToken.position:
            isEqual = False
            
        index += 1
           
    if isEqual:
        print("PASS testcase 08")
        return 1
    else:
        print("FAIL testcase 08")
        return 0

###################################################################################################
###                                Testcase 09                                                  ###
###################################################################################################
def testcase09(filePath, expectedTokens):
    sf = SourceFile(filePath)
    sf.readFile()
    tokens = sf.tokenize()
      
    isEqual = True
    index = 0
    while index < len(tokens):
        token = tokens[index]
        expectedToken = expectedTokens[index]
        
        if token.token_value != expectedToken.token_value or token.token_type != expectedToken.token_type or token.position != expectedToken.position:
            isEqual = False
            
        index += 1
        
    if isEqual:
        print("PASS testcase 09")
        return 1
    else:
        print("FAIL testcase 09")
        return 0

###################################################################################################
###                                Testcase 10                                                  ###
###################################################################################################
def testcase10(filePath):
    sf = SourceFile(filePath)
    tokens = sf.tokenize()
    
    isComment = False
    for token in tokens:
        if token.token_type == TokenType.COMMENT_SYMBOL:
            isComment = True
    
    if not isComment:
        print("PASS testcase 10")
        return 1
    else:
        print("FAIL testcase 10")
        return 0
