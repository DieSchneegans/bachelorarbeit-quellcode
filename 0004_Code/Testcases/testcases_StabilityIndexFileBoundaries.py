"""
    Unit-Testcase StabilityIndexFileBoundaries
"""

###################################################################################################
###                                imports                                                      ###
###################################################################################################
import sys

try:
    from CalculationModule.StabilityIndexFileBoundaries import StabilityIndexFileBoundaries
except ModuleNotFoundError:
    print("CalculationModule.StabilityIndexFileBoundaries not installed. Exiting...")
    sys.exit(4)
    
###################################################################################################
###                                constant values                                              ###
###################################################################################################

###################################################################################################
###                                Testcase 38                                                  ###
###################################################################################################
def testcase38(mode_visual, mode_findMatches):
    si = StabilityIndexFileBoundaries(mode_visual, mode_findMatches)
    
    if si._mode_visual == mode_visual and si._mode_findMatches == mode_findMatches:
        print("PASS testcase 38")
        return 1
    else:
        print("FAIL testcase 38")
        return 0

###################################################################################################
###                                Testcase 39                                                  ###
###################################################################################################
def testcase39(mode_visual, mode_findMatches, expectedStabilityIndex, path_old, path_new):
    si = StabilityIndexFileBoundaries(mode_visual, mode_findMatches)
    si.calcStabilityIndex(path_old, path_new)
    
    if si._stabilityIndex == expectedStabilityIndex:
        print("PASS testcase 39")
        return 1
    else:
        print("FAIL testcase 39")
        return 0

###################################################################################################
###                                Testcase 40                                                  ###
###################################################################################################
def testcase40(mode_visual, mode_findMatches):
    si = StabilityIndexFileBoundaries(mode_visual, mode_findMatches)
    
    if si.__str__() == "S: {} tokens, A: {} tokens, D: {} tokens, M: {} steps, F: {} couple of files -> StabilityIndex: {} percent (Content Stability: {} percent, Structure Stability: {} percent)".format(0, 0, 0, 0, 0, -100, -100, -100):
        print("PASS testcase 40")
        return 1
    else:
        print("FAIL testcase 40")
        return 0
