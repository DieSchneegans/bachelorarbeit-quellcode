"""
    Unit-Testcase StabilityIndex
"""

###################################################################################################
###                                imports                                                      ###
###################################################################################################
import sys

try:
    from CalculationModule.StabilityIndex import StabilityIndex
except ModuleNotFoundError:
    print("CalculationModule.StabilityIndex not installed. Exiting...")
    sys.exit(4)
    
###################################################################################################
###                                constant values                                              ###
###################################################################################################

###################################################################################################
###                                Testcase 35                                                  ###
###################################################################################################
def testcase35(mode_visual, mode_findMatches):
    si = StabilityIndex(mode_visual, mode_findMatches)
    
    if mode_visual == si._mode_visual and mode_findMatches == si._mode_findMatches:
        print("PASS testcase 35")
        return 1
    else:
        print("FAIL testcase 35")
        return 0

###################################################################################################
###                                Testcase 36                                                  ###
###################################################################################################
def testcase36(mode_visual, mode_findMatches, expectedStabilityIndex, path_old, path_new):
    si = StabilityIndex(mode_visual, mode_findMatches)
    si.calcStabilityIndex(path_old, path_new)
    
    if si._stabilityIndex == expectedStabilityIndex:
        print("PASS testcase 36")
        return 1
    else:
        print("FAIL testcase 36")
        return 0

###################################################################################################
###                                Testcase 37                                                  ###
###################################################################################################
def testcase37(mode_visual, mode_findMatches):
    si = StabilityIndex(mode_visual, mode_findMatches)
    
    if si.__str__() == "S: {} tokens, A: {} tokens, D: {} tokens, M: {} steps -> StabilityIndex: {} percent (Content Stability: {} percent, Structure Stability: {} percent)".format(0, 0, 0, 0, -100, -100, -100):
        print("PASS testcase 37")
        return 1
    else:
        print("FAIL testcase 37")
        return 0
