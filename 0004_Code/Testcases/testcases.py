"""
    Unit-Testcases Starts
"""

###################################################################################################
###                                imports                                                      ###
###################################################################################################
import sys

try:
    from pathlib import WindowsPath
except ModuleNotFoundError:
    print("pathlib not installed. Exiting...")
    sys.exit(4)
    
try:
    from sctokenizer import tokenize_file
except ModuleNotFoundError:
    print("sctokenizer not installed. Exiting...")
    sys.exit(4)

try:
    import Testcases.testcases_SourceFile as testcases_SourceFile
except ModuleNotFoundError:
    print("Testcases.testcases_SourceFile not installed. Exiting...")
    sys.exit(4)
    
try:
    import Testcases.testcases_SourceBase as testcases_SourceBase
except ModuleNotFoundError:
    print("Testcases.testcases_SourceBase not installed. Exiting...")
    sys.exit(4)
    
try:
    import Testcases.testcases_Sequence as testcases_Sequence
except ModuleNotFoundError:
    print("Testcases.testcases_Sequence not installed. Exiting...")
    sys.exit(4)
    
try:
    import Testcases.testcases_Match as testcases_Match
except ModuleNotFoundError:
    print("Testcases.testcases_Match not installed. Exiting...")
    sys.exit(4)
    
try:
    import Testcases.testcases_StabilityIndex as testcases_StabilityIndex
except ModuleNotFoundError:
    print("Testcases.testcases_StabilityIndex not installed. Exiting...")
    sys.exit(4)
    
try:
    import Testcases.testcases_StabilityIndexFileBoundaries as testcases_StabilityIndexFileBoundaries
except ModuleNotFoundError:
    print("Testcases.testcases_StabilityIndexFileBoundaries not installed. Exiting...")
    sys.exit(4)
 
###################################################################################################
###                                helping tools                                                ###
###################################################################################################
counterTests = 0
counterTestsPass = 0

def printStart(testcaseNr, numberOfTests):
    global counterTests
    counterTests += numberOfTests
    print(60*"-")
    print("TESTCASE {}".format(testcaseNr))
    print(60*"-")
    
def printBlockStart(blockName):
    print(100*'*')
    print("TESTBLOCK {}".format(blockName))
    input("Press <ENTER> to continue...")
    print(100*'*')
    
def printEnde():
    print(100*'*')
    print("TESTCASES FINISH!")
    print("{} tests successful by {} tests".format(counterTestsPass, counterTests))
    print(100*'*')
    
###################################################################################################
###                                Testcases SourceFile                                         ###
###################################################################################################
printBlockStart("SourceFile")

filePath_1 = WindowsPath(r'C:\Projects\Internal2.0\BSW\ToolDev\StableCode\0005_Tests\0000_Nothing\0001_Old\example.c')
filePath_2 = WindowsPath(r'C:\Projects\Internal2.0\BSW\ToolDev\StableCode\0005_Tests\0000_Nothing\0002_New\example.c')
filePath_3 = WindowsPath(r'C:\Projects\Internal2.0\BSW\ToolDev\StableCode\0005_Tests\0000_Nothing\0003_Old - Cpp\example_cpp.c')
filePath_4 = WindowsPath(r'C:\Projects\Internal2.0\BSW\ToolDev\StableCode\0005_Tests\0000_Nothing\0004_New - Cpp\example_cpp.c')
filePath_5 = WindowsPath(r'C:\Projects\Internal2.0\BSW\ToolDev\StableCode\0005_Tests\0009_TokenizeTests\affenSindCool.c')
filePath_6 = WindowsPath(r'C:\Projects\Internal2.0\BSW\ToolDev\StableCode\0005_Tests\0009_TokenizeTests\SchoenIstDasLeben.c')

printStart(1, 4)
counterTestsPass += testcases_SourceFile.testcase01(filePath_1)
counterTestsPass += testcases_SourceFile.testcase01(filePath_2)
counterTestsPass += testcases_SourceFile.testcase01(filePath_3)
counterTestsPass += testcases_SourceFile.testcase01(filePath_4)

printStart(2, 4)
counterTestsPass += testcases_SourceFile.testcase02(filePath_1, filePath_1.read_text())
counterTestsPass += testcases_SourceFile.testcase02(filePath_2, filePath_2.read_text())
counterTestsPass += testcases_SourceFile.testcase02(filePath_3, filePath_3.read_text())
counterTestsPass += testcases_SourceFile.testcase02(filePath_4, filePath_4.read_text())

printStart(3, 4)
counterTestsPass += testcases_SourceFile.testcase03(filePath_1, filePath_1.read_text())
counterTestsPass += testcases_SourceFile.testcase03(filePath_2, filePath_2.read_text())
counterTestsPass += testcases_SourceFile.testcase03(filePath_3, filePath_3.read_text())
counterTestsPass += testcases_SourceFile.testcase03(filePath_4, filePath_4.read_text())

printStart(4, 4)
counterTestsPass += testcases_SourceFile.testcase04(filePath_1)
counterTestsPass += testcases_SourceFile.testcase04(filePath_2)
counterTestsPass += testcases_SourceFile.testcase04(filePath_3)
counterTestsPass += testcases_SourceFile.testcase04(filePath_4)

printStart(5, 4)
counterTestsPass += testcases_SourceFile.testcase05(filePath_1)
counterTestsPass += testcases_SourceFile.testcase05(filePath_2)
counterTestsPass += testcases_SourceFile.testcase05(filePath_3)
counterTestsPass += testcases_SourceFile.testcase05(filePath_4)

printStart(6, 4)

fileText = filePath_1.read_text()
counterTestsPass += testcases_SourceFile.testcase06(filePath_1, "Affen sind cool!")
filePath_1.write_text(fileText)

fileText = filePath_2.read_text()
counterTestsPass += testcases_SourceFile.testcase06(filePath_2, "Affen sind cool!")
filePath_2.write_text(fileText)

fileText = filePath_3.read_text()
counterTestsPass += testcases_SourceFile.testcase06(filePath_3, "Affen sind cool!")
filePath_3.write_text(fileText)

fileText = filePath_4.read_text()
counterTestsPass += testcases_SourceFile.testcase06(filePath_4, "Affen sind cool!")
filePath_4.write_text(fileText)

printStart(7, 4)

fileText = filePath_1.read_text()
counterTestsPass += testcases_SourceFile.testcase07(filePath_1, filePath_1.read_text(), "Affen sind cool!")
filePath_1.write_text(fileText)

fileText = filePath_2.read_text()
counterTestsPass += testcases_SourceFile.testcase07(filePath_2, filePath_2.read_text(), "Affen sind cool!")
filePath_2.write_text(fileText)

fileText = filePath_3.read_text()
counterTestsPass += testcases_SourceFile.testcase07(filePath_3, filePath_3.read_text(), "Affen sind cool!")
filePath_3.write_text(fileText)

fileText = filePath_4.read_text()
counterTestsPass += testcases_SourceFile.testcase07(filePath_4, filePath_4.read_text(), "Affen sind cool!")
filePath_4.write_text(fileText)

printStart(8, 2)
counterTestsPass += testcases_SourceFile.testcase08(filePath_5, tokenize_file(filePath_5, "c"))
counterTestsPass += testcases_SourceFile.testcase08(filePath_6, tokenize_file(filePath_6, "c"))

printStart(9, 2)
counterTestsPass += testcases_SourceFile.testcase09(filePath_5, tokenize_file(filePath_5, "c"))
counterTestsPass += testcases_SourceFile.testcase09(filePath_6, tokenize_file(filePath_6, "c"))

printStart(10, 4)
counterTestsPass += testcases_SourceFile.testcase10(filePath_1)
counterTestsPass += testcases_SourceFile.testcase10(filePath_2)
counterTestsPass += testcases_SourceFile.testcase10(filePath_3)
counterTestsPass += testcases_SourceFile.testcase10(filePath_4)

###################################################################################################
###                                Testcases SourceBase                                         ###
###################################################################################################
printBlockStart("SourceBase")

rootPath_1 = WindowsPath(r"C:\Projects\Internal2.0\BSW\ToolDev\StableCode\0005_Tests\0000_Nothing\0001_Old")
rootPath_2 = WindowsPath(r"C:\Projects\Internal2.0\BSW\ToolDev\StableCode\0005_Tests\0000_Nothing\0002_New")
rootPath_3 = WindowsPath(r"C:\Projects\Internal2.0\BSW\ToolDev\StableCode\0005_Tests\0000_Nothing\0003_Old - Cpp")
rootPath_4 = WindowsPath(r"C:\Projects\Internal2.0\BSW\ToolDev\StableCode\0005_Tests\0000_Nothing\0004_New - Cpp")
rootPath_5 = WindowsPath(r'C:\Projects\Internal2.0\BSW\ToolDev\StableCode\0005_Tests\0009_TokenizeTests')

printStart(11, 4)
counterTestsPass += testcases_SourceBase.testcase11(rootPath_1, 0)
counterTestsPass += testcases_SourceBase.testcase11(rootPath_2, 1)
counterTestsPass += testcases_SourceBase.testcase11(rootPath_3, 2)
counterTestsPass += testcases_SourceBase.testcase11(rootPath_4, 3)

printStart(12, 4)
counterTestsPass += testcases_SourceBase.testcase11(rootPath_1, 0)
counterTestsPass += testcases_SourceBase.testcase11(rootPath_2, 1)
counterTestsPass += testcases_SourceBase.testcase11(rootPath_3, 2)
counterTestsPass += testcases_SourceBase.testcase11(rootPath_4, 3)

printStart(13, 4)
counterTestsPass += testcases_SourceBase.testcase13(rootPath_1, 0, 1)
counterTestsPass += testcases_SourceBase.testcase13(rootPath_2, 1, 1)
counterTestsPass += testcases_SourceBase.testcase13(rootPath_3, 2, 1)
counterTestsPass += testcases_SourceBase.testcase13(rootPath_4, 3, 1)

printStart(14, 4)
counterTestsPass += testcases_SourceBase.testcase14(rootPath_1, 0)
counterTestsPass += testcases_SourceBase.testcase14(rootPath_2, 1)
counterTestsPass += testcases_SourceBase.testcase14(rootPath_3, 2)
counterTestsPass += testcases_SourceBase.testcase14(rootPath_4, 3)

printStart(15, 4)
counterTestsPass += testcases_SourceBase.testcase15(rootPath_1, 0, 1)
counterTestsPass += testcases_SourceBase.testcase15(rootPath_2, 1, 1)
counterTestsPass += testcases_SourceBase.testcase15(rootPath_3, 2, 1)
counterTestsPass += testcases_SourceBase.testcase15(rootPath_4, 3, 1)

printStart(16, 1)
counterTestsPass += testcases_SourceBase.testcase16(rootPath_5, 0, tokenize_file(rootPath_5.joinpath("affenSindCool.c"), 'c') + tokenize_file(rootPath_5.joinpath("SchoenIstDasLeben.c"), 'c'))

printStart(17, 4)
counterTestsPass += testcases_SourceBase.testcase17(rootPath_1, 0, rootPath_1.joinpath("example.c").read_text())
counterTestsPass += testcases_SourceBase.testcase17(rootPath_2, 0, rootPath_2.joinpath("example.c").read_text())
counterTestsPass += testcases_SourceBase.testcase17(rootPath_3, 0, rootPath_3.joinpath("example_cpp.c").read_text())
counterTestsPass += testcases_SourceBase.testcase17(rootPath_4, 0, rootPath_4.joinpath("example_cpp.c").read_text())

###################################################################################################
###                                Testcases Match                                              ###
###################################################################################################
printBlockStart("Match")

start_old_1 = 5
start_old_2 = 7
start_old_3 = 0
start_old_4 = 50

start_new_1 = 0
start_new_2 = 48
start_new_3 = 8
start_new_4 = 34

length_1 = 7
length_2 = 2
length_3 = 6
length_4 = 13

printStart(18, 4)
counterTestsPass += testcases_Match.testcase18(start_old_1, start_new_1, length_1)
counterTestsPass += testcases_Match.testcase18(start_old_2, start_new_2, length_2)
counterTestsPass += testcases_Match.testcase18(start_old_3, start_new_3, length_3)
counterTestsPass += testcases_Match.testcase18(start_old_4, start_new_4, length_4)

printStart(19, 4)
counterTestsPass += testcases_Match.testcase19(start_old_1, start_new_1, length_1)
counterTestsPass += testcases_Match.testcase19(start_old_2, start_new_2, length_2)
counterTestsPass += testcases_Match.testcase19(start_old_3, start_new_3, length_3)
counterTestsPass += testcases_Match.testcase19(start_old_4, start_new_4, length_4)

printStart(20, 4)
counterTestsPass += testcases_Match.testcase20(start_old_1, start_new_1, length_1)
counterTestsPass += testcases_Match.testcase20(start_old_2, start_new_2, length_2)
counterTestsPass += testcases_Match.testcase20(start_old_3, start_new_3, length_3)
counterTestsPass += testcases_Match.testcase20(start_old_4, start_new_4, length_4)

printStart(21, 4)
counterTestsPass += testcases_Match.testcase21(start_old_1, start_new_1, length_1)
counterTestsPass += testcases_Match.testcase21(start_old_2, start_new_2, length_2)
counterTestsPass += testcases_Match.testcase21(start_old_3, start_new_3, length_3)
counterTestsPass += testcases_Match.testcase21(start_old_4, start_new_4, length_4)

printStart(22, 4)
counterTestsPass += testcases_Match.testcase22(start_old_1, start_new_1, length_1)
counterTestsPass += testcases_Match.testcase22(start_old_2, start_new_2, length_2)
counterTestsPass += testcases_Match.testcase22(start_old_3, start_new_3, length_3)
counterTestsPass += testcases_Match.testcase22(start_old_4, start_new_4, length_4)

printStart(23, 4)
counterTestsPass += testcases_Match.testcase23(start_old_1, start_new_1, length_1)
counterTestsPass += testcases_Match.testcase23(start_old_2, start_new_2, length_2)
counterTestsPass += testcases_Match.testcase23(start_old_3, start_new_3, length_3)
counterTestsPass += testcases_Match.testcase23(start_old_4, start_new_4, length_4)

printStart(24, 4)
counterTestsPass += testcases_Match.testcase24(start_old_1, start_new_1, length_1)
counterTestsPass += testcases_Match.testcase24(start_old_2, start_new_2, length_2)
counterTestsPass += testcases_Match.testcase24(start_old_3, start_new_3, length_3)
counterTestsPass += testcases_Match.testcase24(start_old_4, start_new_4, length_4)

printStart(25, 4)
counterTestsPass += testcases_Match.testcase25(start_old_1, start_new_1, length_1)
counterTestsPass += testcases_Match.testcase25(start_old_2, start_new_2, length_2)
counterTestsPass += testcases_Match.testcase25(start_old_3, start_new_3, length_3)
counterTestsPass += testcases_Match.testcase25(start_old_4, start_new_4, length_4)

###################################################################################################
###                                Testcases Sequence                                           ###
###################################################################################################
printBlockStart("Sequence")

start_1 = 5
start_2 = 48
start_3 = 79
start_4 = 0

length_1 = 4
length_2 = 8
length_3 = 75
length_4 = 2

printStart(26, 4)
counterTestsPass += testcases_Sequence.testcase26(start_1, length_1)
counterTestsPass += testcases_Sequence.testcase26(start_2, length_2)
counterTestsPass += testcases_Sequence.testcase26(start_3, length_3)
counterTestsPass += testcases_Sequence.testcase26(start_4, length_4)

printStart(27, 4)
counterTestsPass += testcases_Sequence.testcase27(start_1, length_1)
counterTestsPass += testcases_Sequence.testcase27(start_2, length_2)
counterTestsPass += testcases_Sequence.testcase27(start_3, length_3)
counterTestsPass += testcases_Sequence.testcase27(start_4, length_4)

printStart(28, 4)
counterTestsPass += testcases_Sequence.testcase28(start_1, length_1)
counterTestsPass += testcases_Sequence.testcase28(start_2, length_2)
counterTestsPass += testcases_Sequence.testcase28(start_3, length_3)
counterTestsPass += testcases_Sequence.testcase28(start_4, length_4)

printStart(29, 4)
counterTestsPass += testcases_Sequence.testcase29(start_1, length_1)
counterTestsPass += testcases_Sequence.testcase29(start_2, length_2)
counterTestsPass += testcases_Sequence.testcase29(start_3, length_3)
counterTestsPass += testcases_Sequence.testcase29(start_4, length_4)

printStart(30, 4)
counterTestsPass += testcases_Sequence.testcase30(start_1, length_1)
counterTestsPass += testcases_Sequence.testcase30(start_2, length_2)
counterTestsPass += testcases_Sequence.testcase30(start_3, length_3)
counterTestsPass += testcases_Sequence.testcase30(start_4, length_4)

printStart(31, 4)
counterTestsPass += testcases_Sequence.testcase31(start_1, length_1)
counterTestsPass += testcases_Sequence.testcase31(start_2, length_2)
counterTestsPass += testcases_Sequence.testcase31(start_3, length_3)
counterTestsPass += testcases_Sequence.testcase31(start_4, length_4)

printStart(32, 4)
counterTestsPass += testcases_Sequence.testcase32(start_1, length_1)
counterTestsPass += testcases_Sequence.testcase32(start_2, length_2)
counterTestsPass += testcases_Sequence.testcase32(start_3, length_3)
counterTestsPass += testcases_Sequence.testcase32(start_4, length_4)

printStart(33, 4)
counterTestsPass += testcases_Sequence.testcase33(start_1, length_1)
counterTestsPass += testcases_Sequence.testcase33(start_2, length_2)
counterTestsPass += testcases_Sequence.testcase33(start_3, length_3)
counterTestsPass += testcases_Sequence.testcase33(start_4, length_4)

printStart(34, 4)
counterTestsPass += testcases_Sequence.testcase34(start_1, length_1)
counterTestsPass += testcases_Sequence.testcase34(start_2, length_2)
counterTestsPass += testcases_Sequence.testcase34(start_3, length_3)
counterTestsPass += testcases_Sequence.testcase34(start_4, length_4)

###################################################################################################
###                                Testcases StabilityIndex                                     ###
###################################################################################################
printBlockStart("StabilityIndex")

path_old_1 = WindowsPath(r"C:\Projects\Internal2.0\BSW\ToolDev\StableCode\0005_Tests\0000_Nothing\0001_Old")
path_new_1 = WindowsPath(r"C:\Projects\Internal2.0\BSW\ToolDev\StableCode\0005_Tests\0000_Nothing\0002_New")
path_old_2 = WindowsPath(r"C:\Projects\Internal2.0\BSW\ToolDev\StableCode\0005_Tests\0000_Nothing\0003_Old - Cpp")
path_new_2 = WindowsPath(r"C:\Projects\Internal2.0\BSW\ToolDev\StableCode\0005_Tests\0000_Nothing\0004_New - Cpp")

printStart(35, 4)
counterTestsPass += testcases_StabilityIndex.testcase35(2, 0)
counterTestsPass += testcases_StabilityIndex.testcase35(3, 1)
counterTestsPass += testcases_StabilityIndex.testcase35(4, 2)
counterTestsPass += testcases_StabilityIndex.testcase35(5, 3)

printStart(36, 2)
counterTestsPass += testcases_StabilityIndex.testcase36(2, 3, 1, path_old_1, path_new_1)
counterTestsPass += testcases_StabilityIndex.testcase36(2, 3, 1, path_old_2, path_new_2)

printStart(37, 4)
counterTestsPass += testcases_StabilityIndex.testcase37(2, 0)
counterTestsPass += testcases_StabilityIndex.testcase37(3, 1)
counterTestsPass += testcases_StabilityIndex.testcase37(4, 2)
counterTestsPass += testcases_StabilityIndex.testcase37(5, 3)

###################################################################################################
###                                Testcases StabilityIndexFileBoundaries                       ###
###################################################################################################
printBlockStart("StabilityIndexFileBoundaries")

path_old_1 = WindowsPath(r"C:\Projects\Internal2.0\BSW\ToolDev\StableCode\0005_Tests\0000_Nothing\0001_Old")
path_new_1 = WindowsPath(r"C:\Projects\Internal2.0\BSW\ToolDev\StableCode\0005_Tests\0000_Nothing\0002_New")
path_old_2 = WindowsPath(r"C:\Projects\Internal2.0\BSW\ToolDev\StableCode\0005_Tests\0000_Nothing\0003_Old - Cpp")
path_new_2 = WindowsPath(r"C:\Projects\Internal2.0\BSW\ToolDev\StableCode\0005_Tests\0000_Nothing\0004_New - Cpp")

printStart(38, 4)
counterTestsPass += testcases_StabilityIndexFileBoundaries.testcase38(2, 0)
counterTestsPass += testcases_StabilityIndexFileBoundaries.testcase38(3, 1)
counterTestsPass += testcases_StabilityIndexFileBoundaries.testcase38(4, 2)
counterTestsPass += testcases_StabilityIndexFileBoundaries.testcase38(5, 3)

printStart(39, 2)
counterTestsPass += testcases_StabilityIndexFileBoundaries.testcase39(2, 3, 1, path_old_1, path_new_1)
counterTestsPass += testcases_StabilityIndexFileBoundaries.testcase39(2, 3, 1, path_old_2, path_new_2)

printStart(40, 4)
counterTestsPass += testcases_StabilityIndexFileBoundaries.testcase40(2, 0)
counterTestsPass += testcases_StabilityIndexFileBoundaries.testcase40(3, 1)
counterTestsPass += testcases_StabilityIndexFileBoundaries.testcase40(4, 2)
counterTestsPass += testcases_StabilityIndexFileBoundaries.testcase40(5, 3)

printEnde()