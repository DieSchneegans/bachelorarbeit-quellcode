"""
    Unit-Testcase SourceBase
"""

###################################################################################################
###                                imports                                                      ###
###################################################################################################
import sys

try:
    from FileModule.SourceBase import SourceBase
except ModuleNotFoundError: 
    print("FileModule.SourceBase not installed. Exiting...")
    sys.exit(4)

###################################################################################################
###                                constant values                                              ###
###################################################################################################

###################################################################################################
###                                Testcase 11                                                  ###
###################################################################################################
def testcase11(rootPath, mode):
    sb = SourceBase(mode, rootPath)
    
    if rootPath == sb._SourceBase__rootPath:
        print("PASS testcase 11")
        return 1
    else:
        print("FAIL testcase 11")
        return 0

###################################################################################################
###                                Testcase 12                                                  ###
###################################################################################################
def testcase12(rootPath, mode):
    sb = SourceBase(mode, rootPath)
    
    if mode == sb._SourceBase__mode:
        print("PASS testcase 12")
        return 1
    else:
        print("FAIL testcase 12")
        return 0

###################################################################################################
###                                Testcase 13                                                  ###
###################################################################################################
def testcase13(rootPath, mode, length_files):
    sb = SourceBase(mode, rootPath)
    sb.searchFiles()
    if length_files == len(sb.getFiles()) and type(sb.getFiles()) is list:
        print("PASS testcase 13")
        return 1
    else:
        print("FAIL testcase 13")
        return 0

###################################################################################################
###                                Testcase 14                                                  ###
###################################################################################################
def testcase14(rootPath, mode):
    sb = SourceBase(mode, rootPath)
    files = sb.searchFiles()
    
    if len(files) == 1 and type(files) is list:
        print("PASS testcase 14")
        return 1
    else:
        print("FAIL testcase 14")
        return 0

###################################################################################################
###                                Testcase 15                                                  ###
###################################################################################################
def testcase15(rootPath, mode, length_files):
    sb = SourceBase(mode, rootPath)
    files = sb.searchFiles()
    
    if len(files) == length_files and type(files) is list:
        print("PASS testcase 15")
        return 1
    else:
        print("FAIL testcase 15")
        return 0

###################################################################################################
###                                Testcase 16                                                  ###
###################################################################################################
def testcase16(rootPath, mode, expectedTokens):
    sb = SourceBase(mode, rootPath)
    tokens = sb.tokenizeFiles()
    
    isEqual = True
    index = 0
    while index < len(tokens):
        token = tokens[index]
        expectedToken = expectedTokens[index]
        
        if token.token_value != expectedToken.token_value or token.token_type != expectedToken.token_type or token.position != expectedToken.position:
            isEqual = False
            
        index += 1
        
    if isEqual:
        print("PASS testcase 16")
        return 1
    else:
        print("FAIL testcase 16")
        return 0

###################################################################################################
###                                Testcase 17                                                  ###
###################################################################################################
def testcase17(rootPath, mode, expectedText):
    sb = SourceBase(mode, rootPath)
    sb.searchFiles()
    text = sb.joinFiles()
    
    if text == expectedText:
        print("PASS testcase 17")
        return 1
    else:
        print("FAIL testcase 17")
        return 0
