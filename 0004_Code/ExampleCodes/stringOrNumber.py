from de.kspg.svn.stats.ExecTimer import ExecTimer
import random
import string

range_value = 100000000
mod_value = range_value / 10

string_length = 15
letters = string.ascii_letters

numbers = []
strings = []
index = 0
for index in range(range_value):
    strings.append("".join(random.choice(letters) for i in range(string_length)))
    numbers.append(random.randint(1E+14, 9E+14))
    
    index += 1
    if not index % 1000000:
        print("Round: {}".format(index))

timer_number = ExecTimer("numbers", range_value)
print("--------------------\nTimerstart Numbers\n--------------------")
timer_number.Start()

yeah_number = 0
for index in range(range_value):
    if numbers[index] == numbers[int(range_value/2)]:
        yeah_number += 1
    
    if not index % mod_value:
        timer_number.Lap(itemsProcessed=index, doReport=True)

timer_number.Stop(itemsProcessed=range_value, doReport=True)
print(yeah_number)
print("--------------------")




timer_number = ExecTimer("strings", range_value)
print("--------------------\nTimerstart Strings\n--------------------")
timer_number.Start()

yeah_string = 0
for index in range(range_value):
    if strings[index] == strings[int(range_value/2)]:
        yeah_string += 1
    
    if not index % mod_value:
        timer_number.Lap(itemsProcessed=index, doReport=True)

timer_number.Stop(itemsProcessed=range_value, doReport=True)
print(yeah_string)
print("--------------------")
