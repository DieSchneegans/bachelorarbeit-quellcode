#===============================================================================
# represents an interval of integer numbers like [3,7[ = (3,4,5,6)
#===============================================================================
class Sequence:

    def __init__(self, start, end):
        self.start = start
        self.end = end
        
    def __str__(self):
        return "[{},{}[".format(self.start, self.end)
        
    def __eq__(self, other):
        #print("comparing {} to {}".format(self, other))
        if isinstance(other, Sequence):
            return self.start == other.start and self.end == other.end
            
    def length(self):
        return self.end - self.start
        
    # subtracts another sequence from this one. If it changed, the function returns True,
    # otherwise the other sequence did not overlap and nothing changed.
    # It is possible that the sequence gets reduced to zero length by the subtraction.
    def subtract(self, other):
        print("calculating {} minus {}".format(self, other))
        
        if other.end <= self.start:
            print("missed left")
            return False, 0 
            
            
        if self.end <= other.start:
            print("missed right")
            return False, 0
            
        
        if other.start <= self.start:
            if other.end >= self.end:
                print("swallowed")
                self.end = self.start
                return True, 1
            else:
                print("clip head")
                self.start = other.end
                return True, 2
        else:
            print("clip tail")
            self.end = other.start
            return True, 3
            
        return True, 0


#===============================================================================
# This is similar to Sequence but it stores the length instead of stop.
# I thought this might reduce the effort of deriving sequences from a match
# but you need to know the stop anyway when you subtract, so no big savings.
#===============================================================================
class SeqByLength:       
    def __init__(self, start, length):
        self.start = start
        self.length = length
        
    def __str__(self):
        return "[{},{}[ l={}".format(self.start, self.end(), self.length)
        
    def __eq__(self, other):
        #print("comparing {} to {}".format(self, other))
        if isinstance(other, SeqByLength):
            return self.start == other.start and self.length == other.length
            
    def end(self):
        return self.start + self.length
        
    # subtracts another sequence from this one. If it changed, the function returns True,
    # otherwise the other sequence did not overlap and nothing changed.
    # It is possible that the sequence gets reduced to zero length by the subtraction.
    def subtract(self, other):
        print("calculating {} minus {}".format(self, other))
        
        otherEnd = other.end()
        if otherEnd <= self.start:
            print("missed left")
            return False, 0
            
        selfEnd = self.end()    
        if selfEnd <= other.start:
            print("missed right")
            return False, 0
            
        
        if other.start <= self.start:
            if otherEnd >= selfEnd:
                print("swallowed")
                self.length = 0
                return True, 1
            else:
                print("clip head")
                self.length = selfEnd - otherEnd
                self.start = otherEnd
                return True, 2
                
        else:
            print("clip tail")
            self.length = other.start - self.start
            return True, 3
            
        return True, 0

#===============================================================================
#
#===============================================================================
class Match:

    def __init__(self, s0, s1, length):
        self.start = [s0, s1]
        self.length = length
        
    def __str__(self):
        return "({},{},{})".format(self.start[0], self.start[1], self.length)
        
    def __eq__(self, other):
        if isinstance(other, Match):
            print("comparing {} with {}".format(self, other))
            return self.start[0] == other.start[0] and self.start[1] == other.start[1] and self.length == other.length
        else:
            return False
        
    def end(self, side):
        return self.start[side] + self.length
        
        
    def reduceBy(self, other):
        print(60*"-")
        print("reducing match {} by {}".format(self, other))
        self.reduceSide(0, other)
        print(self)
        if self.length > 0:
            self.reduceSide(1, other)            
            print(self)
        return self

#===============================================================================
# These Matches compare for non-overlap first
#===============================================================================
class MatchFast(Match):
        
    # this could be minimally faster as in the mean 1.5 comparisons are needed
    def reduceSide(self, side, other):
        print("FAST - side {}: reducing seq ({},{}) by ({},{})".format(side, self.start[side], self.end(side), other.start[side], other.end(side)))
    
        otherEnd = other.end(side)
        if otherEnd <= self.start[side]:
            print("no overlap (missing left)")
            return
        
        selfEnd = self.end(side)
        if other.start[side] >= selfEnd:
            print("no overlap (missing right)")
            return
            
        # now we have a guaranteed overlap >= 1
        if other.start[side] <= self.start[side]:
            if otherEnd >= selfEnd:
                print("other swallowed")
                self.length = 0
            else:
                print("clipping head")
                self.length = selfEnd - otherEnd
                self.start[0] = other.end(0)
                self.start[1] = other.end(1)
        else:
            print("clipping tail")
            self.length = other.start[side] - self.start[side]

#===============================================================================
# These Matches calculate the overlap to reduce each side
#===============================================================================
class MatchOverlap(Match):

    # this needs 2 comparisons minimum, 3 if you count overlap > 0.
    # but it is much easier to understand
    def reduceSide(self, side, other):
        print("OVL - side {}: reducing seq ({},{}) by ({},{})".format(side, self.start[side], self.end(side), other.start[side], other.end(side)))
        
        overlap = min(self.end(side),other.end(side)) - max(self.start[side], other.start[side])
        print("overlap: {}".format(overlap))
        
        if overlap > 0:
            self.length = self.length - overlap
            
            # only if not swallowed
            if self.length > 0:
                print("clipped head or tail")
                if other.start[side] < self.start[side]:
                    print("clipped head")
                    self.start[0] = other.end(0)
                    self.start[1] = other.end(1)

#===============================================================================
# These matches use Sequence objects to reduce each side
#===============================================================================
class MatchSeq(Match):

    def reduceSide(self, side, other):
        
        seqSelf = Sequence(self.start[side], self.start[side] + self.length)
        seqOther = Sequence(other.start[side], other.start[side] + other.length)
        
        print("SEQ - side {}: reducing seq {} by {}".format(side, seqSelf, seqOther))
        
        isSubtract, mode = seqSelf.subtract(seqOther)
        if isSubtract:
            self.start[side] = seqSelf.start
            self.length = seqSelf.length()
            
            if mode == 2 and side == 0:
                self.start[1] = other.end(1)
            elif mode == 2 and side == 1:
                self.start[0] = other.end(0)

#===============================================================================
# These matches use Sequence objects to reduce each side
#===============================================================================
class MatchSeqByLength(Match):

    def reduceSide(self, side, other):
        
        seqSelf = SeqByLength(self.start[side], self.length)
        seqOther = SeqByLength(other.start[side], other.length)
        
        print("SEQBYL - side {}: reducing seq {} by {}".format(side, seqSelf, seqOther))
        
        isSubtract, mode = seqSelf.subtract(seqOther)
        if isSubtract:
            self.start[side] = seqSelf.start
            self.length = seqSelf.length
            
            if mode == 2 and side == 0:
                self.start[1] = other.end(1)
            elif mode == 2 and side == 1:
                self.start[0] = other.end(0)

#===============================================================================
# SCRIPT
#===============================================================================

     
def testSequenceSubtraction(minuend, subtrahend, expDiff):
    print(40*"-")
    backupMinuend = SeqByLength(minuend.start, minuend.length)
    hasChangedCalc = minuend.subtract(subtrahend)
    print("Calculated: {} Expected: {} hasChangedCalc: {}".format(minuend, expDiff, hasChangedCalc))
    if minuend == expDiff:
        print("PASS subtraction")
    else:
        print("FAIL subtraction")
    
    hasChangedExp = backupMinuend != minuend
    
    if hasChangedExp == hasChangedCalc:
        print("PASS hasChanged")
    else:
        print("FAIL hasChanged")
        print("Expected: {}, Calculated: {}".format(hasChangedExp, hasChangedCalc))

        
def unitTestSequenceSubtraction():
    testSequenceSubtraction(Sequence(4,13),Sequence(6,15),Sequence(4,6))
    testSequenceSubtraction(Sequence(4,13),Sequence(2,9),Sequence(9,13))
    testSequenceSubtraction(Sequence(4,13),Sequence(3,14),Sequence(4,4))
    testSequenceSubtraction(Sequence(4,13),Sequence(1,4),Sequence(4,13))
    testSequenceSubtraction(Sequence(4,13),Sequence(14,17),Sequence(4,13))
    testSequenceSubtraction(Sequence(4,13),Sequence(13,14),Sequence(4,13))
    testSequenceSubtraction(Sequence(4,13),Sequence(12,14),Sequence(4,12))
    testSequenceSubtraction(Sequence(4,13),Sequence(4,14),Sequence(4,4)) #swallow with margin right
    testSequenceSubtraction(Sequence(4,13),Sequence(4,13),Sequence(4,4)) #swallow barely
    testSequenceSubtraction(Sequence(4,13),Sequence(3,13),Sequence(4,4)) #swallow with margin left


def unitTestSequenceSubtraction2():
    testSequenceSubtraction(SeqByLength(4,9),SeqByLength(6,15-6),SeqByLength(4,6-4))
    testSequenceSubtraction(SeqByLength(4,9),SeqByLength(2,9-2),SeqByLength(9,13-9))
    testSequenceSubtraction(SeqByLength(4,9),SeqByLength(3,14-3),SeqByLength(4,4-4))
    testSequenceSubtraction(SeqByLength(4,9),SeqByLength(1,4-1),SeqByLength(4,13-4))
    testSequenceSubtraction(SeqByLength(4,9),SeqByLength(14,17-14),SeqByLength(4,13-4))
    testSequenceSubtraction(SeqByLength(4,9),SeqByLength(13,14-13),SeqByLength(4,13-4))
    testSequenceSubtraction(SeqByLength(4,9),SeqByLength(12,14-13),SeqByLength(4,12-4))
    testSequenceSubtraction(SeqByLength(4,9),SeqByLength(4,14-4),SeqByLength(4,4-4)) #swallow with margin right
    testSequenceSubtraction(SeqByLength(4,9),SeqByLength(4,13-4),SeqByLength(4,4-4)) #swallow barely
    testSequenceSubtraction(SeqByLength(4,9),SeqByLength(3,13-3),SeqByLength(4,4-4)) #swallow with margin left


def tryMatchReduction():
    print("Let's try some reductions:")

    print(60*"-")
    MatchFast(3,7,2).reduceBy(MatchFast(6,7,2))
    MatchOverlap(3,7,2).reduceBy(MatchOverlap(6,7,2))
    MatchSeq(3,7,2).reduceBy(MatchSeq(6,7,2))
    MatchSeqByLength(3,7,2).reduceBy(MatchSeqByLength(6,7,2))
    
    print(60*"-")
    MatchFast(3,7,2).reduceBy(MatchFast(2,12,5))
    MatchOverlap(3,7,2).reduceBy(MatchOverlap(2,12,5))
    MatchSeq(3,7,2).reduceBy(MatchSeq(2,12,5))
    MatchSeqByLength(3,7,2).reduceBy(MatchSeqByLength(2,12,5))
    
    print(60*"-")
    MatchFast(4,4,3).reduceBy(MatchFast(5,7,5))        
    MatchOverlap(4,4,3).reduceBy(MatchOverlap(5,7,5))        
    MatchSeq(4,4,3).reduceBy(MatchSeq(5,7,5)) 
    MatchSeqByLength(4,4,3).reduceBy(MatchSeqByLength(5,7,5))

    print(60*"-")
    MatchFast(4,4,3).reduceBy(MatchFast(1,1,5))
    MatchOverlap(4,4,3).reduceBy(MatchOverlap(1,1,5))
    MatchSeq(4,4,3).reduceBy(MatchSeq(1,1,5))
    MatchSeqByLength(4,4,3).reduceBy(MatchSeqByLength(1,1,5))
            
def main():
    tryMatchReduction()
    
    
    
    
main()    