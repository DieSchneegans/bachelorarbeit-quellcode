# returns number of matching tokens at the given coordinates
def sequenceLength(old, new):
    # dummy implementation, simulate a sequence at (0,1) and nowhere else
    if (old == 0 and new == 1):
        return 2
    else:
        return 0

TokenCountOld = 3
TokenCountNew = 5

# loop through matrix of coordinates diagonally
for offset in range(1 - TokenCountOld, TokenCountNew):
    nextOld = max(0, -offset)
    nextNew = max(0, offset)
    print("starting new diagonal with offset {}".format(offset))
    
    while nextOld < TokenCountOld and nextNew < TokenCountNew:
        print("({},{})".format(nextOld, nextNew))
        
        seqLength = sequenceLength(nextOld, nextNew)
        if (seqLength > 0):
            print("skipping to first token after sequence of length {} found at ({},{})".format(seqLength, nextOld, nextNew))
            nextOld += seqLength
            nextNew += seqLength
        else:
            nextOld += 1
            nextNew += 1