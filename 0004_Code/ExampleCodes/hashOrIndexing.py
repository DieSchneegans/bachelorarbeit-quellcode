import sys

try:
    from pathlib import WindowsPath
except ModuleNotFoundError:
    print("pathlib")
    sys.exit(4)
    
try:
    import copy
except ModuleNotFoundError:
    print("copy")
    sys.exit(4)
             
try:
    from FileModule.SourceBase import SourceBase
except ModuleNotFoundError:
    print("SourceBase")
    sys.exit(4)

path1 = WindowsPath(
    r"C:\Projects\Internal2.0\BSW\ToolDev\StableCode\0005_Tests\0010_3ppReleases\SW35.01")
path2 = WindowsPath(
    r"C:\Projects\Internal2.0\BSW\ToolDev\StableCode\0005_Tests\0010_3ppReleases\SW36.01")


# load files
print("Phase 1")
print("Phase 1.1")
sourcebase1 = SourceBase(0, path1)
sourcebase1.searchFiles()
print("Phase 1.2")
tokens1 = sourcebase1.tokenizeFiles()
print("Phase 2")
print("Phase 2.1")
sourcebase2 = SourceBase(0, path2)
sourcebase2.searchFiles()
print("Phase 2.2")
tokens2 = sourcebase2.tokenizeFiles()

# remove dupletten
print("Phase 3")
tokens_indexing = []
tokens_all = tokens1 + tokens2
print("Phase 4")
for token in tokens_all:
    if token not in tokens_indexing:
        tokens_indexing.append(token)

print("Length of tokens: {}".format(len(tokens_indexing)))
# Hashing old token list
print("Phase 5")
indicesTokensOld = []
for token in tokens1:
    indicesTokensOld.append(token.__hash__())
print("Phase 6")
# Hashing new token list
indicesTokensNew = []
for token in tokens2:
    indicesTokensNew.append(token.__hash__())
print("Phase 7")
tokens_hashing = indicesTokensOld + indicesTokensNew
tokens_hashing = list(dict.fromkeys(tokens_hashing))

# comparison

print("Length of hashableToken: {}".format(len(tokens_hashing)))
print("Kollision: {}".format(len(tokens_hashing)-len(tokens_indexing)))
