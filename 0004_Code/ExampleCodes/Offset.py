
# calculates the next positive offset based on the sign of lastOffset
# returns None if result diagonal above maxPositive
def calcNextPosOffset(lastOffset, maxPositive):
    
    if lastOffset > 0:
        nextOffset = lastOffset + 1
    else:
        nextOffset = -lastOffset + 1
        
    if nextOffset <= maxPositive:
        return nextOffset
    else:
        return None
        
# calculates the next negative offset based on the sign of lastOffset
# returns None if result diagonal below minNegative
def calcNextNegOffset(lastOffset, minNegative):
    
    if lastOffset > 0:
        nextOffset = -lastOffset
    else:
        nextOffset = lastOffset - 1
        
    if nextOffset >= minNegative:
        #print("{} >= {}".format(nextOffset, minNegative))
        return nextOffset
    else:
        #print("negatives depleted")
        return None
        

# calculates the next offset (defined as new - old) based on the last one, with preference of 
#  a) longest diagonal, i.e. abs(offset) is minimal
#  b) positive offset
# returns None if all remaining diagonals are shorther than threshold
def calcNextOffset(lastOffset, lengthOld, lengthNew, threshold):

    maxPos = lengthNew - threshold
    minNeg = threshold - lengthOld
        
    if lastOffset == 0:
        print("maxPos={:2}".format(maxPos))
        print("minNeg={:2}".format(minNeg))
        print("--------------------------")
        
    if maxPos < 0 or minNeg > 0:
        return None        

    # note: None or 42 equals 42, -5 or 7 equals -5, None or None equals None
    if lastOffset > 0:
        return calcNextNegOffset(lastOffset, minNeg) or calcNextPosOffset(lastOffset, maxPos)
    else:
        return calcNextPosOffset(lastOffset, maxPos) or calcNextNegOffset(lastOffset, minNeg)
    

            
            
# -------- test code -----------------

myLengthNew = 6
myLengthOld = 4
myThreshold = 3

print("iterating through diagnonals of {}x{}-matrix that are at least {} element(s) long".format(myLengthNew, myLengthOld, myThreshold))
            
offset = 0            
while offset is not None:
    print("offset {:2}".format(offset))
    offset = calcNextOffset(offset, myLengthOld, myLengthNew, myThreshold)
print("Done!")

        