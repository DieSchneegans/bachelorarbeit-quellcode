cars1 = ["Volvo", "VW", "Mercedes", "BMW", "Audi", "Tesla", "Renault", "peugeot", "Dacia", "Ford", "Saab"]
cars2 = ["Volvo", "VW", "Mercedes", "BMW", "Audi", "Tesla", "Renault", "peugeot", "Dacia", "Ford", "Saab", "Alfa Romeo", "Bugatti", "Chevrolet", "Chrysler", "jeep", "Citoen", "Ferrari"]

matchLength = min(len(cars1), len(cars2))

del cars1[0:(matchLength)]
del cars2[0:(matchLength)]

print(cars1)
print(cars2)
