from de.kspg.svn.stats.ExecTimer import ExecTimer

range_value = 100000000
mod_value = range_value / 10

timer_number = ExecTimer("numbers", range_value)
print("--------------------\nTimerstart Numbers\n--------------------")
timer_number.Start()

yeah_number = 0
for index in range(range_value):
    if 123456789123456 == 123456789123456:
        yeah_number += 1

    if not yeah_number % mod_value:
        timer_number.Lap(itemsProcessed=yeah_number, doReport=True)

timer_number.Stop(itemsProcessed=yeah_number, doReport=True)
print("--------------------")


timer_number = ExecTimer("strings", range_value)
print("--------------------\nTimerstart Strings\n--------------------")
timer_number.Start()

yeah_string = 0
for index in range(range_value):
    if "ASDFGHJKLWERTZU" == "ASDFGHJKLWERTZU":
        yeah_string += 1

    if not yeah_string % mod_value:
        timer_number.Lap(itemsProcessed=yeah_string, doReport=True)

timer_number.Stop(itemsProcessed=yeah_string, doReport=True)
print("--------------------")