tokens_old = [
    "hans",
    "peter",
    "wolf",
    "wurst",
    "affe",
    "wolf",
    "schwein",
    "horst",
    "wolf",
    "plaumenmus",
]
tokens_new = [
    "hans",
    "peter",
    "wolf",
    "schwein",
    "horst",
    "wolf",
    "wurst",
    "affe",
    "wolf",
    "plaumenmus",
]
sequences = []
matches = []


class Match:
    def __init__(self, length, offset):
        self.__length = length
        self.__offset = offset

    def __str__(self):
        return "Offset {}".format(self.__offset)

    def __repr__(self):
        return "Offset {}, Length {}".format(self.__offset, self.__length)

    def removeLength(self):
        self.__length -= 1

    def getLength(self):
        return self.__length

    def getOffset(self):
        return self.__offset


index_old = 0
for index_old in range(len(tokens_old)):
    index_new = 0
    for index_new in range(len(tokens_new)):
        isOffset = False

        for match in matches:
            if index_new - index_old == match.getOffset():
                isOffset = True
                break

        if tokens_old[index_old] == tokens_new[index_new] and not isOffset:
            index_old_temp = index_old + 1
            index_new_temp = index_new + 1

            length = 1

            while (
                len(tokens_old) > index_old_temp
                and len(tokens_new) > index_new_temp
                and tokens_old[index_old_temp] == tokens_new[index_new_temp]
            ):
                index_old_temp += 1
                index_new_temp += 1

                length += 1

            matches.append(Match(length, index_new - index_old))
            sequences.append(
                {
                    "posOld": index_old,
                    "posNew": index_new,
                    "length": length,
                    "offset": index_new - index_old,
                }
            )

    for match in matches:
        match.removeLength()
        if match.getLength() <= 0:
            matches.remove(match)

print("FINISH!")
