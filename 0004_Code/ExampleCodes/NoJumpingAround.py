



# returns true if the tokens are equal
def areTokensEqual(old, new):
    # dummy implementation, simulate a sequence at (0,1) with length 2 and nowhere else
    return (old == 1 and new == 2) or (old == 2 and new == 3)
        
# dummy implementation of sequences.append(Sequence(next_oldVersion, next_newVersion, seqLength))
def memorizeSequence(startOld, startNew, length):
    print("memorized Sequence ({},{},{})".format(startOld, startNew, length))



TokenCountOld = 3
TokenCountNew = 5
seqLength = 0

# loop through matrix of coordinates diagonally
for offset in range(1 - TokenCountOld, TokenCountNew):
    nextOld = max(0, -offset)
    nextNew = max(0, offset)
    print("starting new diagonal with offset {}".format(offset))
    
    # loop through diagonal
    while nextOld < TokenCountOld and nextNew < TokenCountNew:
        
        if areTokensEqual(nextOld, nextNew):
            seqLength += 1
        else:
            if seqLength > 0:
                memorizeSequence(nextOld - seqLength, nextNew - seqLength, seqLength)
                seqLength = 0
        
        print("({},{}), seqLen:{}".format(nextOld, nextNew, seqLength))
        nextOld += 1
        nextNew += 1

    # close existing sequence from previous diagonal
    if seqLength > 0:
        memorizeSequence(nextOld - seqLength, nextNew - seqLength, seqLength)
        seqLength = 0

