from sctokenizer import tokenize_str
from pathlib import WindowsPath

filePath = WindowsPath(r"C:\Projects\ELE-00137\Product\SW1_Application\SwReleases\SW34.02\03_SourceCode\06_MCAL\dsPIC33\Adc\Adc.c")
fileText = filePath.read_text()

tokens = tokenize_str(fileText, 'c')

for token in tokens:
    print(token)
    
print("Length of tokens: {}".format(len(tokens)))