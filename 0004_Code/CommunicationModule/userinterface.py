"""
    Module Userinterface
"""

###################################################################################################
###                                imports                                                      ###
###################################################################################################
import sys

try:
    # Package to create logging messages
    import logging
except ModuleNotFoundError:
    print("logging not installed. Exiting...")
    sys.exit(3)

try:
    # Classes to create a pretty console interface
    from argparse import ArgumentParser, RawTextHelpFormatter
except ModuleNotFoundError:
    logging.error("argparse not installed. Exiting...")
    sys.exit(3)

try:
    # Class to handle paths
    from pathlib import Path
except ModuleNotFoundError:
    logging.error("pathlib.Path not installed. Exiting...")
    sys.exit(3)

###################################################################################################
###                                global variables                                             ###
###################################################################################################
DESCRIPTION = """\
This script calculates the stability index. The stability index is defined by HIS and was modified.
The stability index is the content stability multiply by structure stability.
Content stability is defined as: S/(S+A+D)
Structure stability is defined as: 1-([M-F]/max[1,min(K,L)])

The different parts of the formula:
    S - stable part of code (in this case: tokens in both code versions)
    A - added part of code (in this case: tokens in new version of code)
    D - deleted part of code (in this case: tokens in old version of code)
    M - minimum number of sequences
    F - Number of file couples
    K - number of tokens in the old version of code
    L - number of tokens in the new version of code
"""

EPILOG = """\
Example usage:
python stablecode.py "C:\Projects\SW42.01\A" "C:\Projects\SW42.02\B"
"""

HELP_OLDVERSION = """\
Specifies folder tree to search files of the old software version.
May or may not end with a forward slash.
"""

HELP_NEWVERSION = """\
Specifies folder tree to search files of the new software version.
May or may not end with a forward slash.
"""

HELP_DEBUG = """\
Activate debug logging mode.
Default: False
"""

HELP_SVN = """\
Use the svn mode. In this mode you can give a url to a folder on the svn server and the script checkout this folder first.
Then the script calculates the stability index, the script deletes the checkout folders

Default: False
"""

HELP_THRESHOLD = """\
The threshold argument sets the starting value for the threshold. 
The value set here is used as an upper limit for the removal of matches.

Default: 256
"""

HELP_MINTHRESHOLD = """\
The minimum threshold sets a lower limit for the deletion of matches within file pairs

Default: 16
"""

HELP_DIVISOR = """\
The threshold divisor is used to define the reduction of the threshold value. 
The larger the value, the faster the threshold value falls. 
The smaller the value, the slower the threshold value falls.

Default: 4
"""

HELP_VISUAL = """\
The visualization mode can be used to turn on or off the outputs on the console, which are not part of the logging.

Default: True
"""

###################################################################################################
###                                public classes                                               ###
###################################################################################################

###################################################################################################
###                                private classes                                              ###
###################################################################################################

###################################################################################################
###                                public functions                                             ###
###################################################################################################


def parseArguments():
    """
        Generate arguments by calling script
    """

    # Create ArgumentParser
    parser = ArgumentParser(
        formatter_class=RawTextHelpFormatter, description=DESCRIPTION, epilog=EPILOG
    )

    # Add arguments
    parser.add_argument("oldVersion", type=str, help=HELP_OLDVERSION)
    parser.add_argument("newVersion", type=str, help=HELP_NEWVERSION)
    parser.add_argument("--debug", action="store_true", help=HELP_DEBUG)
    parser.add_argument(
        "-s", "--svn", action="store_true", help=HELP_SVN
    )
    parser.add_argument("-t", "--threshold", type=int,
                        default=256, help=HELP_THRESHOLD)
    parser.add_argument("-m", "--minThreshold",
                        type=int, default=16, help=HELP_MINTHRESHOLD)
    parser.add_argument("-d", "--divisor", type=int,
                        default=4, help=HELP_DIVISOR)
    parser.add_argument(
        "-v", "--visual", action="store_false", help=HELP_VISUAL)

    return parser.parse_args()


def initialize_logger(debug : bool, output_dir : Path = Path().resolve()) -> None:
    """
        Initialize logger -> Logging to console and logging to files
        Source: https://aykutakin.wordpress.com/2013/08/06/logging-to-console-and-file-in-python/
    """
    logger = logging.getLogger()
    logger.setLevel(logging.DEBUG)

    # activate debug logging mode
    if debug:
        mode = logging.DEBUG
    else:
        mode = logging.INFO

    # create console handler and set level to info
    handler = logging.StreamHandler(sys.stdout)
    handler.setLevel(logging.INFO)
    formatter = logging.Formatter("%(levelname)s - %(message)s")
    handler.setFormatter(formatter)
    logger.addHandler(handler)

    # create error file handler and set level to error
    handler = logging.FileHandler(output_dir.joinpath("error.log"), "w")
    handler.setLevel(logging.WARNING)
    formatter = logging.Formatter(
        "%(asctime)s [%(levelname)s] %(message)s", datefmt="%d %b %Y %H:%M:%S"
    )
    handler.setFormatter(formatter)
    logger.addHandler(handler)

    # create info/debug file handler and set level to debug
    handler = logging.FileHandler(output_dir.joinpath("all.log"), "w")
    handler.setLevel(mode)
    formatter = logging.Formatter(
        "%(asctime)s [%(levelname)s] %(message)s", datefmt="%d %b %Y %H:%M:%S"
    )
    handler.setFormatter(formatter)
    logger.addHandler(handler)


###################################################################################################
###                                private functions                                            ###
###################################################################################################

###################################################################################################
###                                main                                                         ###
###################################################################################################
