"""Performs a Monte-Carlo-simulation in order to estimate the probabilities
of certain sector image types created by EEPROM corruption.

$HeadURL: https://svnserver.europe.kspag.de/svn/Projects/Internal2.0/BSW/ToolSupport/Subversion/trunk/04_Software/SvnScripts/src/de/kspg/Faltung.py $  # @IgnorePep8
$LastChangedDate: 2015-11-24 16:08:02 +0100 (Di, 24 Nov 2015) $
$LastChangedBy: CA43420 (Winzer, Wolfgang) $
$Revision: 68802 $
"""


# -----------------------------------------------------------------------------
# CONFIGURATION
# -----------------------------------------------------------------------------


# -----------------------------------------------------------------------------
# CONSTANTS
# -----------------------------------------------------------------------------


import numpy as np
import statsmodels.api as sm  # recommended import according to the docs
import matplotlib.pyplot as plt


REPETITIONS = 100

if __name__ == "__main__":
    print("Hello")
    usample = np.random.randint(800, 810, REPETITIONS)
    # nsample = [0] * REPETITIONS
    nsample = np.random.normal(0, 0.5, REPETITIONS)

    csample = [x + y for x, y in zip(usample, nsample)]
    uecdf = sm.distributions.ECDF(csample)

    x = np.linspace(min(csample), max(csample), num=10000)
    y = uecdf(x)
    plt.step(x, y)

    plt.show()
