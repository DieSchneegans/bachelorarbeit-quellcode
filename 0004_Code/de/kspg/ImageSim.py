"""Performs a Monte-Carlo-simulation in order to estimate the probabilities
of certain sector image types created by EEPROM corruption.

$HeadURL: https://svnserver.europe.kspag.de/svn/Projects/Internal2.0/BSW/ToolSupport/Subversion/trunk/04_Software/SvnScripts/src/de/kspg/ImageSim.py $  # @IgnorePep8
$LastChangedDate: 2015-04-22 13:26:50 +0200 (Mi, 22 Apr 2015) $
$LastChangedBy: CA43420 (Winzer, Wolfgang) $
$Revision: 57452 $
"""

from random import *
from scipy.stats import norm
import matplotlib.pyplot as plt
from de.kspg.svn.stats.Counters import Counters
from de.kspg.svn.stats.ExecTimer import ExecTimer

# -----------------------------------------------------------------------------
# CONFIGURATION
# -----------------------------------------------------------------------------


# Please note that you can scale down the number of experiments by 480.000
# because each experiment creates a corrupted sector
# Example: 15.695.488.545 / 480.000 = ca. 32.000 loops expected to be needed to
# reproduce the error.
NOF_LOOPS = 10 ** 9
FIRST_REPORT = 100
REPORT_FREQ = 1000

DO_USE_TYPICAL_WL = True
DO_USE_EMPIRIC_DELETION = True

# -----------------------------------------------------------------------------
# CONSTANTS
# -----------------------------------------------------------------------------

# sector structure and size
WL_BYTES = 32
WL_SECTORS = 4
SECTOR_BYTES = WL_BYTES * WL_SECTORS

# wordline structure
DATA_BYTES = 30
IDX_CS = 30
IDX_IB = 31

# indicator byte values
IB_PROG_VALUE = 0x81
IB_EMPTY_VALUE = 0x00


class Software:
    OLD = 0
    NEW = 1


SOFTWARES = [Software.OLD, Software.NEW]


class WlType(object):
    EMPTY = "empty"
    VALID = "valid"
    INVALID = "invalid"
    PSEUDO_VALID = "pseudo-valid"
    PSEUDO_EMPTY = "pseudo-empty"
    PSEUDO_EMPTY_ICS = "pseudo-empty-ics"


WLTYPES = [
    WlType.EMPTY,
    WlType.VALID,
    WlType.INVALID,
    WlType.PSEUDO_VALID,
    WlType.PSEUDO_EMPTY,
    WlType.PSEUDO_EMPTY_ICS,
]


class Behaviour:

    INVPROB_SECTOR_CORRUPTION = 480000

    MARCHTHROUGH = None
    TOPSEARCH = None
    USEOLDHAT = None
    DESTRUCTION = None
    CORRUPTION = None
    ALL = []

    def __init__(self, name, descr, probfactor):
        self.name = name
        self.description = descr
        self.baseprob = probfactor * Behaviour.INVPROB_SECTOR_CORRUPTION
        Behaviour.ALL.append(self)

    @staticmethod
    def createConstants():
        Behaviour.MARCHTHROUGH = Behaviour("MARCHTHROUGH", "Found newest wordline", 0)
        Behaviour.TOPSEARCH = Behaviour(
            "TOPSEARCH", "Found newest wordline (2nd try)", 4
        )
        Behaviour.USEOLDHAT = Behaviour("USEOLDHAT", "Found older wordline", 1)
        Behaviour.DESTRUCTION = Behaviour("DESTRUCTION", "eeprom judged defective", 4)
        Behaviour.CORRUPTION = Behaviour("CORRUPTION", "eeprom corrupted", 1)

    @staticmethod
    def getNames():
        return [b.name for b in Behaviour.ALL]


# TODO check how this can be done on definition of class Behaviour
Behaviour.createConstants()


class States:
    NORMAL = 1
    OLDHAT_STARTED = 2
    OLDHAT_COMPLETE = 3
    NOTFOUND_STARTED = 4
    NOTFOUND_COMPLETE = 5
    CORRUPTION_STARTED = 6
    CORRUPTION_COMPLETE = 7


class IbStatus:
    EMPTY = 0
    TRASH = 1
    PROG = 2


class DataStatus:
    EMPTY = 0
    REDUCED = 1
    FULL = 2


# modifies behavior for old and new software
WLTYPE_MAP = {
    Software.OLD: {
        WlType.PSEUDO_EMPTY_ICS: WlType.EMPTY,
        WlType.PSEUDO_EMPTY: WlType.EMPTY,
    },
    Software.NEW: {
        WlType.PSEUDO_EMPTY_ICS: WlType.EMPTY,
        WlType.PSEUDO_EMPTY: WlType.INVALID,
    },
}

TYPICAL_WL = [
    0x00,
    0x00,
    0x0B,
    0x04,
    0x00,
    0x0C,
    0x00,
    0x01,
    0x0C,
    0x00,
    0x02,
    0x00,
    0x08,
    0xF7,
    0x08,
    0x00,
    0x00,
    0x00,
    0x00,
    0x00,
    0x00,
    0x57,
    0x00,
    0x00,
    0x00,
    0x00,
    0x13,
    0x00,
    0x05,
    0x00,
    0xA0,
    0x81,
]


def createValidSector():
    sector = []
    for wl in range(WL_SECTORS):
        if DO_USE_TYPICAL_WL:
            sector.append(TYPICAL_WL)
        else:
            sector.append(createRandomWordline())
    return sector


def createRandomWordline():
    wordline = []
    for byte in range(DATA_BYTES):
        wordline.append(randomDataByte())
    wordline.append(calcChecksum(wordline))
    wordline.append(IB_PROG_VALUE)
    return wordline


def printSector(decodedSector):
    for byte in range(WL_BYTES):
        # print(format(byte, '02'), end='|')
        pass
    print()
    for byte in range(WL_BYTES):
        # print("--", end='+')
        pass
    print()
    for wl in decodedSector:
        printWordline(wl)
    print("Hamming Weight:", hammingWeight(decodedSector))


def printWordline(wordline):
    for byte in wordline:
        pass
        # print(format(byte, '02x'), end='|')
    print()


def randomDataByte():
    return randint(0, 255)  # Inclusive


def calcChecksum(wordline):
    cs = 0
    for byte in wordline:
        cs = cs + byte
    cs = cs % 256
    return cs


def corruptSector(sector):
    hw = hammingWeight(sector)
    bitsToDelete = randomizeDeletedBits(hw)
    # print("bitsToDelete:", bitsToDelete)
    corruptedSector = deleteBits(sector, bitsToDelete)
    return corruptedSector


def hammingWeight(sector):
    hw = 0
    for wl in sector:
        for byte in wl:
            hw = hw + bin(byte).count("1")
    return hw


def randomizeDeletedBits(hw):
    if DO_USE_EMPIRIC_DELETION:
        bitsToDelete = int(round(delBitsEmpirical()[0] * hw))
    else:
        bitsToDelete = randint(0, hw)  # Laplace(0,K)
    return bitsToDelete


def deleteBits(sector, nofBitsToDelete):
    hw = hammingWeight(sector)
    assert nofBitsToDelete <= hw
    encodedSector = bitEncodeSector(sector)
    for i in range(nofBitsToDelete):
        bitToDelete = randint(0, hw - 1)
        # print("deleting {0} of {1}:".format(bitToDelete, hw))
        del encodedSector[bitToDelete]
        hw = hw - 1
    return decodeSector(encodedSector)


def decodeSector(encodedSector):
    decodedSector = []
    # create empty decodedSector
    for i in range(WL_SECTORS):
        wl = [0] * WL_BYTES
        decodedSector.append(wl)
    # add bits
    # print(encodedSector)
    for bitpos in encodedSector:
        relBitPos = bitpos % 8
        relBytePos = (bitpos // 8) % WL_BYTES
        wlIdx = bitpos // (WL_BYTES * 8)
        value = 2 ** (7 - relBitPos)
        # print("Bitpos:", bitpos)
        # print("relBitPos:", relBitPos)
        # print("relBytePos:", relBytePos)
        # print("wlIdx:", wlIdx)
        # print("value:", value)

        decodedSector[wlIdx][relBytePos] = decodedSector[wlIdx][relBytePos] + value

    return decodedSector


def bitEncodeSector(decodedSector):
    encodedSector = []
    bitpos = 0
    for wl in decodedSector:
        for byte in wl:
            binaryByte = format(byte, "8b")
            # print(binaryByte)
            for bit in binaryByte:
                if bit == "1":
                    encodedSector.append(bitpos)
                bitpos = bitpos + 1
    return encodedSector


def wlCategory(wl, wlref):
    """maps a given wordline to one of the following status:
    emtpy, pseudo-empty, pseudo-valid, valid, invalid
    """
    dataStatus, checksum = dataStatusAndCs(wl, wlref)
    ibStatus = calcIbStatus(wl)
    csMatched = isCsMatched(wl, checksum)

    return combineStatus(dataStatus, ibStatus, csMatched)


def dataStatusAndCs(wl, wlref):
    """ calculates data status and checksum """
    checksum = 0
    dataStatus = DataStatus.EMPTY
    for byteIdx in range(DATA_BYTES):
        checksum = checksum + wl[byteIdx]
        # ------------------------------------
        if dataStatus == DataStatus.EMPTY:
            # ------------------------------------
            if wl[byteIdx] > 0:
                if wl[byteIdx] != wlref[byteIdx]:
                    dataStatus = DataStatus.REDUCED
                else:
                    dataStatus = DataStatus.FULL
        # ------------------------------------
        elif dataStatus == DataStatus.FULL:
            # ------------------------------------
            if wl[byteIdx] != wlref[byteIdx]:
                dataStatus = DataStatus.REDUCED
        # ------------------------------------
        else:
            # ------------------------------------
            assert dataStatus == DataStatus.REDUCED
    checksum = checksum % 256
    return dataStatus, checksum


def calcIbStatus(wl):
    if wl[IDX_IB] == IB_PROG_VALUE:
        return IbStatus.PROG
    elif wl[IDX_IB] == IB_EMPTY_VALUE:
        return IbStatus.EMPTY
    else:
        return IbStatus.TRASH


def isCsMatched(wl, checksum):
    return checksum == wl[IDX_CS]


def combineStatus(dataStatus, ibStatus, isCsMatched):
    # Combine all three status to wordline status according to exel chart
    # See Excel-Chart "Fehlertypen.xlsx"
    # print("dataStatus={}, ibStatus={}, isCsMatched={}".format(dataStatus, ibStatus, isCsMatched))
    if ibStatus == IbStatus.EMPTY:
        if dataStatus == DataStatus.EMPTY:
            if isCsMatched:
                wlstatus = WlType.EMPTY
            else:
                wlstatus = WlType.PSEUDO_EMPTY_ICS
        else:
            wlstatus = WlType.PSEUDO_EMPTY
    elif ibStatus == IbStatus.TRASH:
        wlstatus = WlType.INVALID
    elif ibStatus == IbStatus.PROG:
        if isCsMatched:
            if dataStatus == DataStatus.FULL:
                wlstatus = WlType.VALID
            else:
                wlstatus = WlType.PSEUDO_VALID
        else:
            wlstatus = WlType.INVALID
    else:
        assert False

    return wlstatus


def categorizeWordlines(sector, sectorRef, wlTypeCounters):
    wlTypes = []
    for i in range(WL_SECTORS):
        wl = sector[i]
        wlref = sectorRef[i]
        wltype = wlCategory(wl, wlref)
        wlTypes.append(wltype)
        wlTypeCounters.Inc(wltype)
    return wlTypes


def printWordlineCategories(wlTypes):
    i = 0
    for wltype in wlTypes:
        print("Typ WL{0}: {1}".format(i, wltype))
        i = i + 1


def determineBehavior(wlTypes, behaviorCounters):
    for sw in SOFTWARES:
        behaviour = determineBehaviour(wlTypes, sw)
        behaviorCounters[sw].Inc(behaviour.name)


def convertWlTypes(wlTypes, wlTypeMap):
    convertedWlTypes = []
    for wlType in wlTypes:
        if wlType in wlTypeMap:
            convertedWlTypes.append(wlTypeMap[wlType])
        else:
            convertedWlTypes.append(wlType)
    return convertedWlTypes


def determineBehaviour(wlTypes, sw):
    convertedWlTypes = convertWlTypes(wlTypes, WLTYPE_MAP[sw])
    state = States.NORMAL
    """ implements the state machine with the wlTypes being triggers"""

    for wlType in convertedWlTypes:
        # ---------------------------------------
        if state == States.NORMAL:
            # ---------------------------------------
            if wlType == WlType.VALID:
                state = States.OLDHAT_STARTED
            elif wlType == WlType.INVALID:
                state = States.NOTFOUND_STARTED
            elif wlType == WlType.PSEUDO_VALID:
                state = States.CORRUPTION_STARTED
        # ---------------------------------------
        elif state == States.OLDHAT_STARTED:
            # ---------------------------------------
            if wlType == WlType.EMPTY:
                return Behaviour.USEOLDHAT
            elif wlType == WlType.PSEUDO_VALID:
                state = States.CORRUPTION_STARTED
        # ---------------------------------------
        elif state == States.CORRUPTION_STARTED:
            # ---------------------------------------
            if wlType == WlType.EMPTY:
                return Behaviour.CORRUPTION
            elif wlType == WlType.VALID:
                state = States.OLDHAT_STARTED
        # ---------------------------------------
        elif state == States.NOTFOUND_STARTED:
            # ---------------------------------------
            if wlType == WlType.EMPTY:
                if sw == Software.OLD:
                    return Behaviour.DESTRUCTION
                else:
                    return Behaviour.TOPSEARCH
            elif wlType == WlType.VALID:
                state = States.OLDHAT_STARTED
            elif wlType == WlType.PSEUDO_VALID:
                state = States.CORRUPTION_STARTED
        # ---------------------------------------
        else:
            # ---------------------------------------
            assert False

    # no other behavior has been found
    return Behaviour.MARCHTHROUGH


def performExperiment(wlTypeCounters, behaviorCounters):
    sector = createValidSector()
    corruptedSector = corruptSector(sector)
    wlTypes = categorizeWordlines(corruptedSector, sector, wlTypeCounters)
    determineBehavior(wlTypes, behaviorCounters)


def repeatExperiment():
    wlTypeCounters = Counters(WLTYPES)
    names = Behaviour.getNames()
    behaviourCounters = {Software.OLD: Counters(names), Software.NEW: Counters(names)}

    timer = ExecTimer("experiments", NOF_LOOPS)
    timer.Start()
    for loop in range(1, NOF_LOOPS + 1):
        performExperiment(wlTypeCounters, behaviourCounters)
        reportInterm(wlTypeCounters, behaviourCounters, timer, loop)
    timer.Stop(NOF_LOOPS)
    outputCounters(wlTypeCounters, behaviourCounters)


def reportInterm(wlTypeCounters, behaviorCounters, timer, loop):
    if loop == FIRST_REPORT or loop % REPORT_FREQ == 0:
        timer.Lap(loop)
        outputCounters(wlTypeCounters, behaviorCounters)
        showProbabilities(behaviorCounters)


def outputCounters(wlTypeCounters, behaviourCounters):
    print()
    wlTypeCounters.Print("Wordline Types")
    print()
    behaviourCounters[Software.OLD].Print("Behaviours M060400")
    print()
    behaviourCounters[Software.NEW].Print("Behaviours M060500")
    print()


def outputSectorComparison(sector, corruptedSector, wlTypes):
    printSector(sector)
    printSector(corruptedSector)
    printWordlineCategories(wlTypes)


def showProbabilities(behaviourCounters):
    for sw in SOFTWARES:
        showProbabilitiesForSw(behaviourCounters[sw], sw)
        print()


def showProbabilitiesForSw(behaviourCounters, sw):
    print("Occurence expected every x key-offs for SW {}:".format(sw))
    print("--------------------------------------------")
    maxkeylen = str(max([len(b.description) for b in Behaviour.ALL]))
    outmask = "{0:" + maxkeylen + "}: {1:8.1e}"
    for behaviour in Behaviour.ALL:
        if behaviour.baseprob > 0:
            perc = behaviourCounters.Percentage(behaviour.name)
            if perc > 0:
                prob = behaviour.baseprob / perc
            else:
                prob = 0
            print(outmask.format(behaviour.description, prob))


def test():
    reset = uniform(0, 2)
    total = 0
    for i in range(1024):
        death = gauss(0.7, 0.25)
        if death > reset:
            total = total + 1
    print("Survivors: {:4.2%}".format(total / i))


def delBitsEmpirical():
    mu = 0.7
    sigma = 0.25
    resettime = uniform(0, 2)
    bitsdeleted = norm.cdf((resettime - mu) / sigma)
    return bitsdeleted, resettime


def testPlotting():
    xval = []
    yval = []
    for i in range(1000):
        y, x = delBitsEmpirical()
        xval.append(x)
        yval.append(y)
    plotXY(xval, yval)


def plotXY(xvalues, yvalues):
    plt.plot(xvalues, yvalues, "ro")
    plt.show()


if __name__ == "__main__":
    print("Monte-Carlo-Simulation to estimate probability of corrupted sector images")
    testPlotting()
    repeatExperiment()
