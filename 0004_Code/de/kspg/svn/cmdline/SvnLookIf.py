import os
import logging

USE_MOCKS = False
MOCK_PROPGET = "PythonHooks\de\kspg\svn\cmdline\mockPropget.txt"
MOCK_CHANGED = "PythonHooks\de\kspg\svn\cmdline\mockChanged.txt"

log = logging.getLogger("Monitor")


def openFile(file):
    try:
        return open(file, "r")
    except FileNotFoundError:
        log.error("could not find %s in path %s", file, os.getcwd())
        raise


def SvnlookPropget(repo, prop, path):
    """ If the property exist, function returns the property value. 
        If the property consists of several lines, they will be concatenated.
        If the property does not exist, function returns None
        If you get "", this means the property exists but the value is empty.
    
    """
    E_PROP_NOT_FOUND = "E200017"
    propgetCommand = " ".join(["svnlook propget", repo, prop, path, "2>&1"])

    if USE_MOCKS:
        stdout = openFile(MOCK_PROPGET)
    else:
        stdout = os.popen(propgetCommand)

    cmdResult = stdout.read().strip()
    if E_PROP_NOT_FOUND in cmdResult:
        return None
    else:
        return cmdResult


def SvnLookChanged(repo, transaction):
    """ generates a dictionary path:changetype from the given transaction"""
    SEP = "   "  # yes, three spaces, this is the way svn look delivers it
    CMD_PATTERN = "svnlook changed -t {0} {1}"
    command = CMD_PATTERN.format(transaction, repo)
    if USE_MOCKS:
        stdout = openFile(MOCK_CHANGED)
    else:
        stdout = os.popen(command)

    # read out stdout and store each line in a pair (changetype, path)
    pairs = [line.split(SEP, 1) for line in stdout if SEP in line]
    changeDict = dict((pair[1].strip(), pair[0].strip()) for pair in pairs)

    return changeDict
