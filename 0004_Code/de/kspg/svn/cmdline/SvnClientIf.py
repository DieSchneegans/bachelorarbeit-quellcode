import os
import logging

log = logging.getLogger("Monitor")


def SvnInfo(fullPath):
    """ calls svn info on the given path and creates a dictionary from the output"""
    SEP = ":"
    svninfoCommand = "svn info " + fullPath
    stdout = os.popen(svninfoCommand)
    # store a list of pairs and discard lines that do not contain a pair
    pairs = [line.split(SEP, 1) for line in stdout if SEP in line]
    infoDic = dict((pair[0].strip(), pair[1].strip()) for pair in pairs)
    return infoDic


def SvnPropset(fullPath, prop, value):
    """ sets a property prop->value to the specified file or folder"""
    CMD_PATTERN = "svn propset {} '{}' '{}'"
    command = CMD_PATTERN.format(prop, value, fullPath)
    log.debug("command %s", command)
    stdout = os.popen(command)
    result = " ".join(stdout)
    log.debug("result %s", result)
