"""
Created on 05.02.2014

@author: ca43420
"""
import csv
import subprocess
import inspect
import os


class CaUserLut:
    """
    classdocs
    """

    # Will be used as real name for
    # colleagues that left PG some time ago
    UNRESOLVED_CAUSER = "UnresolvedCaUser"

    # Delimiter used in CSV file. It can't be ',' as comma is used within the real name
    CSV_DELIMITER = ";"

    # Filename of the default LUT
    LUTNAME = "CaUserLookupTable.csv"

    def __init__(self, filename=None, allowAd=False):
        """
        Constructor
        """
        self.allowAd = allowAd
        self.__Lut = dict()
        self.__AddedUsers = dict()
        self.HasChanged = False
        if filename == None:
            self.__Filename = self._GetDefaultLutPath()
        else:
            self.__Filename = filename
        self._Load()

    def __del__(self):
        if self.HasChanged:
            self._Save()

    def PrettyPrint(self):
        for a, b in sorted(self.__Lut.items()):
            print(a, "->", b)

    def PrintNewUsers(self):
        nofUsers = len(self.__AddedUsers)
        print("{} user(s) have been added".format(nofUsers))
        for a, b in sorted(self.__AddedUsers.items()):
            print(a, "->", b)

    def _Load(self):
        print("Loading RAM-LUT from {0}".format(self.__Filename))
        self.__Lut = dict()
        try:
            with open(self.__Filename, "r", newline="") as f:
                reader = csv.reader(f, delimiter=CaUserLut.CSV_DELIMITER)
                for row in reader:
                    self.AddEntry(row[0], row[1], isNew=False)
                f.close()
        except IOError:
            print("File not found: {0}".format(self.__Filename))
            print("No entries will be imported.")
            print("Starting with empty RAM-LUT.")

    def _Save(self):
        print("Saving RAM-LUT to {0}".format(self.__Filename))
        self.PrettyPrint()
        self.HasChanged = False
        with open(self.__Filename, "w", newline="") as f:
            writer = csv.writer(f, delimiter=CaUserLut.CSV_DELIMITER)
            writer.writerows(sorted(self.__Lut.items()))
            f.close()

    def AddEntry(self, caUser, realName, isNew=True):
        """ adds an entry caUser->realName to the lut dictionary. Existing
        entries will be replaced """
        print("Adding {0} as {1} to RAM-LUT".format(caUser, realName))
        self.__Lut[caUser] = realName
        self.HasChanged = True
        if isNew:
            self.__AddedUsers[caUser] = realName

    def LookupRealName(self, caUser):
        print("Looking up user {0} in RAM-LUT".format(caUser))

        # enter as unresolved if missing
        if not caUser in self.__Lut:
            self.AddEntry(caUser, self.UNRESOLVED_CAUSER)

        # resolve unresolved
        if self.__Lut[caUser] == self.UNRESOLVED_CAUSER and self.allowAd:
            self.AddEntry(caUser, self.__LookupRealNameInActiveDirectory(caUser))

        return self.__Lut[caUser]

    def __LookupRealNameInActiveDirectory(self, caUser):
        print("Looking up user {0} in Active Directory".format(caUser))
        STR_CMD_TEMPLATE = "wmic useraccount where (domain='EUROPE' and name='{0}') get FullName | findstr /V FullName"
        strCmd = STR_CMD_TEMPLATE.format(caUser)
        strFullName = subprocess.check_output(strCmd, shell=True).decode().rstrip()
        # note that a negative response of wmic will be routed to stderr but only
        # stdout has been captured. This means strFullName will be empty.
        if strFullName:
            return strFullName
        else:
            print("causer {0} not found in AD".format(caUser))
            return self.UNRESOLVED_CAUSER

    def _GetDefaultLutPath(self):
        file = inspect.getfile(inspect.currentframe())
        folder = os.path.dirname(os.path.abspath(file))
        return os.path.join(folder, self.LUTNAME)

    def LookoupAllUnresolvedInActiveDirectory(self):
        if self.allowAd:
            print("Trying to resolve all unresolved caUsers")
            unresolvedCaUsers = [
                caUser
                for caUser in self.__Lut.keys()
                if self.__Lut[caUser] == self.UNRESOLVED_CAUSER
            ]
            print("Found {0} unresolved caUsers".format(len(unresolvedCaUsers)))
            for caUser in unresolvedCaUsers:
                realName = self.__LookupRealNameInActiveDirectory(caUser)
                self.AddEntry(caUser, realName)
        else:
            print("No active directory allowed")
