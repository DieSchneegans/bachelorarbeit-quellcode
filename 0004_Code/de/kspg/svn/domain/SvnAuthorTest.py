"""
$HeadURL: https://svnserver.europe.kspag.de/svn/Projects/Internal2.0/BSW/ToolSupport/Subversion/trunk/04_Software/SvnScripts/src/de/kspg/svn/domain/SvnAuthorTest.py $  # @IgnorePep8
$LastChangedDate: 2014-05-07 14:05:47 +0200 (Mi, 07 Mai 2014) $
$LastChangedBy: CA43420 (Winzer, Wolfgang) $
$Revision: 31128 $
"""
import unittest

from de.kspg.svn.domain.CaUserLut import CaUserLut
from de.kspg.svn.domain.SvnAuthor import SvnAuthor
import pysvn


class SvnAuthorTest(unittest.TestCase):

    REPO = "https://localhost/svn/projects"

    REV_EXPANDED = 22230
    EXP_CAUSER = "CA43420"
    EXP_REALNAME = "Winzer, Wolfgang"
    EXP_OLDAUTHOR = "CA43420 (Winzer, Wolfgang)"
    EXP_AUTHOR = "CA43420 (Winzer, Wolfgang)"

    REV_UNEXPANDED = 22494
    UNEXP_CAUSER = "CA45020"
    UNEXP_REALNAME = "Kunkel, Marco"
    UNEXP_OLDAUTHOR = "CA45020"
    UNEXP_AUTHOR = "CA45020 (Kunkel, Marco)"

    REV_NEW_MALFORMED = 5
    NMAL_CAUSER = SvnAuthor.MALFORMED_CAUSER
    NMAL_OLDAUTHOR = "VisualSVN Server (Nicht aufloesbar)"
    NMAL_REALNAME = NMAL_OLDAUTHOR
    NMAL_AUTHOR = "Malformed: " + NMAL_OLDAUTHOR

    REV_KNOWN_MALFORMED = 22427
    KMAL_CAUSER = SvnAuthor.MALFORMED_CAUSER
    KMAL_OLDAUTHOR = "Malformed: Bill Gates"
    KMAL_REALNAME = "Bill Gates"
    KMAL_AUTHOR = "Malformed: Bill Gates"

    def ssl_server_trust_prompt(self, trust_dict):
        # just say everything is fine, thank you
        return 0xFF, 1, True

    def SetupSvnClient(self):
        client = pysvn.Client()
        client.callback_ssl_server_trust_prompt = self.ssl_server_trust_prompt
        return client

    def setUp(self):
        print("--- Setting up ---")
        self.client = self.SetupSvnClient()
        # For this tests, we will lookup all caUsers in the ad, the lut can be
        # empty
        self.lut = CaUserLut("SvnAuthorTestLut.csv", allowAd=True)
        print("--- Setup complete ---")

    def tearDown(self):
        print("--- Tearing down ---")
        print("--- Tear down complete ---")

    def test01ReadExpandedAuthor(self):
        svnAuthor = SvnAuthor(self.REPO, self.client, self.REV_EXPANDED, self.lut)
        print(svnAuthor.GetOldAuthorString())
        print(svnAuthor.GetAuthorString())
        self.assertEqual(svnAuthor.GetCaUser(), self.EXP_CAUSER)
        self.assertEqual(svnAuthor.GetRealName(), self.EXP_REALNAME)
        self.assertEqual(svnAuthor.GetOldAuthorString(), self.EXP_OLDAUTHOR)
        self.assertEqual(svnAuthor.GetAuthorString(), self.EXP_AUTHOR)
        self.assertFalse(svnAuthor.IsMalformed())
        self.assertFalse(svnAuthor.HasChanged())

    def test02ReadUnexpandedAuthor(self):
        """Precondition: Author of revision self.REV_UNEXPANDED is still
        unexpanded. If you have repaired the repository, this test will fail
        """
        svnAuthor = SvnAuthor(self.REPO, self.client, self.REV_UNEXPANDED, self.lut)
        print(svnAuthor.GetOldAuthorString())
        print(svnAuthor.GetAuthorString())
        self.assertEqual(svnAuthor.GetCaUser(), self.UNEXP_CAUSER)
        self.assertEqual(svnAuthor.GetRealName(), self.UNEXP_REALNAME)
        self.assertEqual(svnAuthor.GetOldAuthorString(), self.UNEXP_OLDAUTHOR)
        self.assertEqual(svnAuthor.GetAuthorString(), self.UNEXP_AUTHOR)
        self.assertFalse(svnAuthor.IsMalformed())
        self.assertTrue(svnAuthor.HasChanged())

    def test03ReadNewMalformedAuthor(self):
        svnAuthor = SvnAuthor(self.REPO, self.client, self.REV_NEW_MALFORMED, self.lut)
        self.assertEqual(svnAuthor.GetCaUser(), self.NMAL_CAUSER)
        self.assertEqual(svnAuthor.GetRealName(), self.NMAL_REALNAME)
        self.assertEqual(svnAuthor.GetOldAuthorString(), self.NMAL_OLDAUTHOR)
        self.assertEqual(svnAuthor.GetAuthorString(), self.NMAL_AUTHOR)
        self.assertTrue(svnAuthor.IsMalformed())
        self.assertTrue(svnAuthor.HasChanged())

    def test04ReadKnownMalformedAuthor(self):
        """Precondition: The author of self.REV_KNOWN_MALFORMED is really
        malformed in you test repository. If not, this test will fail.
        """
        svnAuthor = SvnAuthor(
            self.REPO, self.client, self.REV_KNOWN_MALFORMED, self.lut
        )
        self.assertTrue(svnAuthor.IsMalformed())
        self.assertEqual(svnAuthor.GetCaUser(), self.KMAL_CAUSER)
        self.assertEqual(svnAuthor.GetRealName(), self.KMAL_REALNAME)
        self.assertEqual(svnAuthor.GetOldAuthorString(), self.KMAL_OLDAUTHOR)
        self.assertEqual(svnAuthor.GetAuthorString(), self.KMAL_AUTHOR)
        self.assertFalse(svnAuthor.HasChanged())

    def test05CreationNakedFails(self):
        with self.assertRaises(Exception):
            svnAuthor = SvnAuthor()
            del svnAuthor

    def test06CreationMissingClientFails(self):
        with self.assertRaises(Exception):
            svnAuthor = SvnAuthor(
                repo=self.REPO, rev=self.REV_KNOWN_MALFORMED, lut=self.lut
            )
            del svnAuthor

    def test07CreationMissingRevFails(self):
        with self.assertRaises(Exception):
            svnAuthor = SvnAuthor(repo=self.REPO, client=self.client, lut=self.lut)
            del svnAuthor

    def test08CreationMissingRepoFails(self):
        with self.assertRaises(Exception):
            svnAuthor = SvnAuthor(
                client=self.client, rev=self.REV_KNOWN_MALFORMED, lut=self.lut
            )
            del svnAuthor

    def test09CreationMissingLutOk(self):
        """This is basically a copy of test01ReadExpandedAuthor but without
        a supporting lut
        """
        svnAuthor = SvnAuthor(self.REPO, self.client, self.REV_EXPANDED)
        print(svnAuthor.GetOldAuthorString())
        print(svnAuthor.GetAuthorString())
        self.assertEqual(svnAuthor.GetCaUser(), self.EXP_CAUSER)
        self.assertEqual(svnAuthor.GetRealName(), self.EXP_REALNAME)
        self.assertEqual(svnAuthor.GetOldAuthorString(), self.EXP_OLDAUTHOR)
        self.assertEqual(svnAuthor.GetAuthorString(), self.EXP_AUTHOR)
        self.assertFalse(svnAuthor.IsMalformed())
        self.assertFalse(svnAuthor.HasChanged())


if __name__ == "__main__":
    # import sys;sys.argv = ['', 'SvnAuthorTest.testName']
    unittest.main()
