"""
$HeadURL: https://svnserver.europe.kspag.de/svn/Projects/Internal2.0/BSW/ToolSupport/Subversion/trunk/04_Software/SvnScripts/src/de/kspg/svn/domain/SvnAuthor.py $  # @IgnorePep8
$LastChangedDate: 2014-05-07 14:05:47 +0200 (Mi, 07 Mai 2014) $
$LastChangedBy: CA43420 (Winzer, Wolfgang) $
$Revision: 31128 $
"""

import re
import pysvn


class SvnAuthor:
    """
    Reflects an author of a specific revision in a specific svn repository.
    On construction, the author is loaded from repository. The class will
    evaluate the author to expanded/unexpanded, wellformed/malformed.
    Wellformed unexpanded authors will be expanded using a lookup table (which
    in turn may be backed up by the active directory but this is not our
    business here).
    If the author is changed in any way, he can be written to the server again.

    Depending on the configuration of this class, expanded authors can be
    expanded differently. This will happen if the parse pattern doesn't match
    the combine pattern.
    """

    # Will be appended to authors that do not contain a CaUser
    MALFORMED_CAUSER = "MalformedCaUser"

    # colleagues that left PG some time ago
    UNRESOLVED_AUTHOR = "UnresolvedCaUser"
    UNRESOLVED_AUTHOR_LEGACY = "UnknownCaUser"

    COMBINE_PATTERN_WELLFORMED = "{0} ({1})"
    COMBINE_PATTERN_MALFORMED = "Malformed: {0}"

    PARSE_PATTERN_KNOWN_MALFORMED = "^Malformed: (.*)$"
    PARSE_PATTERN_EXTENDED = "^(\w+) \((.*)\)$"
    PARSE_PATTERN_CAUSERONLY = "^(\w+)$"

    def __init__(self, repo=None, client=None, rev=None, lut=None, txn=None):
        errmsg = """Supply either a transaction or a combination of client,
        repo and revision.
        """
        if txn is None and (client is None or rev is None or repo is None):
            raise Exception(errmsg)
        self.repo = repo
        self.client = client
        self.revision = rev
        self.txn = txn
        self.oldAuthorString = ""
        self.newAuthorString = ""
        self.lutNoAd = lut

        self._ReadFromServer()

    @classmethod
    def CreateOnClient(cls, repo, client, rev, lut):
        return cls(repo=repo, client=client, rev=rev, lut=lut)

    @classmethod
    def CreateOnServer(cls, txn, rev, lut):
        return cls(lut=lut, txn=txn, rev=rev)

    def GetCaUser(self):
        return self.caUser

    def GetRealName(self):
        return self.realName

    def HasChanged(self):
        return self.oldAuthorString != self.newAuthorString

    def IsMalformed(self):
        return self.caUser == self.MALFORMED_CAUSER

    def GetOldAuthorString(self):
        return self.oldAuthorString

    def GetAuthorString(self):
        return self.newAuthorString

    def _ReadFromServer(self):
        # print("Reading CaUser from revision {0}".format(self.revision))
        if self.txn:
            self.oldAuthorString = self.txn.revpropget("svn:author")
        else:
            revObj = pysvn.Revision(
                pysvn.opt_revision_kind.number, self.revision
            )  # @UndefinedVariable
            self.oldAuthorString = self.client.revpropget(
                "svn:author", self.repo, revObj
            )[1]

        self.__ParseOldAuthorString()
        if (
            self.realName == self.UNRESOLVED_AUTHOR
            or self.realName == self.UNRESOLVED_AUTHOR_LEGACY
        ) and self.lutNoAd:
            self.__LookupRealNameInLut()

    def WriteToServer(self):
        if self.HasChanged():
            print(
                "Setting user to {0} in revision {1}".format(
                    self.GetAuthorString(), self.revision
                )
            )
            if self.txn:
                self.txn.revpropset("svn:author", self.GetAuthorString())
            else:
                revObj = pysvn.Revision(
                    pysvn.opt_revision_kind.number, self.revision
                )  # @UndefinedVariable
                self.client.revpropset(
                    "svn:author", self.GetAuthorString(), self.repo, revObj, force=False
                )

    def __CombineWellformed(self):
        self.newAuthorString = self.COMBINE_PATTERN_WELLFORMED.format(
            self.caUser, self.realName
        )

    def __CombineMalformed(self):
        self.newAuthorString = self.COMBINE_PATTERN_MALFORMED.format(self.realName)

    def __ParsesAsKnownMalformed(self):
        match = re.match(self.PARSE_PATTERN_KNOWN_MALFORMED, self.oldAuthorString)
        if match:
            self.caUser = self.MALFORMED_CAUSER
            self.realName = match.group(1)
            self.__CombineMalformed()
            return True
        else:
            return False

    def __ParsesAsUnextended(self):
        match = re.match(self.PARSE_PATTERN_CAUSERONLY, self.oldAuthorString)
        if match:
            self.caUser = match.group(1).upper()
            self.realName = self.UNRESOLVED_AUTHOR
            self.__CombineWellformed()
            return True
        else:
            return False

    def __ParsesAsExtended(self):
        match = re.match(self.PARSE_PATTERN_EXTENDED, self.oldAuthorString)
        if match:
            self.caUser = match.group(1).upper()
            self.realName = match.group(2)
            self.__CombineWellformed()
            return True
        else:
            return False

    def __ParseNewMalformed(self):
        print("Malformed author detected.")
        self.caUser = self.MALFORMED_CAUSER
        self.realName = self.oldAuthorString
        self.__CombineMalformed()

    def __ParseOldAuthorString(self):
        if not self.__ParsesAsKnownMalformed():
            if not self.__ParsesAsUnextended():
                if not self.__ParsesAsExtended():
                    self.__ParseNewMalformed()

    def __LookupRealNameInLut(self):
        self.realName = self.lutNoAd.LookupRealName(self.caUser)
        self.__CombineWellformed()
