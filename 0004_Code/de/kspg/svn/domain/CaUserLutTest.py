"""
Created on 18.02.2014

@author: ca43420
"""
import unittest
import os
from de.kspg.svn.domain.CaUserLut import CaUserLut


class CaUserLutTest(unittest.TestCase):

    TEST_LUT_NOAD = "TestLutNoAd.csv"
    TEST_LUT_WITHAD = "TestLutWithAd.csv"
    # resolved user in csv
    EXISTING_CAUSER = "ca43420"
    EXISTING_REALNAME = "Winzer, Wolfgang"
    # unresolved user in csv
    EX_UNRES_CAUSER = "CA32780"
    EX_UNRES_REALNAME = "Bayer, Martin"
    # user not in csv
    NEW_CAUSER = "CA15850"
    NEW_REALNAME = "Gommers, Hans-Georg"

    def setUp(self):
        """Creates a lut without and another lut with ad support
        for use in the test cases. The luts are created empty and fed with
        defined data to make the tests reproducable.
        """
        print("--- Setting up ---")
        if os.path.exists(self.TEST_LUT_NOAD):
            os.remove(self.TEST_LUT_NOAD)
        self.lutNoAd = CaUserLut(self.TEST_LUT_NOAD)
        self.lutNoAd.AddEntry(self.EXISTING_CAUSER, self.EXISTING_REALNAME)
        self.lutNoAd.AddEntry(self.EX_UNRES_CAUSER, self.lutNoAd.UNRESOLVED_CAUSER)

        self.lutWithAd = CaUserLut(self.TEST_LUT_WITHAD, allowAd=True)
        self.lutWithAd.AddEntry(self.EXISTING_CAUSER, self.EXISTING_REALNAME)
        self.lutWithAd.AddEntry(self.EX_UNRES_CAUSER, self.lutWithAd.UNRESOLVED_CAUSER)
        print("--- Setup complete ---")

    def tearDown(self):
        print("--- Tearing down ---")
        print("--- Tear down complete ---")

    def test01PrettyPrint(self):
        self.lutNoAd.PrettyPrint()

    def test02LookupExisting(self):
        self.assertEqual(
            self.lutNoAd.LookupRealName(self.EXISTING_CAUSER), self.EXISTING_REALNAME
        )

    def test03AddEntry(self):
        CAUSER = "blitz"
        REALNAME = "donner"
        self.lutNoAd.AddEntry(CAUSER, REALNAME)
        lookedUpRealName = self.lutNoAd.LookupRealName(CAUSER)
        self.assertEqual(lookedUpRealName, REALNAME, "Missmaetsch!")

    def test04FindUserViaAd(self):
        realName = self.lutWithAd.LookupRealName(self.NEW_CAUSER)
        self.assertEqual(realName, self.NEW_REALNAME)

    def test05ResolveExistingUserOnLookup(self):
        realName = self.lutWithAd.LookupRealName(self.EX_UNRES_CAUSER)
        self.assertEqual(realName, self.EX_UNRES_REALNAME)

    def test06SaveRetainsNewEntry(self):
        CAUSER = "pech"
        REALNAME = "schwefel"
        lut1 = CaUserLut(self.TEST_LUT_NOAD)
        self.assertEqual(
            lut1.LookupRealName(CAUSER), lut1.UNRESOLVED_CAUSER, "lutNoAd is not empty!"
        )
        lut1.AddEntry(CAUSER, REALNAME)
        # trigger garbage collection and thus self.lut1.__del__()
        lut1 = None

        lut2 = CaUserLut(self.TEST_LUT_NOAD)
        realName = lut2.LookupRealName(CAUSER)
        self.assertEqual(realName, REALNAME)

    def test07ResolveAll(self):
        self.lutWithAd.LookoupAllUnresolvedInActiveDirectory()
        realName = self.lutWithAd.LookupRealName(self.EX_UNRES_CAUSER)
        self.assertEqual(realName, self.EX_UNRES_REALNAME)

    def test08FindDefaultLut(self):
        lutpath = self.lutWithAd._GetDefaultLutPath()
        print(lutpath)
        self.assertTrue(os.path.exists(lutpath))

    def test09CreateLutWithDefaultFile(self):
        lut = CaUserLut()
        realname = lut.LookupRealName(self.EXISTING_CAUSER)
        self.assertEqual(realname, self.EXISTING_REALNAME)


if __name__ == "__main__":
    unittest.main()
