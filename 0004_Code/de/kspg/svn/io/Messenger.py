import sys
import logging
import os


def passTransaction():
    """ stops the script and signals the svn server go on with the commit/lock/propchange """
    sys.exit(0)


def failTransaction():
    """ stops the script and signals the svn server to stop the commit/lock/propchange """
    sys.exit(1)


def createMonitoringLogger(hookName, repopath):
    """ Provides a logger that writes both to stdout and file for use by dev
    and admin. The log file will be located in the current working directory
    which, if triggered by the svn server will be the installation directory
    of the VisualSVNServer (e.g. C:\Spezial\VisualSVNServer)
    """
    logger = logging.getLogger(hookName)
    logger.setLevel(logging.DEBUG)

    # formatter to be used by both handlers
    formatter = logging.Formatter("%(asctime)s %(module)s %(levelname)-5s: %(message)s")

    # adding a handler for looging to file
    # print("Current directory: {}".format(os.getcwd()), file=sys.stderr)
    filepath = os.path.join(repopath, "hooks", "hook.log")
    fileHandler = logging.FileHandler(filepath)
    fileHandler.setFormatter(formatter)
    logger.addHandler(fileHandler)

    # adding a handler for logging to console (stdout)
    streamHandler = logging.StreamHandler(sys.stdout)
    streamHandler.setFormatter(formatter)
    logger.addHandler(streamHandler)

    return logger


def createSvnLogger(hookName):
    logger = logging.getLogger(hookName)
    formatter = logging.Formatter("%(module)s %(levelname)-5s: %(message)s")
    # by svn's hook script convention, only stderr is sent back in response.
    handler = logging.StreamHandler(sys.stderr)
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    logger.setLevel(logging.DEBUG)
    return logger
