:: =============================================================================
:: Repacks xlsx, docx, etc. without compression.
:: This will have the following effects
:: 1) The file will grow on your harddisk
:: 2) The deltas stored by SVN will be reduced drastically
:: 3) Keyword substitution will work but ruin the CRC of one file, making
::    the office application think it needs repairs.
:: If Microsoft was giving us the possibility to store xlsx/docx with
:: compression method NoCompression, this script would not be necessary.
::
:: PARAMETERS
::  %1 - Filepath
::
:: SVN KEYWORDS
::  $HeadURL: https://svnserver.europe.kspag.de/svn/Projects/Internal2.0/BSW/ToolSupport/Subversion/trunk/04_Software/SvnScripts/src/de/kspg/svn/thook/InflateOpenXML.bat $
::  $LastChangedDate: 2014-04-29 13:47:47 +0200 (Di, 29 Apr 2014) $
::  $LastChangedBy: CA43420 (Winzer, Wolfgang) $
::  $Revision: 30807 $
:: =============================================================================
@echo off

set ZIP_EXE=7z.exe
set TMP_DIR=C:\Temp\UOX

IF "%1"=="" GOTO E_MISSING_PARAM1

rd /S /Q %TMP_DIR%

echo uncompressing
%ZIP_EXE% x %1 -o%TMP_DIR%

del %1

echo recompressing
%ZIP_EXE% a -tzip -mx0 %1 %TMP_DIR%\*

GOTO END

:E_MISSING_PARAM1
echo Please specify a file path
GOTO USAGE


:E_UNKNOWN
echo An unknown error has occured.
GOTO USAGE

:USAGE
echo Example usage: %0 C:\projects\myProject\rtm.xlsx

:END