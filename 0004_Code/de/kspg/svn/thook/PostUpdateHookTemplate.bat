@echo off
1>con (
echo %0:
echo PATH: %1
echo DEPTH: %2
echo REVISION: %3
echo ERROR: %4
echo CWD: %5
echo.
echo The following paths were updated:
echo ---------------------------------
type %1
echo.
echo Errors reported by svn update:
echo ---------------------------------
type %4

pause < con
)