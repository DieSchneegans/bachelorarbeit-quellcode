"""
Created on 10.03.2014

@author: ca43420
"""

import sys
import pysvn
from de.kspg.svn.domain.CaUserLut import CaUserLut


PATH_SEP = "\\"
FILE_LUT_CSV = "CaUserLookupTable.csv"


def main():

    # copy hook arguments to variables
    lockPath = sys.argv[1]
    lockOwner = sys.argv[2]

    repoPath = "dummy"
    fileWithPath = PathToLut(repoPath)

    client = SetupSvnClient()

    existingLock = client.list(
        lockPath,
        peg_revision=pysvn.Revision(
            opt_revision_kind.unspecified
        ),  # @UndefinedVariable
        revision=pysvn.Revision(pysvn.opt_revision_kind.head),  # @UndefinedVariable
        recurse=False,
        dirent_fields=pysvn.SVN_DIRENT_ALL,
        fetch_locks=True,
        depth=pysvn.depth.unknown,
    )[
        1
    ]  # @UndefinedVariable

    owner = existingLock.owner

    # get existing lock comment
    # store existing lock token
    # extend user
    #
    # use svn (force) lock to recreate lock,
    #    1) passing the lock token in the comment: TOKEN: ABCDEFG
    #    2) passing new author as part of the comment
    # another pre-commit lock will pick up, extract the lock token from the comment and pass it to stdout,
    #    recreating the token in the new lock
    #

    # Get 'CA-USER'
    lut = CaUserLut(fileWithPath)
    realName = lut.LookupRealName(lockOwner)
    strNewOwner = CombinedName(lockOwner, realName)

    # Set new lock owner
    client.lock(lockPath, existingLock.comment, force=True)


def PathToLut(repoPath):
    parentDir = repoPath.rpartition(PATH_SEP)[0] + PATH_SEP
    pathToLut = parentDir + FILE_LUT_CSV
    return pathToLut


def CombinedName(caUser, realName):
    return "{0} ({1})".format(caUser, realName)


def ssl_server_trust_prompt(trust_dict):
    """Callback for pysvn.Client if svn lib complains about certificate.
       Emulates positive user feedback.
    """
    # just say everything is fine, thank you
    return 0xFF, 1, True


def SetupSvnClient():
    "Provides a pysvn.client."
    client = pysvn.Client()
    client.callback_ssl_server_trust_prompt = ssl_server_trust_prompt
    return client


if __name__ == "__main__":
    main()
