"""
Created on 25.09.2013

@author:         CA50380
@note:           Module restricts transaction if it refers to a 'tag'.
Reference at https://svnserver.europe.kspag.de/svn/internal/...
KSPG_Konfigurationsmanagement/trunk/08_ServerAdministration/...
Untersuchungen/P051-TagChange/ReqsTagChange.txt
"""

import sys, subprocess, re

CONST_PASSED = 0
CONST_FAILED = 1
CONST_REPOSITORY = 1
CONST_TRANSACTION = 2
CONST_STR_CHANGE = "TAG-CHANGE:"
CONST_STR_TAGS_PATH = "/tags/[^/]+/[^/]+"
CONST_STR_TAG_ADD = "^A+"
CONST_STR_FAILED = b'SVN-Admin: You are trying to change a tag. This is not encouraged. If you absolutely must change the tag start your commit message with the string "TAG-CHANGE:"'
# CONST_STR_ERROR     = b'An error occured while checking your transaction, please contact a SVN-Admin.'


def main():
    # Get log-message
    proc = subprocess.Popen(
        [
            "svnlook",
            "log",
            sys.argv[CONST_REPOSITORY],
            "-t",
            sys.argv[CONST_TRANSACTION],
        ],
        stdout=subprocess.PIPE,
    )
    out, err = proc.communicate()
    del err
    str_out = out.decode()
    # D1: F4: "TAG-CHANGE:" string in log-message AND at beginning of log-message?
    tagchange = re.compile(CONST_STR_CHANGE)
    found = tagchange.match(str_out)
    if found != None:
        proc.kill()
        exit(code=CONST_PASSED)
    # Get transaction content
    proc = subprocess.Popen(
        [
            "svnlook",
            "changed",
            sys.argv[CONST_REPOSITORY],
            "-t",
            sys.argv[CONST_TRANSACTION],
            "--copy-info",
        ],
        stdout=subprocess.PIPE,
    )
    out, err = proc.communicate()
    del err
    str_out = out.decode()
    str_out_list = str_out.splitlines()
    # D2+D3+D4:  F1.1 to F1.4: Reject rename and block change in tag, allow new tag
    tagspath = re.compile(CONST_STR_TAGS_PATH)
    tagadd = re.compile(CONST_STR_TAG_ADD)
    for str_line in str_out_list:
        found = tagspath.search(str_out)
        if found != None:
            added = tagadd.search(str_line)
            if added == None:
                sys.stderr.buffer.write(CONST_STR_FAILED)
                proc.kill()
                exit(code=CONST_FAILED)
    # no issue found, allow commit
    proc.kill()
    exit(code=CONST_PASSED)


if __name__ == "__main__":
    main()
