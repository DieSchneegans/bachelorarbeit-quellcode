"""
Post-commit hook that extends the svn author.
The hook searches for full name of a CAUser in a csv table and appends it to
the svn:author property of the revision processed.

$HeadURL: https://svnserver.europe.kspag.de/svn/projects/Internal/KSPG_Konfigurationsmanagement/trunk/04_Software/ServerSide/HookScripts/PythonHooks/de/kspg/svn/script/FindUnprotectedUnmergables.py $  # @IgnorePep8
$LastChangedDate: 2014-03-31 15:52:39 +0200 (Mon, 31 Mar 2014) $
$LastChangedBy: CA43420 (Winzer, Wolfgang) $
$Revision: 29635 $

"""

import sys

from de.kspg.svn.domain.CaUserLut import CaUserLut
from de.kspg.svn.domain.SvnAuthor import SvnAuthor
from de.kspg.svn.io.Messenger import createMonitoringLogger, passTransaction
import pysvn
import os

# The filename of the lut to use
LUTNAME = "CaUserLookupTable.csv"


def main():

    # copy hook arguments to variables
    repoPath = sys.argv[1]  # e.g. C:/Repositories/playground
    rev = sys.argv[2]  # e.g. 30758

    log = createMonitoringLogger("Monitor", repoPath)
    log.debug("hook started")

    log.debug("acquiring client")
    # client = SetupSvnClient()
    txn = pysvn.Transaction(repoPath, rev, True)
    lutpath = os.path.join(os.path.dirname(repoPath), LUTNAME)
    log.debug("acquiring lut from {}".format(lutpath))
    lut = CaUserLut(filename=lutpath)

    log.debug("reading author")
    svnAuthor = SvnAuthor.CreateOnServer(txn, rev, lut)
    log.debug("writing modified author")
    svnAuthor.WriteToServer()

    oldAuthor = svnAuthor.GetOldAuthorString()
    newAuthor = svnAuthor.GetAuthorString()
    log.info("r{}: replaced {} by {}".format(rev, oldAuthor, newAuthor))

    log.debug("hook finished")
    passTransaction()


if __name__ == "__main__":
    main()
