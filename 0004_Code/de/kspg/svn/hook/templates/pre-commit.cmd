@echo off  
::  
:: Stops commits that have empty log messages.  
::  
@echo off  
   
setlocal  
   
rem Subversion sends through the path to the repository and transaction id  
set REPOS=%1  
set TXN=%2
set SVNLOOK="C:\Program Files\TortoiseSVN\bin\svnlook.exe"
   
rem check for an empty log message  
rem %SVNLOOK% log %REPOS% -t %TXN% | findstr . > nul  
rem if %errorlevel% gtr 0 (goto err) else exit 0  
exit 0
   
:err  
echo. 1>&2  
echo Your commit has been blocked because you didn't give any log message 1>&2  
echo Please write a log message describing the purpose of your changes and 1>&2  
echo then try committing again. -- Thank you 1>&2  
exit 1 