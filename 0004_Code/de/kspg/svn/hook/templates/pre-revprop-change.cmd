@echo off  
::  
:: Allows changes to log messages but not to anything else
::  
@echo off  
   
setlocal  
   
rem copy parameters passed by subversion to hook
set PROPNAME=%4
set ACTION=%5

rem allow only changes to log message
rem if "%PROPNAME%" NEQ "svn:log" (GOTO err)
exit 0
   
:err  
echo. 1>&2  
echo Changing %PROPNAME% is not allowed by Pierburg KM process. 1>&2  
echo You can try begging your head CME (Wolfgang Winzer) for exceptions. 1>&2  
exit 1 