@echo off
:: =============================================================================
:: Calls all post-lock hooks using the specified order, interpreters and
:: environment.
::
::   [1] REPOS-PATH   (the path to this repository)
::   [2] USER         (the user who created the lock)
::
:: typical parameters
:: %1=https://svnserver.europe.kspag.de/svn/projects
:: %2=CA43420
::
:: $HeadURL: https://svnserver.europe.kspag.de/svn/Internal/KSPG_Konfigurationsmanagement/trunk/04_Software/ServerSide/HookScripts/MainHooks/pre-lock.bat $
:: $LastChangedDate: 2014-01-29 09:18:37 +0100 (Mi, 29 Jan 2014) $
:: $LastChangedBy: CA43420 $
:: $LastChangedRevision: 3665 $  
:: =============================================================================

:: =======================
:: environment setup
:: =======================

:: hook paths
set DIR_HOOKS=%1\hooks\subhooks


:: =============================
:: script part
:: =============================

:: no subhooks currently

:SUCCESS
exit 0

:FAILURE
exit 1