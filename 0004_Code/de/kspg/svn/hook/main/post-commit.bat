@echo off
:: =============================================================================
::
:: POST-COMMIT HOOK
::
:: The post-commit hook is invoked after a commit.  Subversion runs
:: this hook by invoking a program (script, executable, binary, etc.)
:: named 'post-commit' with the following ordered arguments:
::
::   [1] REPOS-PATH   (the path to this repository)
::   [2] REV          (the number of the revision just committed)
::
:: $HeadURL: https://svnserver.europe.kspag.de/svn/Projects/Internal2.0/BSW/ToolSupport/Subversion/trunk/04_Software/SvnScripts/src/de/kspg/svn/hook/main/post-commit.bat $
:: $LastChangedDate: 2020-03-03 07:42:08 +0100 (Di, 03 Mrz 2020) $
:: $LastChangedBy: CA43420 (Winzer, Wolfgang) $
:: $LastChangedRevision: 171245 $
:: =============================================================================

:: =======================
:: environment setup
:: =======================

:: hook paths
set DIR_HOOKS=%1\hooks\subhooks


:: =======================
:: scripts
:: =======================

python %DIR_HOOKS%\CaUserToFullName.py %*
if ERRORLEVEL 1 GOTO FAILURE


:SUCCESS
exit 0

:FAILURE
exit 1