@echo off
::==============================================================================
::
:: PRE-COMMIT HOOK
:: 
:: This batch acts as the main pre-commit hook,
:: calling all configured sub-hooks.
:: Please note that you can't configure multiple hooks of
:: the same type (e.g. pre-commit) so we need to program
:: a fan-out.
:: If one of the sub-hooks fails, the main hook
:: fails immediately.
:: You can mix hooks in different languages but don't
:: forget to include the interpreter (java.exe, python.exe)
:: in the call.
::
:: PARAMETERS
:: -------------------
::  [1] REPOS-PATH   (the path to this repository)
::  [2] TXN-NAME     (the name of the transaction about to be committed)
::
:: Hints for developers
:: ----------------
:: - Don't rely on the current directory, it's the installation directory of the server, not the repositories
:: - Don't rely on the environment, it is empty (read about it in the subversion book)
::     - Although it contradicts the svn book, the PATH variable is available!
::
:: $HeadURL: https://svnserver.europe.kspag.de/svn/Projects/Internal2.0/BSW/ToolSupport/Subversion/trunk/04_Software/SvnScripts/src/de/kspg/svn/hook/main/pre-commit.bat $
:: $LastChangedDate: 2020-03-03 07:35:39 +0100 (Di, 03 Mrz 2020) $
:: $LastChangedBy: CA43420 (Winzer, Wolfgang) $
:: $LastChangedRevision: 171244 $  
::==============================================================================

:: uncomment these lines to debug environment
::echo the following environment variables are available: 1>&2
::echo %* 1>&2



:: =======================
:: environment setup
:: =======================

:: hook paths
set DIR_HOOKS=%1\hooks\subhooks


:: =======================
:: script
:: =======================


call %DIR_HOOKS%\RejectEmptyComment.bat %*
if ERRORLEVEL 1 GOTO FAILURE

python %DIR_HOOKS%\ForceJiraRef.py %*
if ERRORLEVEL 1 GOTO FAILURE

:: it is important to call this hook last because it generates a lot of output even when successful
python %DIR_HOOKS%\ProtectFrozen.py %*
if ERRORLEVEL 1 GOTO FAILURE



:SUCCESS
exit 0

:FAILURE
exit 1