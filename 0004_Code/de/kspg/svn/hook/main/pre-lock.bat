@echo off
:: =============================================================================
:: Calls a configurable svn pre-lock hook
:: the same way the subversion server will.
:: This, coupled with dummies for the
:: programs used by the hook, allows for
:: thorough, even automatic testing of
:: the hook before it is installed into a
:: server.
::   [1] REPOS-PATH   (the path to this repository)
::   [2] PATH         (the path in the repository about to be locked)
::   [3] USER         (the user creating the lock)
::   [4] COMMENT      (the comment of the lock)
::   [5] STEAL-LOCK   (1 if the user is trying to steal the lock, else 0)
::
:: typical parameters
:: %1=https://svnserver.europe.kspag.de/svn/playground
:: %2=Testprojekt/trunk/e1
:: %3=CA43420
:: %4="Hands off, everyone"
:: %5=0
::
:: $HeadURL: https://svnserver.europe.kspag.de/svn/Projects/Internal2.0/BSW/ToolSupport/Subversion/trunk/04_Software/SvnScripts/src/de/kspg/svn/hook/main/pre-lock.bat $
:: $LastChangedDate: 2020-03-03 07:42:08 +0100 (Di, 03 Mrz 2020) $
:: $LastChangedBy: CA43420 (Winzer, Wolfgang) $
:: $LastChangedRevision: 171245 $  
:: =============================================================================

:: =======================
:: environment setup
:: =======================

:: hook paths
set DIR_HOOKS=%1\hooks\subhooks


:: =============================
:: script part
:: =============================

:: no subhooks currently

:SUCCESS
exit 0

:FAILURE
exit 1