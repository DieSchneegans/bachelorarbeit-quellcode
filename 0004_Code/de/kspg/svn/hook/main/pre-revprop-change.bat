@echo off
:: =============================================================================
::
:: PRE-REVPROP-CHANGE HOOK
::
:: The pre-revprop-commit hook is invoked when a user tries to change a
:: non-versioned revision property like message or author. All output
:: to stderr (but not stdout!) will be routed back to the user.
::
:: Subversion runs this hook by invoking this script with the following 
:: ordered arguments:
::
::   [1] REPOS-PATH   (the path to this repository)
::   [2] REVISION     (the revision being tweaked)
::   [3] USER         (the username of the person tweaking the property)
::   [4] PROPNAME     (the property being set on the revision)
::   [5] ACTION       (the property is being 'A'dded, 'M'odified, or 'D'eleted)
::
:: This script does nothing by itself, it only calls a chain of sub-hooks
:: that contain the actual functionality. If any of these sub-hooks returns
:: an exit code other than zero (0), the change is prevented and the user
:: receives an error message.
::
:: $HeadURL: https://svnserver.europe.kspag.de/svn/Projects/Internal2.0/BSW/ToolSupport/Subversion/trunk/04_Software/SvnScripts/src/de/kspg/svn/hook/main/pre-revprop-change.bat $
:: $LastChangedDate: 2020-03-03 07:42:08 +0100 (Di, 03 Mrz 2020) $
:: $LastChangedBy: CA43420 (Winzer, Wolfgang) $
:: $LastChangedRevision: 171245 $  
:: =============================================================================

:: =======================
:: environment setup
:: =======================

:: hook paths
set DIR_HOOKS=%1\hooks\subhooks


:: =======================
:: script
:: =======================

call %DIR_HOOKS%\AllowCommentChangeOnly.bat %*
if ERRORLEVEL 1 GOTO FAILURE


:SUCCESS
exit 0

:FAILURE
exit 1