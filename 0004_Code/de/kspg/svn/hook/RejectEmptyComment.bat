@echo off  
::  
:: Stops commits that have empty log messages.  
:: 
:: $HeadURL: https://svnserver.europe.kspag.de/svn/Projects/Internal2.0/BSW/ToolSupport/Subversion/trunk/04_Software/SvnScripts/src/de/kspg/svn/hook/RejectEmptyComment.bat $
:: $LastChangedDate: 2014-04-29 14:10:00 +0200 (Di, 29 Apr 2014) $
:: $LastChangedBy: CA43420 (Winzer, Wolfgang) $
:: $LastChangedRevision: 30814 $ 
   
setlocal  
   
rem Subversion sends through the path to the repository and transaction id  
set REPOS=%1  
set TXN=%2

   
rem check for an empty log message  
svnlook log %REPOS% -t %TXN% | findstr . > nul  
if %errorlevel% gtr 0 (goto err) else exit /B 0  
   
:err  
echo. 1>&2  
echo Your commit has been blocked because you didn't give any log message 1>&2  
echo Please write a log message describing the purpose of your changes and 1>&2  
echo then try committing again. -- Thank you 1>&2  
exit /B 1 