:: =============================================================================
:: Allows changes to log messages but not to anything else.
::
:: $HeadURL: https://svnserver.europe.kspag.de/svn/Projects/Internal2.0/BSW/ToolSupport/Subversion/trunk/04_Software/SvnScripts/src/de/kspg/svn/hook/AllowCommentChangeOnly.bat $
:: $LastChangedDate: 2014-04-29 14:10:00 +0200 (Di, 29 Apr 2014) $
:: $LastChangedBy: CA43420 (Winzer, Wolfgang) $
:: $LastChangedRevision: 30814 $
:: =============================================================================

@echo off
setlocal

:: copy parameters passed by subversion to hook
set REPOPATH=%1
set REVISION=%2
set USER=%3
set PROPNAME=%4
set ACTION=%5

set ADMINUSER=CA43420

:: allow changes to log message for anyone
if "%PROPNAME%"=="svn:log" (
	GOTO pass
)

:: allow changes to other properties only to admin
if /I "%USER%" NEQ "%ADMINUSER%" (
	GOTO fail
)

:pass
exit /B 0

:fail
echo. 1>&2
echo Changing %PROPNAME% is not allowed by Pierburg KM process. 1>&2
echo You can try begging your head CME (Wolfgang Winzer) for exceptions. 1>&2
exit /B 1