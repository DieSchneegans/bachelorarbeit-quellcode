# =============================================================================
# This script checks the commited content and rejects the commit if any invalid
# condition is found. This check is divided into 2 steps:
#
# 1. The commited contend will be checked regarding its type of change. Freeze
#       and thaw commands are not allowed to be commited while other changes
#       are made.
#
# 2. Changes on files and folders are not allowed to commit when the objects
#       are either frozen or located in frozen folders (inheritance will be
#       taken into account for this).
#
# IMPORTANT NOTE:
# The URL of the repository has to be specified manually in this scipt. The
# variable 'repoURL' has to be set to the path of the URL.
#
# $HeadURL: https://svnserver.europe.kspag.de/svn/Projects/Internal2.0/BSW/ToolSupport/Subversion/trunk/04_Software/SvnScripts/src/de/kspg/svn/hook/ProtectFrozen.py $
# $LastChangedDate: 2020-03-03 07:58:29 +0100 (Di, 03 Mrz 2020) $
# $LastChangedBy: CA43420 (Winzer, Wolfgang) $
# $LastChangedRevision: 171249 $
# =============================================================================


import sys
from lib2to3.pgen2.pgen import DFAState
from de.kspg.svn.stats.ExecTimer import ExecTimer
import pysvn
from pickle import FALSE


FrozenStates_IsNotFrozen = 0
FrozenStates_IsFrozen = 1
FrozenStates_HasFrozenParent = 2


# Config

# URL of the repopsitory: https://XYZ/RepoName/
repoURL = "https://svnserver.europe.kspag.de/svn/playground"
# repoURL = "https://svnserver.europe.kspag.de/svn/projects"
# repoURL = "https://c02l14nb0835.europe.kspag.de/svn/FreezeTest"

# Name of the new property
frozenProperty = "rha:frozen"

# Indices of the changed function result tuple
TA_Action = 0
TA_ContentChanged = 2
TA_PropertyChanged = 3


# --- --- --- --- --- --- Helper functions --- --- --- --- --- --- --- ---

##############################################################################################################
# prints message to stderr which is the only output routed by svn hook environment
# The message will appear in red letters for the user to see if an error occurs.
##############################################################################################################
def eprint(message):
    print(message, file=sys.stderr)


##############################################################################################################
# Leave the script with the return code 2 if it is not called with a parameter count of 3.                   #
#                                                                                                            #
# Input:  -                                                                                                  #
# Return: -                                                                                                  #
##############################################################################################################
def checkInputParameters():

    if len(sys.argv) != 3:
        eprint("Wrong number of input parameters!")
        sys.exit(2)


##############################################################################################################
# Generate the parent directory of the given path.                                                           #
#                                                                                                            #
# Input:  Path                                                                                               #
# Return: Parent of the input                                                                                #
#                                                                                                            #
# Examples...                                                                                                #
# "/Folder1/Test.txt" => "Folder1"                                                                            #
# "/Test.txt" => ""                                                                                           #
##############################################################################################################
def getParentDirectory(path):
    return path.rsplit("/", 1)[0]


# --- --- --- --- --- --- Check the frozen property --- --- --- --- --- --- --- ---

##############################################################################################################
# a dictionary which caches the isFrozen verdict for all paths
##############################################################################################################
cachedNodes = {}

##############################################################################################################
# gets information for one node either from cache or from the server
# information from the server is then added to cache
##############################################################################################################
def isNodeFrozen(client, path):
    if path in cachedNodes:
        return cachedNodes[path]
    else:
        isFrozen = isNodeFrozenInRepo(client, path)
        cachedNodes[path] = isFrozen
        return isFrozen


##############################################################################################################
# asks the repo (never cache) if the frozen property is set for one node
##############################################################################################################
def isNodeFrozenInRepo(client, relativePathPlus):

    head = pysvn.Revision(pysvn.opt_revision_kind.head)

    try:
        proplist = client.propget(
            frozenProperty,
            repoURL + relativePathPlus,
            revision=head,
            recurse=False,
            peg_revision=head,
        )
        # to see if property is set, look for any value
        return len(proplist) > 0

    except Exception as err:
        analyseException(err)


##############################################################################################################
# Determine if the given object has the frozen property set. The result can be one of the following states:  #
# - IsNotFrozen                                                                                              #
# - IsFrozen                                                                                                 #
# - HasFrozenParent                                                                                          #
# The function also returns the path of the first appearance of the frozen property in the parent tree       #
# structure, when the object itself is not frozen.  This is useful to check if the given object has a        #
# inherited frozen property (this function can be disabled via the parameter IncludeInherited).              #                                                       #
#                                                                                                            #
# Input:  PySvn client object                                                                                #
#         Path of the object                                                                                 #
#         Should inherited frozen properties should be also checked (True)?                                  #
# Return: Frozen property state of the object                                                                #
#         Path of the first appearance of the frozen property in the parent tree                             #                                                  #
##############################################################################################################
def isFrozenPropSetInRepo(client, relativePathPlus, IncludeInherited):

    Result = FrozenStates_IsNotFrozen

    # start the check at the lowest node
    remainingPath = relativePathPlus

    for LoopCount in range(0, 99):

        # break from loop when the frozen property was found. The value is irrelevant.
        if isNodeFrozen(client, remainingPath):

            if LoopCount == 0:
                # 1st iteration => the given relativePath was frozen
                Result = FrozenStates_IsFrozen
                # eprint("file is frozen")
            else:
                # 2nd iteration or later, parent is frozen
                Result = FrozenStates_HasFrozenParent
                # eprint("path is frozen")

            break

        # Exit when the path to check is either empty or reached the repoURL or when only a single file
        # should be checked (IncludeInherited == False)
        if len(remainingPath) == 0 or IncludeInherited == False:
            break

        # work our way up towards the repository root
        remainingPath = getParentDirectory(remainingPath)

    return Result, remainingPath


##############################################################################################################
# Analyse the exception to get the source of it.                                                             #
#                                                                                                            #
# Input:  Exception                                                                                          #
# Return: -                                                                                                  #
##############################################################################################################
def analyseException(err):

    # Get the name of the exception source
    exceptionType = str(type(err))

    # Is the exception raised by PySVN?
    if exceptionType.find("pysvn") > 0:
        handlePySvnException(err)
    else:
        eprint("UNHANDLED EXCEPTION in script ProtectFrozen.py:")
        eprint(exceptionType)
        exit(1)


##############################################################################################################
# Analyse the exception to get the source of it.                                                             #
#                                                                                                            #
# Input:  Exception                                                                                          #
# Return: -                                                                                                  #
##############################################################################################################
def handlePySvnException(err):

    for message, code in err.args[1]:

        # Code 150000 (=entry_not_found) is not an error because this exception is raised when a new object
        # is added in a new folder that doesn't exist at the time of the commit. All other PySVN exceptions
        # will terminate the script.
        if code != pysvn.svn_err.entry_not_found:
            eprint("PySVN EXCEPTION in script ProtectFrozen.py")
            eprint("Code: {}".format(code))
            eprint("Message: {}".format(message))
            exit(code)


##############################################################################################################
# Determine if the parent of the given object has the frozen property set. Because the function              #
# calls isFrozenPropSetInRepo(), all options are the same here.                                              #
#                                                                                                            #
# Input:  PySvn client object                                                                                #
#         Path of the object                                                                                 #
# Return: Frozen property state of the parent object                                                         #
#         Path of the first appearance of the frozen property in the parent tree                             #                                                  #
##############################################################################################################
def isFrozenPropSetInParentInRepo(client, relativePathPlus):
    return isFrozenPropSetInRepo(client, getParentDirectory(relativePathPlus), True)


##############################################################################################################
# Determine if the given object in the working copy has the frozen property set.                             #                                                       #
#                                                                                                            #
# Input:  Transaction object                                                                                 #
#         Path of the object                                                                                 #
# Return: Frozen property set (=True)?                                                                       #
##############################################################################################################
def isFrozenPropSetInWorkingCopy(transaction, relativePathPlus):
    # Frozen property found? The value is irrelevant.
    return transaction.propget(frozenProperty, relativePathPlus) != None


# --- --- --- --- --- --- Analyse changes in the commit --- --- --- --- --- --- --- ---


##############################################################################################################
# Show the count of the different changes types.                                                             #
#                                                                                                            #
# Input:  Changed object count                                                                               #
#         Added object count                                                                                 #
#         Removed object count                                                                               #
#         Frozen object count                                                                                #
#         Thawed object count                                                                                #
#         Other poperty changes count                                                                        #
# Return: -                                                                                                  #
##############################################################################################################
def showCounts(
    ChangedObjects,
    AddedObjects,
    RemovedObjects,
    FrozenObjects,
    ThawedObjects,
    OtherPropChanged,
):

    eprint("\n--- --- Changes summary --- ---")

    eprint("{:3d} objects changed".format(ChangedObjects))
    eprint("{:3d} objects added".format(AddedObjects))
    eprint("{:3d} objects deleted".format(RemovedObjects))
    eprint("{:3d} objects frozen".format(FrozenObjects))
    eprint("{:3d} objects thawed".format(ThawedObjects))
    eprint("{:3d} other props changed".format(OtherPropChanged))


##############################################################################################################
# Analyse the count of the different changes types and leave the script if any invalid                       #
# combination was found.                                                                                     #
#                                                                                                            #
# Input:  Changed object count                                                                               #
#         Added object count                                                                                 #
#         Removed object count                                                                               #
#         Frozen object count                                                                                #
#         Thawed object count                                                                                #
#         Other poperty changes count                                                                        #
# Return: -                                                                                                  #
##############################################################################################################
def analyseCounts(
    ChangedObjects,
    AddedObjects,
    RemovedObjects,
    FrozenObjects,
    ThawedObjects,
    OtherPropChanged,
):

    if (FrozenObjects > 0) and (ThawedObjects > 0):
        eprint(
            "\nERR: Simultaneous committing of freeze and thaw commands is not allowed!\n"
        )
        sys.exit(3)
    else:
        if (AddedObjects + RemovedObjects + ChangedObjects + OtherPropChanged) > 0:
            if FrozenObjects > 0:
                eprint(
                    "\nERR: Simultaneous committing of freeze commands and other changes isn't allowed!\n"
                )
                sys.exit(4)
            elif ThawedObjects > 0:
                eprint(
                    "\nERR: Simultaneous committing of thaw commands and other changes isn't allowed!\n"
                )
                sys.exit(5)


##############################################################################################################
# Count the different changes types (object changed, deleted, added or property changed)                     #                                                       #
#                                                                                                            #
# Input:  Client object                                                                                      #
#         Transaction object                                                                                 #
# Return: -                                                                                                  #
##############################################################################################################
def countChangesTypes(client, transaction):

    ChangedObjects = 0
    AddedObjects = 0
    RemovedObjects = 0
    FrozenObjects = 0
    ThawedObjects = 0
    OtherPropChanged = 0

    eprint("\n--- --- Check changes types --- ---")

    changed = transaction.changed()

    for relativePath, TransactionInfo in changed.items():

        relativePathPlus = "/" + relativePath
        eprint(relativePathPlus)

        # Check for added object
        if TransactionInfo[TA_Action] == "A":
            AddedObjects += 1
            eprint("Object added")

        else:
            # Check for removed object
            if TransactionInfo[TA_Action] == "D":
                RemovedObjects += 1
                eprint("Object deleted")

            # Check for content change
            if TransactionInfo[TA_ContentChanged] > 0:
                ChangedObjects += 1
                eprint("Object changed")

            # Check for property change
            if TransactionInfo[TA_PropertyChanged] > 0:
                FrozenState, frozenParent = isFrozenPropSetInRepo(
                    client, relativePathPlus, False
                )
                IsSetInWorkingCopy = isFrozenPropSetInWorkingCopy(
                    transaction, relativePathPlus
                )

                if FrozenState == FrozenStates_IsFrozen and IsSetInWorkingCopy == False:
                    ThawedObjects += 1
                    eprint("Frozen property removed")
                elif (
                    FrozenState == FrozenStates_IsNotFrozen
                    and IsSetInWorkingCopy == True
                ):
                    FrozenObjects += 1
                    eprint("Frozen property set")
                else:
                    OtherPropChanged += 1
                    eprint("Other property changed")

    showCounts(
        ChangedObjects,
        AddedObjects,
        RemovedObjects,
        FrozenObjects,
        ThawedObjects,
        OtherPropChanged,
    )

    analyseCounts(
        ChangedObjects,
        AddedObjects,
        RemovedObjects,
        FrozenObjects,
        ThawedObjects,
        OtherPropChanged,
    )


##############################################################################################################
# Check if any frozen object is either changed or removed, or any object is added, removed or changed in     #
# a frozen folder. Leave the script if such a illegal change was found.                                      #
#                                                                                                            #
# Input:  Client object                                                                                      #
#         Transaction object                                                                                 #
# Return: -                                                                                                  #
##############################################################################################################
def checkForFrozenChanges(client, transaction):

    Error = False

    eprint("\n--- --- Check for frozen changes --- ---")

    changed = transaction.changed()

    pathCount = len(changed.items())
    timer = ExecTimer("paths", pathCount)
    timer.Start()

    for relativePath, TransactionInfo in changed.items():

        relativePathPlus = "/" + relativePath
        eprint(relativePathPlus)

        # Check for added object
        if TransactionInfo[TA_Action] == "A":
            # Check for frozen parent
            FrozenState, frozenParent = isFrozenPropSetInParentInRepo(
                client, relativePathPlus
            )
            if FrozenState != FrozenStates_IsNotFrozen:
                Error = True
                eprint("ERR: Creating new objects in frozen folders is not allowed!")
                eprint("Frozen folder: ".format(frozenParent))

        # Check for deleted object
        elif TransactionInfo[TA_Action] == "D":
            # Check for frozen parent
            FrozenState, frozenParent = isFrozenPropSetInRepo(
                client, relativePathPlus, True
            )
            if FrozenState != FrozenStates_IsNotFrozen:
                Error = True
                if FrozenState == FrozenStates_IsFrozen:
                    eprint("ERR: Deleting frozen objects is not allowed!")
                else:
                    eprint("ERR: Deleting objects in frozen folders is not allowed!")
                    eprint("Frozen folder: {}".format(frozenParent))

        else:
            # Check for content change
            FrozenState, frozenParent = isFrozenPropSetInRepo(
                client, relativePathPlus, True
            )
            if (
                TransactionInfo[TA_ContentChanged] > 0
                and FrozenState != FrozenStates_IsNotFrozen
            ):
                Error = True
                if FrozenState == FrozenStates_IsFrozen:
                    eprint("ERR: Changing frozen files is not allowed!")
                else:
                    eprint("ERR: Changing files in frozen folders is not allowed!")
                    eprint("Frozen folder: {}".format(frozenParent))

    eprint("")
    timer.Stop(pathCount)

    if Error:
        sys.exit(6)


# --- --- --- --- --- --- Init PySVN --- --- --- --- --- --- --- ---


def ssl_server_trust_prompt(trust_dict):
    """Callback for pysvn.Client if svn lib complains about certificate.
       Emulates positive user feedback.
    """
    # just say everything is fine, thank you
    return 0xFF, 1, True


def setupSvnClient():
    "Provides a pysvn.client."

    client = pysvn.Client()
    client.callback_ssl_server_trust_prompt = ssl_server_trust_prompt
    client.exception_style = 1

    return client


# --- --- --- --- --- --- Main --- --- --- --- --- --- --- --- --- --- --- ---


def main():

    eprint("ProtectFrozen.py starts...")

    checkInputParameters()

    client = setupSvnClient()
    transaction = pysvn.Transaction(sys.argv[1], sys.argv[2])

    countChangesTypes(client, transaction)
    checkForFrozenChanges(client, transaction)

    eprint("ProtectFrozen.py ended")

    sys.exit(0)


main()
