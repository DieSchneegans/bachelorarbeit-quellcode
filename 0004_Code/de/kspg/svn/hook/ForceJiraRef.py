# =============================================================================
# This script checks the commited content and rejects the commit if any invalid
# condition is found. This check is divided into 2 steps:
#
# 1. The commited contend will be checked regarding its type of change. Freeze
#       and thaw commands are not allowed to be commited while other changes
#       are made.
#
# 2. Changes on files and folders are not allowed to commit when the objects
#       are either Forced or located in Forced folders (inheritance will be
#       taken into account for this).
#
# IMPORTANT NOTE:
# The URL of the repository has to be specified manually in this scipt. The
# variable 'repoURL' has to be set to the path of the URL.
#
# $HeadURL: https://svnserver.europe.kspag.de/svn/Projects/Internal2.0/BSW/ToolSupport/Subversion/trunk/04_Software/SvnScripts/src/de/kspg/svn/hook/ForceJiraRef.py $
# $LastChangedDate: 2020-05-04 12:12:17 +0200 (Mo, 04 Mai 2020) $
# $LastChangedBy: CA43420 (Winzer, Wolfgang) $
# $LastChangedRevision: 175510 $
# =============================================================================


import sys
from lib2to3.pgen2.pgen import DFAState
import pysvn
import re
from pickle import FALSE


ForcedStates_IsForced = 0
ForcedStates_IsNotForced = 1


# Config

# URL of the repopsitory: https://XYZ/RepoName/
# repoURL = "file:///C:/Repo-Test/"
repoURL = "https://svnserver.europe.kspag.de/svn/playground"
# repoURL = "https://svnserver.europe.kspag.de/svn/projects"
# repoURL = "https://c02l14nb0835.europe.kspag.de/svn/FreezeTest"

# Name of the new property
ForcedProperty = "rha:forceJiraRef"

# Indices of the changed function result tuple
TA_Action = 0
TA_ContentChanged = 2
TA_PropertyChanged = 3


# --- --- --- --- --- --- Helper functions --- --- --- --- --- --- --- ---


##############################################################################################################
# Leave the script with the return code 2 if it is not called with a parameter count of 3.                   #
#                                                                                                            #
# Input:  -                                                                                                  #
# Return: -                                                                                                  #
##############################################################################################################
def checkInputParameters():

    if len(sys.argv) != 3:
        print("Wrong number of input parameters!")
        sys.exit(2)


##############################################################################################################
# Generate the parent directory of the given path.                                                           #
#                                                                                                            #
# Input:  Path                                                                                               #
# Return: Parent of the input                                                                                #
#                                                                                                            #
# Examples...                                                                                                #
# "/Folder1/Test.txt" => "Folder1"                                                                            #
# "/Test.txt" => ""                                                                                           #
##############################################################################################################
def getParentDirectory(path):
    return path.rsplit("/", 1)[0]


# --- --- --- --- --- --- Check the Forced property --- --- --- --- --- --- --- ---

##############################################################################################################
# a dictionary which caches the isForced verdict for all paths
##############################################################################################################
cachedNodes = {}

##############################################################################################################
# gets information for one node either from cache or from the server
# information from the server is then added to cache
##############################################################################################################
def isJiraRefForced(client, path):
    if path in cachedNodes:
        return cachedNodes[path]
    else:
        isForced = isJiraRefForcedInRepo(client, path)
        cachedNodes[path] = isForced
        return isForced


##############################################################################################################
# asks the repo (never cache) if the Forced property is set for one node
##############################################################################################################
def isJiraRefForcedInRepo(client, relativePathPlus):

    head = pysvn.Revision(pysvn.opt_revision_kind.head)

    try:
        proplist = client.propget(
            ForcedProperty,
            repoURL + relativePathPlus,
            revision=head,
            recurse=False,
            peg_revision=head,
        )
        # to see if property is set, look for any value
        return len(proplist) > 0
    except Exception as err:
        analyseException(err)


##############################################################################################################
# Determine if the given object has the Forced property set. The result can be one of the following states:  #
# - IsNotForced                                                                                              #
# - isForced                                                                                                 #
# - HasForcedParent                                                                                          #
# The function also returns the path of the first appearance of the Forced property in the parent tree       #
# structure, when the object itself is not Forced.  This is useful to check if the given object has a        #
# inherited Forced property (this function can be disabled via the parameter IncludeInherited).              #                                                       #
#                                                                                                            #
# Input:  PySvn client object                                                                                #
#         Path of the object                                                                                 #
#         Should inherited Forced properties should be also checked (True)?                                  #
# Return: Forced property state of the object                                                                #
#         Path of the first appearance of the Forced property in the parent tree                             #                                                  #
##############################################################################################################
def isForcedPropSetInRepo(client, relativePathPlus, IncludeInherited):

    Result = ForcedStates_IsNotForced

    # start the check at the lowest node
    remainingPath = relativePathPlus

    for LoopCount in range(0, 99):

        # break from loop when the Forced property was found. The value is irrelevant.
        if isJiraRefForced(client, remainingPath):
            Result = ForcedStates_IsForced
            break

        # Exit when the path to check is either empty or reached the repoURL or when only a single file
        # should be checked (IncludeInherited == False)
        if len(remainingPath) == 0 or IncludeInherited == False:
            break

        # work our way up towards the repository root
        remainingPath = getParentDirectory(remainingPath)

    return Result, remainingPath


##############################################################################################################
# Analyse the exception to get the source of it.                                                             #
#                                                                                                            #
# Input:  Exception                                                                                          #
# Return: -                                                                                                  #
##############################################################################################################
def analyseException(err):

    # Get the name of the exception source
    exceptionType = str(type(err))

    # Is the exception raised by PySVN?
    if exceptionType.find("pysvn") > 0:
        handlePySvnException(err)
    else:
        print("UNHANDLED EXCEPTION in script ProtectForced.py:")
        print(exceptionType)
        exit(1)


##############################################################################################################
# Analyse the exception to get the source of it.                                                             #
#                                                                                                            #
# Input:  Exception                                                                                          #
# Return: -                                                                                                  #
##############################################################################################################
def handlePySvnException(err):

    for message, code in err.args[1]:

        # Code 150000 (=entry_not_found) is not an error because this exception is raised when a new object
        # is added in a new fodler that doesn't exist at the time of the commit. All other PySVN exceptions
        # will terminate the script.
        if code != pysvn.svn_err.entry_not_found:
            print(
                "PySVN EXCEPTION in script ProtectForced.py - Code:",
                code,
                "\nMessage:",
                message,
            )
            exit(code)


##############################################################################################################
# Determine if the parent of the given object has the Forced property set. Because the function              #
# calls isForcedPropSetInRepo(), all options are the same here.                                              #
#                                                                                                            #
# Input:  PySvn client object                                                                                #
#         Path of the object                                                                                 #
# Return: Forced property state of the parent object                                                         #
#         Path of the first appearance of the Forced property in the parent tree                             #                                                  #
##############################################################################################################
def isForcedPropSetInParentInRepo(client, relativePathPlus):
    return isForcedPropSetInRepo(client, getParentDirectory(relativePathPlus), True)


##############################################################################################################
# Determine if the given object in the working copy has the Forced property set.                             #                                                       #
#                                                                                                            #
# Input:  Transaction object                                                                                 #
#         Path of the object                                                                                 #
# Return: Forced property set (=True)?                                                                       #
##############################################################################################################
def isForcedPropSetInWorkingCopy(transaction, relativePathPlus):
    # Forced property found? The value is irrelevant.
    return transaction.propget(ForcedProperty, relativePathPlus) != None


# --- --- --- --- --- --- Analyse changes in the commit --- --- --- --- --- --- --- ---

##############################################################################################################
# prints message to stderr which is the only output routed by svn hook environment
# The message will appear in red letters for the user to see if an error occurs.
##############################################################################################################
def printErr(message):
    print(message, file=sys.stderr)


##############################################################################################################
# Check if any object is either changed or removed, or any object is added, removed or changed in            #
# a folder with a forced jira reference without referencing.                                                 #
# Leave the script if such a illegal change was found.                                                       #
#                                                                                                            #
# Input:  Client object                                                                                      #
#         Transaction object                                                                                 #
# Return: -                                                                                                  #
##############################################################################################################
def checkForInvalidChanges(client, transaction):

    Error = False

    print("\n--- --- Check for Forced changes --- ---")

    changed = transaction.changed()

    pathCount = len(changed.items())

    head = pysvn.Revision(pysvn.opt_revision_kind.head)

    for relativePath, TransactionInfo in changed.items():

        relativePathPlus = "/" + relativePath
        print(relativePathPlus)

        message = transaction.revpropget("svn:log")

        # Check for added object
        if (
            TransactionInfo[TA_Action] == "A"
            or TransactionInfo[TA_Action] == "D"
            or TransactionInfo[TA_ContentChanged] > 0
        ):
            ForcedState, ForcedParent = isForcedPropSetInParentInRepo(
                client, relativePathPlus
            )
            if ForcedState != ForcedStates_IsNotForced:
                ReferencingIssueMatch = re.match(
                    r"([A-Z][A-Z0-9]+-\d+)(,\s([A-Z][A-Z0-9]+-\d+))*: ", message
                )
                CmActivityMatch = re.match(r"^CM.*: ", message)
                if ReferencingIssueMatch is None and CmActivityMatch is None:
                    Error = True
                    printErr(
                        "ERROR: Your commit message must reference a Jira issue or CM activity!"
                    )
                    printErr(
                        "Folder that forces Jira references: {}".format(ForcedParent)
                    )
                    printErr("Examples for allowed comments:")
                    printErr("    ABC-123: Lorem Ipsum ...")
                    printErr("    ABC-234, ABC-789: Lorem Ipsum ...")
                    printErr("    CM: Lorem Ipsum ...'")
                    printErr("    CM-BRA: Lorem Ipsum ...'")
                    printErr("")
                    printErr(
                        "Note that the spaces after commas and colon are mandatory!"
                    )
                    printErr("")
                    printErr(
                        "Please ignore the following error message generated by SVN:"
                    )
                    break

    print("")

    if Error:
        sys.exit(6)


# --- --- --- --- --- --- Init PySVN --- --- --- --- --- --- --- ---


def ssl_server_trust_prompt(trust_dict):
    """Callback for pysvn.Client if svn lib complains about certificate.
       Emulates positive user feedback.
    """
    # just say everything is fine, thank you
    return 0xFF, 1, True


def setupSvnClient():
    "Provides a pysvn.client."

    client = pysvn.Client()
    client.callback_ssl_server_trust_prompt = ssl_server_trust_prompt
    client.exception_style = 1

    return client


# --- --- --- --- --- --- Main --- --- --- --- --- --- --- --- --- --- --- ---


def main():

    print("Python script starts...")

    checkInputParameters()

    client = setupSvnClient()
    transaction = pysvn.Transaction(sys.argv[1], sys.argv[2])

    checkForInvalidChanges(client, transaction)

    print("Python script ended")

    sys.exit(0)


main()
