:: =============================================================================
:: Finds locks in a subversion repository
:: No working copy is needed
::
:: PARAMTERS
::  %1 - URL of folder in repository (e.g. https://svnserver/svn/projects/myProject)
::
:: SVN KEYWORDS
::  $HeadURL: https://svnserver/svn/internal/KSPG_Konfigurationsmanagement/trunk/04_Software/CommandLineScripts/UpdateSvnMirror.bat $
::  $LastChangedDate: 2012-09-18 08:05:18 +0200 (Di, 18 Sep 2012) $
::  $LastChangedBy: ca43420 $
::  $Revision: 627 $
:: =============================================================================
@echo off
echo Looking for locks in %1
svn list %1 -R -v | findstr /r "\<O\>"