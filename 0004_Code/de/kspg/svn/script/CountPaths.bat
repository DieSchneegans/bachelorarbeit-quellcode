@echo off
SET PYTHONPATH=C:\Projects\_Internal\Konfigurationsmanagement\04_Software\ServerSide\HookScripts\PythonHooks
SET LOGFILE=%0.log
SET REPO=%1
python BatchExecTimer.py start
svn list -R %REPO% | find /C "/" > %LOGFILE%
set /p TOTAL= < %LOGFILE%
echo found paths: %TOTAL%
python BatchExecTimer.py stop --items %TOTAL%
