@echo off
rem ------------------------------------------------
rem UpdateSvnMirror.bat
rem ------------------------------------------------
rem Autor: Wolfgang Winzer
rem Subversion Metadata
rem  $HeadURL: https://svnserver.europe.kspag.de/svn/Projects/Internal2.0/BSW/ToolSupport/Subversion/trunk/04_Software/SvnScripts/src/de/kspg/svn/script/UpdateSvnMirror.bat $
rem  $LastChangedDate: 2014-04-29 13:47:47 +0200 (Di, 29 Apr 2014) $
rem  $LastChangedBy: CA43420 (Winzer, Wolfgang) $
rem  $Revision: 30807 $
rem
rem H�lt einen Read-Only-Mirror des Repositories auf Stand.
rem Zun�chst werden eventuelle �nderungen R�ckg�ngig gemacht (Revert),
rem anschlie�end die neueste Version vom Server geholt (Update)
rem
rem Das Script sollte vom Taskplaner regelmaessig (taeglich) aufgerufen werden.
rem WinXP: In "Geplante Tasks" aufrufen
rem Win7 : Geht analog, bei Stefan Riefers schon eingerichtet
rem
rem Voraussetzung
rem a) Pfad WCPATH zeigt auf die Wurzel einer Working Copy (ein Checkout)
rem b) Host hat svn.exe installiert (z.B. wird in TortoiseSVN 1.7.7 mitgeliefert, ist aber standardmaessig deaktivert)
rem c) Host hat Zugriffsrechte auf die Working Copy, die updgedatet werden soll
rem d) Host hat Zugriffsrechte auf den SVN-Server (z.B. jeder CA-User aus Z-F)
rem ------------------------------------------------

set WCPATH="W:\300 SubversionWorkingCopy"

rem Problem1: Dauert lange
rem Problem2: Alle (schreibgesch�tzten!) Bin�rfiles werden dadurch "ge�ndert" und vom darauffolgenden Revert zur�ckgesetzt.
rem echo removing write-protection for all files
rem attrib -R %WCPATH%\*.* /S 

rem Mache alle versehentlichen �nderungen r�ckg�ngig
rem -R: recursive
rem --non-interactive:  keine R�ckfragen, lieber fehlschlagen
echo reverting all changes (which should never have happended in the first place)
svn revert -R --non-interactive %WCPATH%

svn status


rem Holt neueste Version von Server
rem --non-interactive:  keine R�ckfragen, lieber fehlschlagen
echo updating the working copy to the repository's HEAD revsion
svn update --non-interactive %WCPATH%

rem Dauert lange und sch�tzt doch nicht sicher
rem echo write-protecting all files (but not .svn)
rem attrib +R %WCPATH%\*.* /S
rem attrib -R %WCPATH%\.svn\*.* /S