"""Analyses, extends or repairs svn authors in a repository. A lookup table
is used as input on how to extend user names but at the same time the script
can extend the lookup table whenever unknown users are discovered. To this
end the script looks up the ca-user in the active directory and inserts the
corrsponding real name into the table.
"""
"""
$HeadURL: https://svnserver.europe.kspag.de/svn/Projects/Internal2.0/BSW/ToolSupport/Subversion/trunk/04_Software/SvnScripts/src/de/kspg/svn/script/CaUserConverter.py $  # @IgnorePep8
$LastChangedDate: 2015-04-22 15:20:14 +0200 (Mi, 22 Apr 2015) $
$LastChangedBy: CA43420 (Winzer, Wolfgang) $
$Revision: 57472 $
"""

############
# IMPORTS
############

import pysvn
import datetime
import time
import argparse
import sys
from de.kspg.svn.domain.CaUserLut import CaUserLut
from de.kspg.svn.domain.SvnAuthor import SvnAuthor
from de.kspg.svn.stats.ExecTimer import ExecTimer
from de.kspg.svn.stats.Counters import Counters

############
# HELP
############


HELP_REPO = """
URL of the repository you want to process, e.g. https://svnserver.europe.kspag.de/svn/projects
"""

HELP_DRYRUN = """
If set, the script only goes through the motions but does not change the
repository. The lookup-table, however, is changed, so make a backup before.
"""

HELP_LIMIT_DAYS = """
Defines which revisions (measured in days) from present to past shall be
processed. Note that it can take hours to process a whole repository.
If both days and revision are specified, the stronger stop condition
will stop the script first.
"""

HELP_LIMIT_REV = """
The script will only process revisions greater or equal than this revision
number. If both days and revision are specified, the stronger stop condition
will stop the script first.
"""

HELP_FILE = """
The path (absolute or relative) to the CA-USER lookup table file you want
to use. If the file doesn't exist, it will be created by the script. Otherwise
it will be loaded and modified.
"""

HELP_VERBOSE = """
If set, more log output will be generated.
"""

EPILOG = """
Example usage:
python CaUserConverter.py https://svnserver/svn/projects -l 90
"""


############
# FUNCTIONS
############


def parseArguments():
    parser = argparse.ArgumentParser(
        description=sys.modules[__name__].__doc__, epilog=EPILOG
    )
    parser.add_argument(
        "repo",
        type=str,
        help=HELP_REPO,
        default="https://svnserver.europe.kspag.de/svn/projects",
    )
    parser.add_argument(
        "-l", "--limit-days", dest="limitDays", type=int, help=HELP_LIMIT_DAYS
    )
    parser.add_argument(
        "-r", "--revision", dest="minRev", type=int, default="1", help=HELP_LIMIT_REV
    )
    parser.add_argument(
        "-f", "--file", dest="lutfile", type=str, default="demoLut.csv", help=HELP_FILE
    )
    parser.add_argument(
        "-d", "--dry-run", dest="doDryRun", action="store_true", help=HELP_DRYRUN
    )
    parser.add_argument(
        "-v", "--verbose", dest="doVerbose", action="store_true", help=HELP_VERBOSE
    )
    return parser.parse_args()


def main():
    print("CaUserConverter starting")
    reposToProcess = [args.repo]

    for repo in reposToProcess:

        statistics = ProcessRepository(repo)
        print("")

        print("\n")
        repoShortName = repo.rsplit("/", 1)[-1]
        statsHeader = "Statistics for repository {}".format(repoShortName)
        statistics.Print(header=statsHeader, showTotal=True)
        print("")

    print("CaUserConverter stopping")


def processAuthor(svnAuthor, revPrefix, stats):
    """Logs the action performed on the current author writes the change to
    repository if any.
    """
    if svnAuthor.HasChanged():
        if svnAuthor.IsMalformed():
            print(revPrefix + "New Malformed: {} ".format(svnAuthor.GetAuthorString()))
            stats.Inc("new malformed")
        else:
            print(revPrefix + "Expanded: {} ".format(svnAuthor.GetAuthorString()))
            stats.Inc("expanded")
        if not args.doDryRun:
            svnAuthor.WriteToServer()
    elif svnAuthor.IsMalformed():
        print(revPrefix + "Known Malformed: {} ".format(svnAuthor.GetAuthorString()))
        stats.Inc("known malformed")
    else:
        if args.doVerbose:
            print(revPrefix + "Skipped: {} ".format(svnAuthor.GetAuthorString()))
        stats.Inc("skipped")


def ProcessRepository(repo):
    print("Processing Repository {0}".format(repo))
    client = SetupSvnClient()
    stats = Counters(["expanded", "skipped", "new malformed", "known malformed"])
    lut = CaUserLut(filename=args.lutfile, allowAd=True)
    head = GetHeadRevisionNo(repo, client)
    stopRev = GetStopRevisionNo(repo, client, args.limitDays, args.minRev)
    revPattern = "r{0:" + str(len(str(head))) + "}: "
    revsToProcess = head - stopRev + 1
    print("Processing {0} revisions ({1} to {2})".format(revsToProcess, head, stopRev))
    timer = ExecTimer("revisions", revsToProcess)
    timer.Start()
    revsProcessed = 0
    # remember that range() excludes the last number, but
    # we want the head revision included
    for rev in reversed(range(stopRev, head + 1)):
        revPrefix = revPattern.format(rev)
        svnAuthor = SvnAuthor.CreateOnClient(repo, client, rev, lut)
        processAuthor(svnAuthor, revPrefix, stats)
        revsProcessed = revsProcessed + 1
        if revsProcessed % 100 == 0:
            timer.Lap(revsProcessed)
    print("")
    timer.Stop(revsProcessed)
    lut.PrintNewUsers()
    return stats


def ssl_server_trust_prompt(trust_dict):
    """Callback for pysvn.Client if svn lib complains about certificate.
       Emulates positive user feedback.
    """
    # just say everything is fine, thank you
    return 0xFF, 1, True


def SetupSvnClient():
    "Provides a pysvn.client."
    client = pysvn.Client()
    client.callback_ssl_server_trust_prompt = ssl_server_trust_prompt
    return client


def GetHeadRevisionNo(repo, client):
    "Aks the server for the head revision of the given repository."
    headrev = client.revpropget("revision", url=repo)[0].number
    print("Found {0} revisions in repository".format(headrev))
    return headrev


def GetStopRevisionNo(repo, client, daysToProc=None, minRev=1):
    """Calculates the revision to stop processing at depeding on the
    configuration.
    """
    if daysToProc and daysToProc > 0:
        daysToProc = datetime.timedelta(daysToProc)
        today = datetime.datetime.now()
        stopDate = today - daysToProc
        # take care, this is time.time, not datetime.time
        stopTime = time.mktime(stopDate.timetuple())
        revObj = pysvn.Revision(  # @UndefinedVariable
            pysvn.opt_revision_kind.date, stopTime  # @UndefinedVariable
        )
        stopRev = client.revpropget("revision", url=repo, revision=revObj)[0].number
        stopRev = max(stopRev, minRev)
    else:
        stopRev = minRev

    return stopRev


# all top-level code was moved into a main() function. This prevents
# the execution of this code when the module is imported by another one.
# When called using 'python CaUserConverter.py', main() is executed
# automatically
if __name__ == "__main__":
    args = parseArguments()
    main()
