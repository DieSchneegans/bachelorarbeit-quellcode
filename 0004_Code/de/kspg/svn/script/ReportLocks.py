#! python3
"""
Finds all server-based locks in a subtree of an SVN repository. The locks are
listed with creation date, owner, path and lock comment. Using the 'break'
option, the locks can also be broken.
"""

"""
$HeadURL: https://svnserver.europe.kspag.de/svn/Projects/Internal2.0/BSW/ToolSupport/Subversion/trunk/04_Software/SvnScripts/src/de/kspg/svn/script/ReportLocks.py $
$LastChangedDate: 2015-11-03 09:37:11 +0100 (Di, 03 Nov 2015) $
$LastChangedBy: CA43420 (Winzer, Wolfgang) $
$Revision: 67676 $
"""

import sys
import subprocess
import argparse
import xml.sax as sax
import re

EPILOG = """
Example usage:
python ReportLocks.py https://svnserver/svn/projects -u Winzer -s ;
"""

HELP_PATH = """
Specifies which file tree to search for locks. Must point to a folder in an
SVN repository. May or may not end with a forward slash.
If you specify a working copy path, the script will search the corresponding
tree in the repository so a server connectino is always necessary.
"""

HELP_SEP = """
A character (or string) that separates the columns in the output. Defaults to
three spaces. Use ';' to generate csv output.
"""

HELP_USER = """
List only locks for users containing the given search string. Please note that
users are given by real name (e.g. Winzer, Wolfgang). The search is case
sensitive.
"""

HELP_BREAK = """
Breaks all locks found. If report is filtered (e.g. by name), only those locks
are broken that match the filter criteria.
"""

STDOUT_ENCODING = "cp850"


class Lock:

    """Represents an SVN lock. As that it has only data, no functions."""

    def __init__(self, path, sep, root):
        self.path = path
        self.comment = "(no comment)"
        self.sep = sep
        self.root = root

    def url(self):
        return self.root + "/" + self.path

    def __str__(self):
        OUT_FORMAT = "{date}{sep}{owner}{sep}{path}{sep}{comment}"
        string = OUT_FORMAT.format(
            date=self.date[0:10],
            owner=self.owner,
            path=self.path,
            comment=self.comment,
            sep=self.sep,
        )
        # trim trailing spaces if comment was empty
        return string.rstrip()


class SvnListHandler(sax.handler.ContentHandler):

    """XML handler suitable for the output of the svn list command. It looks
    for lock data only.
    """

    def __init__(self, args):
        self.xpath = []
        self.lock = None
        self.args = args

    def startElement(self, name, attrs):
        """Called each time a <whatever> tag is encountered"""
        self.xpath.append(name)

    def endElement(self, name):
        """Called each time a </whatever> tag is encountered"""
        del self.xpath[-1]

    def characters(self, content):
        """Called each time <we>BlaBla</we> is encountered"""
        if self.xpath[-1] == "name":  # name is the lock's path
            self.lock = Lock(content, self.args.separator, self.args.path)

        elif self.xpath[-1] == "owner":
            self.lock.owner = lookupRealName(content)

        elif self.xpath[-1] == "created":
            self.lock.date = content
            # this is the last piece of data in the xml stream,
            # so trigger lock printing
            handleLock(self.lock, self.args)

        elif self.xpath[-1] == "comment":
            self.lock.comment = content


def handleLock(lock, args):
    """Filters, reports and optionally breaks a lock"""
    if args.user in lock.owner:
        print(lock)
        if args.doBreak:
            breakLock(lock)


def lookupRealName(causer):
    CMD1 = "wmic useraccount where (domain='EUROPE' and name='{}') get FullName 2>nul| findstr ,"
    CMD = CMD1.format(causer)
    try:
        fullName = subprocess.check_output(CMD, shell=True).decode().rstrip()
    except subprocess.CalledProcessError:
        # name could not be looked up, note that it is findstr that returns
        # error code 1 if searchstring can't be found, not wmic.
        warnMsg = "Warning: {} not found in Active Directory".format(causer)
        print(warnMsg, file=sys.stderr)
        fullName = causer
    return fullName


def buildSaxParser(args):
    handler = SvnListHandler(args)
    parser = sax.make_parser()
    parser.setContentHandler(handler)
    return parser


def parseArguments():
    parser = argparse.ArgumentParser(
        description=sys.modules[__name__].__doc__, epilog=EPILOG
    )
    parser.add_argument("path", type=str, help=HELP_PATH)
    parser.add_argument("-s", "--separator", type=str, help=HELP_SEP, default="  ")
    parser.add_argument("-u", "--user", type=str, help=HELP_USER, default="")
    parser.add_argument(
        "-b", "--break", dest="doBreak", action="store_true", help=HELP_BREAK
    )

    return parser.parse_args()


def getUrl(path):
    cmdString = 'svn info "{}"'.format(path)
    RE_URL = re.compile("^URL: (.*)$")
    proc = subprocess.Popen(cmdString, stdout=subprocess.PIPE)
    for line in proc.stdout:
        cline = line.decode(STDOUT_ENCODING).rstrip()
        match = RE_URL.search(cline)
        if match:
            url = match.group(1)
            print("Searching for locks in {}".format(url))
            return url

    # svn info couldn't give us an url, probably because user specified
    # a non-working copy path or non-existant url
    print("ERROR: path {} is invalid".format(path))
    sys.exit(1)


def breakLock(lock):
    cmdString = 'svn unlock "{}" --force'.format(lock.url())
    try:
        subprocess.check_call(cmdString)
    except:
        print("Lock could not be broken", file=sys.stderr)


def main():
    args = parseArguments()
    url = getUrl(args.path)
    cmdString = "svn list {} -R --xml".format(url)
    proc = subprocess.Popen(cmdString, stdout=subprocess.PIPE)
    source = sax.xmlreader.InputSource()
    source.setByteStream(proc.stdout)

    parser = buildSaxParser(args)
    parser.parse(source)


# All top-level code must be contained in main() function. This prevents
# the execution of this code when the module is imported by another one.
# When called using 'python xxx.py', main() is executed automatically.
if __name__ == "__main__":
    main()
