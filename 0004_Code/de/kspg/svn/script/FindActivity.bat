@echo off
:: =============================================================================
:: Gives you a list of ELE numbers that match the name of your activity.
::
:: SVN KEYWORDS
::  $HeadURL: https://svnserver.europe.kspag.de/svn/Projects/Internal2.0/BSW/ToolSupport/Subversion/trunk/04_Software/SvnScripts/src/de/kspg/svn/script/FindActivity.bat $
::  $LastChangedDate: 2015-09-16 09:37:50 +0200 (Mi, 16 Sep 2015) $
::  $LastChangedBy: CA43420 (Winzer, Wolfgang) $
::  $Revision: 65057 $
:: =============================================================================

set PROPERTY=kspg:activity
set REPO=https://svnserver/svn/projects

echo Welcome to FindActivity.bat. 
echo I will try to find the activity you are looking for.
echo(
echo SEARCH TIPS
echo - You can search forward (number) or backward (activity name).
echo - Don't use umlauts, use 'ae', 'oe', 'ue' and 'ss' instead.
echo - Search is case insensitive.
echo - You can use regular expressions. See 'help findstr' for details.
echo - If you want the full list, just hit enter.

:retry
echo(
set SEARCHSTRING=
set /p SEARCHSTRING="Search string: "

if "%SEARCHSTRING%" == "" (
	echo Listing everything
	svn propget %PROPERTY% %REPO% --depth immediates
	goto again
)

if "%SEARCHSTRING%" == "exit" (
	goto end
)

svn propget %PROPERTY% %REPO% --depth immediates | findstr /I "%SEARCHSTRING%"
if %errorlevel% NEQ 0 (
	echo No search results for "%SEARCHSTRING%".
)

:again
echo(
echo Try again or type 'exit' to leave script.
goto retry

:end

