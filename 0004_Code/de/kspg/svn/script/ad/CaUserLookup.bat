:: =============================================================================
:: Identifies the specificed CA-User non-interactively. If no CA-User is
:: specified, the batch works in interactive mode.
:: The Active Directory is used for the actual lookup.
:: The CA-User must be in the EUROPE Domain.
::
:: PARAMETERS
::  %1 - The causer to look up. If not specified, batch works in interactive mode
::
:: SVN KEYWORDS
::  $HeadURL: https://svnserver/svn/internal/KSPG_Konfigurationsmanagement/trunk/04_Software/CommandLineScripts/UpdateSvnMirror.bat $
::  $LastChangedDate: 2012-09-18 08:05:18 +0200 (Di, 18 Sep 2012) $
::  $LastChangedBy: ca43420 $
::  $Revision: 627 $
:: =============================================================================
@echo off

SET user=%1
IF NOT "%1%" == "" goto LOOKUP

echo Welcome to Lookup-CA-User. Enter users (e.g. "CA43420") to search for their full name in the active directory (only EUROPE domain).

:PROMPT

:: overwrite last round's input
SET user=EMPTY

:: prompt user for ca-user
SET /P user=Please enter CA-User or 'e' to exit: || SET user=e

:: trim trailing spaces, especially useful if input is piped in
CALL :TRIM %user% user

:: provide means to leave the batch
if %user%==e exit /B 0


:LOOKUP
:: use wmi console to query active directory and throw away the chaff
wmic useraccount where (domain='EUROPE' and name='%user%') get FullName | findstr /V FullName

IF "%1%" == "" goto PROMPT

goto END


:TRIM
SET %2=%1
GOTO :EOF

:END