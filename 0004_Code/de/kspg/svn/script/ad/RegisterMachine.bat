:: =============================================================================
:: Writes the caller's computername to a csv file on a network share.
:: 
:: PRECONDITION
::  The caller has write access to the csv file.
::
:: SVN KEYWORDS
::  $HeadURL: https://svnserver.europe.kspag.de/svn/Projects/Internal2.0/BSW/ToolSupport/Subversion/trunk/04_Software/SvnScripts/src/de/kspg/svn/script/ad/RegisterMachine.bat $
::  $LastChangedDate: 2014-05-05 11:36:36 +0200 (Mo, 05 Mai 2014) $
::  $LastChangedBy: CA43420 (Winzer, Wolfgang) $
::  $Revision: 30962 $
:: =============================================================================
@echo off

set CSVFILE=X:\KSPG_FuT\40_FuT_Zentrale_Entwicklung\60_Elektronik\30_Administration\Maschinenkonten.csv
set TMPFILE=tmp.txt

wmic useraccount where (domain='EUROPE' and name='%USERNAME%') get FullName | findstr /V FullName > %TMPFILE%
set /P REALNAME= < %TMPFILE%
del %TMPFILE%

echo %DATE%;%TIME:~0,8%;%USERNAME%;%REALNAME%;%COMPUTERNAME% >> %CSVFILE%
echo Danke! Daten wurden geschrieben nach %CSVFILE%
pause


