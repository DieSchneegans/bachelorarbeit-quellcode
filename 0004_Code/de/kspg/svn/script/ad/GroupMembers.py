#! python3
"""
Lists all members of a group stored in active directory.
"""

"""
$HeadURL: https://svnserver.europe.kspag.de/svn/Projects/Internal2.0/BSW/ToolSupport/Subversion/trunk/04_Software/SvnScripts/src/de/kspg/svn/script/ad/GroupMembers.py $
$LastChangedDate: 2014-05-05 11:36:36 +0200 (Mo, 05 Mai 2014) $
$LastChangedBy: CA43420 (Winzer, Wolfgang) $
$Revision: 30962 $
"""
import sys
import subprocess
import argparse
import locale
import re

HELP_GROUP = """
Group name or part of the group name. If the parameter fits several groups,
all of them are displayed. Case-sensitive. May include dashes (-). If the
group contains spaces, enclose parameter in double colons ("my Group"). If
you want to list ALL groups available (do you really?), use empty string ("")
as parameter.
"""

HELP_CSV = """
If set, a table is generated instead of a list of users with groups for
headers. Please note that with this option enabled, you will not see empty
groups.
"""

# command line tools configuration
CMD_NET_GROUP_LIST = "NET GROUP /DOMAIN"
CMD_NET_GROUP = 'NET GROUP "{}" /DOMAIN'
CMD_NET_USER = 'NET USER "{}" /DOMAIN'

# tool output parsing configuration
RE_GROUP = re.compile("^\*(.*)$")
RE_NAME = re.compile("^Vollst.ndiger Name\s+(.*)\s*$")
STDOUT_ENCODING = "cp850"

# output configuration
SEPARATOR = ";"
OUTLINE_CSV = "{GROUP}{SEP}{USER}{SEP}{FULLNAME}"
OUTLINE_CSV_HEADER = OUTLINE_CSV.format(
    GROUP="Group", USER="User", FULLNAME="Fullname", SEP=SEPARATOR
)
OUTLINE_LIST = "{USER} ({FULLNAME})"


def main():
    args = parseArgs()
    proc = subprocess.Popen(CMD_NET_GROUP_LIST, stdout=subprocess.PIPE)
    if args.csv:
        print(OUTLINE_CSV_HEADER)
    for line in proc.stdout:
        cline = line.decode(STDOUT_ENCODING).rstrip()
        match = RE_GROUP.search(cline)
        if match:
            matchedGroup = match.group(1)
            if args.group in matchedGroup:
                showMembers(matchedGroup, args.csv)


def parseArgs():
    parser = argparse.ArgumentParser(description=sys.modules[__name__].__doc__)
    parser.add_argument("group", type=str, help=HELP_GROUP)
    parser.add_argument("--csv", dest="csv", action="store_true", help=HELP_CSV)
    return parser.parse_args()


def showMembers(groupname, asCsv):
    MARKER_START = "------"
    MARKER_STOP = "Der Befehl wurde"
    if not asCsv:
        print("----- matching Group: {} -----".format(groupname))

    with subprocess.Popen(
        CMD_NET_GROUP.format(groupname), stdout=subprocess.PIPE
    ) as proc:
        isParsingUsers = False
        for line in proc.stdout:
            cline = line.decode(STDOUT_ENCODING).rstrip()
            if isParsingUsers:
                if MARKER_STOP in cline:
                    isParsingUsers = False
                else:
                    parseUsers(groupname, cline, asCsv)

            elif MARKER_START in cline:
                isParsingUsers = True
            else:
                pass  # stay inactive


def parseUsers(group, cline, asCsv):
    users = cline.split()
    for user in users:
        printUser(group, user, asCsv)


def printUser(group, user, asCsv):
    fullname = lookupUser(user)
    if asCsv:
        output = OUTLINE_CSV.format(
            GROUP=group, USER=user, FULLNAME=fullname, SEP=SEPARATOR
        )
    else:
        output = OUTLINE_LIST.format(USER=user, FULLNAME=fullname)
    print(output)


def lookupUser(user):
    with subprocess.Popen(
        CMD_NET_USER.format(user), stdout=subprocess.PIPE, stderr=subprocess.DEVNULL
    ) as proc:
        for line in proc.stdout:
            cline = line.decode(STDOUT_ENCODING).rstrip()

            match = RE_NAME.search(cline)
            if match:
                return match.group(1)

    # no match, probably because command returned error instead of output
    return "not found in AD"


if __name__ == "__main__":
    main()
