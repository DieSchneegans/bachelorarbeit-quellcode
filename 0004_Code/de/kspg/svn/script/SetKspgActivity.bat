:: This script is still under construction. The following steps should be
:: automated eventually:
::
:: 1) filter KapaTracker activity list to ELE
:: 2) select columns "activity" and "number" and copy-paste them into the script
:: 3) Eliminiate umlauts
:: 4) replace (.*)\t(.*) by svn propset kspg:activity "$1" $2
:: 5) run the script
:: 6) commit

svn propset kspg:activity "Audi Regelklappe / Rezirkulationsventil" ELE-00006
svn propset kspg:activity "Volvo Bypass" ELE-00007
svn propset kspg:activity "Labor_Actuators" ELE-00008
svn propset kspg:activity "EAM Bi" ELE-00017
svn propset kspg:activity "FORD DRAGON EDR-E 7.06241.00.0: SENT" ELE-00018
svn propset kspg:activity "AUDI 2/3 Wege Klappe" ELE-00021
svn propset kspg:activity "EGR Coolermod. FIAT 2.2L D - Fam.B =>LOI" ELE-00040
svn propset kspg:activity "Renault Abgasklappe - EAM Kostenreduzierung" ELE-00043
svn propset kspg:activity "EAM-S Acoustic Flap Mazda" ELE-00052
svn propset kspg:activity "Grundlagen Sensorversorgung" ELE-00057
svn propset kspg:activity "Labor_Automotive Emission Systems" ELE-00009
svn propset kspg:activity "DV5R EGR" ELE-00026
svn propset kspg:activity "VW cEM-EGR" ELE-00046
svn propset kspg:activity "Ford cEM-EGR Gasoline" ELE-00047
svn propset kspg:activity "GM cEM-EGR SENT" ELE-00048
svn propset kspg:activity "cEM-EGR Basisentwicklung EC-Motor" ELE-00049
svn propset kspg:activity "FPT F1C EGR ProMaster valve test procedure" ELE-00066
svn propset kspg:activity "Basisentwicklung EC-Motor - cEM-AGR" ELE-00067
svn propset kspg:activity "Heavy Duty Valve - Platfrom project" ELE-00003
svn propset kspg:activity "YCM_EGR_PWM" ELE-00030
svn propset kspg:activity "FPT ETC" ELE-00032
svn propset kspg:activity "HD EGR Poppet Valve" ELE-00036
svn propset kspg:activity "DAF R�ckl�ufer EEPROM" ELE-00042
svn propset kspg:activity "SiDiT" ELE-00054
svn propset kspg:activity "Kompakt-Aktuator" ELE-00055
svn propset kspg:activity "DAF-R�ckl�ufer" ELE-00058
svn propset kspg:activity "Isuzu Aquise BPV 16Bit" ELE-00061
svn propset kspg:activity "Isuzu Aquise AGR 8 Bit" ELE-00062
svn propset kspg:activity "FPT Frischluftklappe" ELE-00063
svn propset kspg:activity "DC-BPV" ELE-00064
svn propset kspg:activity "T�mosan AGR" ELE-00065
svn propset kspg:activity "Labor_Commercial Diesel Systems" ELE-00010
svn propset kspg:activity "Labor_Solenoid Valves" ELE-00011
svn propset kspg:activity "CSVi" ELE-00031
svn propset kspg:activity "A9_CU" ELE-00034
svn propset kspg:activity "UbC" ELE-00035
svn propset kspg:activity "Labor_Variable Valve Train Systems" ELE-00012
svn propset kspg:activity "CWA Gen5" ELE-00024
svn propset kspg:activity "WUP4S" ELE-00002
svn propset kspg:activity "WUP4M" ELE-00022
svn propset kspg:activity "WUP3L_EA211_China" ELE-00025
svn propset kspg:activity "CWP35_PWM_IF" ELE-00027
svn propset kspg:activity "CWP35_Production_Support" ELE-00028
svn propset kspg:activity "WUP3L_VW_Kein Alive nach POR" ELE-00029
svn propset kspg:activity "WUP4M 3phasig" ELE-00033
svn propset kspg:activity "Projekt Purge Pump" ELE-00037
svn propset kspg:activity "WUP3L_EA211_China" ELE-00038
svn propset kspg:activity "EOP_Renault" ELE-00039
svn propset kspg:activity "EOVP" ELE-00041
svn propset kspg:activity "WUP3Lx" ELE-00051
svn propset kspg:activity "WUP3L-China EMV-Optimierung" ELE-00053
svn propset kspg:activity "WUP3Lx-3ph" ELE-00056
svn propset kspg:activity "ECE-400" ELE-00059
svn propset kspg:activity "EOP for Getrag 6DCT451" ELE-00068
svn propset kspg:activity "WUP3L_LIN" ELE-00069
svn propset kspg:activity "Labor_Pierburg Pump Technology" ELE-00013
svn propset kspg:activity "eBooster eAC" ELE-00005
svn propset kspg:activity "Labor_Vorentwicklung" ELE-00014
svn propset kspg:activity "Hymotion" ELE-00050
svn propset kspg:activity "Ansteuerung FVCP" ELE-00060
svn propset kspg:activity "Grundlagenentw. Methoden 2015 M-CTEE" ELE-00044
svn propset kspg:activity "Spice Improvement Int. Entw.-Lstg. 2015" ELE-00045
svn propset kspg:activity "Labor_Zentrale Entwicklung Elektronik" ELE-00015
