@echo off
:: Uses CaUserConverter.py to perform maintenance job on all repositories
:: This script must be run by a user with network privileges (any domain user, not a local user)
:: Also keep in mind that CaUserConverter.py uses other classes which it takes from the python install directory. If you made
:: changes to those classes, call build-and-install.bat before you run this script.
python CaUserConverter.py -l 3 -f E:\SvnRepositories\CaUserLookupTable.csv https://svnserver.europe.kspag.de/svn/playground >> converter.log
python CaUserConverter.py -l 3 -f E:\SvnRepositories\CaUserLookupTable.csv https://svnserver.europe.kspag.de/svn/projects >> converter.log
python CaUserConverter.py -l 3 -f E:\SvnRepositories\CaUserLookupTable.csv https://svnserver.europe.kspag.de/svn/M-CTA >> converter.log
python CaUserConverter.py -l 3 -f E:\SvnRepositories\CaUserLookupTable.csv https://svnserver.europe.kspag.de/svn/M-BE >> converter.log