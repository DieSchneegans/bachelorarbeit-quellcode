:: =============================================================================
:: Finds svn:mergeinfo scattered throughout the specified path
:: No working copy is needed
::
:: PARAMETERS
::  %1 - Path in WC or URL of repository folder
::
:: SVN KEYWORDS
::  $HeadURL: https://svnserver.europe.kspag.de/svn/Projects/Internal2.0/BSW/ToolSupport/Subversion/trunk/04_Software/SvnScripts/src/de/kspg/svn/script/FindMergeInfo.bat $
::  $LastChangedDate: 2014-06-25 11:08:35 +0200 (Mi, 25 Jun 2014) $
::  $LastChangedBy: CA43420 (Winzer, Wolfgang) $
::  $Revision: 42542 $
:: =============================================================================
@echo off

IF "%1"=="" GOTO E_MISSING_PARAM1

echo Looking for svn:mergeinfo in %1
svn propget svn:mergeinfo %1 -R | findstr /B /V /C:"/"
GOTO SUCCESS

:E_MISSING_PARAM1
echo Please specify a working copy path or respository URL to search!
GOTO USAGE

:USAGE
echo Example usage 1: FindMergeInfo.bat C:\projects\myProject
echo Example usage 2: FindMergeInfo.bat https://svnserver/svn/projects/myProject

:SUCCESS