"""
Creates an URL that points to a fixed revision of the given file in the
repository (https://svnserver.../myFile.docx?r=71348). The link will be
copied to your clipbaord so you can easily paste it into an email.

$HeadURL: https://svnserver.europe.kspag.de/svn/Projects/Internal2.0/BSW/ToolSupport/Subversion/trunk/04_Software/SvnScripts/src/de/kspg/svn/script/CopyUrl.py $ # @IgnorePep8
$LastChangedDate: 2019-05-08 15:02:27 +0200 (Mi, 08 Mai 2019) $
$LastChangedBy: CA46800 (Schmorleiz, Tobias) $
$Revision: 149709 $
"""

import sys
import subprocess
import argparse
import re
import os

HELP_URL = """
Path of file under revision in a working copy. May be absolute or relative.
"""

EPILOG = """
Creates an URL that points to a fixed revision of the given file in the
repository (https://svnserver.../myFile.docx?r=71348). The link will be
copied to your clipbaord so you can easily paste it into an email.
"""

STDOUT_ENCODING = "cp850"


def addToClipBoard(text):
    # dirty workaround for not being able to store "|" symbols within a string to the clipboard:
    # - create a new textfile, write clipboard text into it, send content of textfile to clipboard and delete previous created textfile.
    ClipText = open("ClipText.txt", "w")
    ClipText.write(text.strip())
    ClipText.close()

    cmd = "clip < ClipText.txt"
    try:
        subprocess.check_call(cmd, shell=True)
    except:
        print("Could not paste to clipboard", file=sys.stderr)

    os.remove("ClipText.txt")


def buildUrl(url, revision):  # Funktion zum Zusammensetzen der URL
    extra = "?p="
    return url + extra + revision


def buildFancyUrl(
    url, revision
):  # Funktion zum Zusammensetzen der URL und der Kurzbezeichnung
    Expr = r"[\s\S]+/([\w\W]+)\?[\w\W]+"
    ExpUrl = buildUrl(url, revision)
    SplitUrl = re.search(Expr, ExpUrl)
    ShortName = SplitUrl.group(1)
    return "[" + ShortName + "|" + ExpUrl + "]"


def parseArguments():
    parser = argparse.ArgumentParser(
        description=sys.modules[__name__].__doc__, epilog=EPILOG
    )
    parser.add_argument("path", type=str, help=HELP_URL)
    parser.add_argument(
        "-j", "--jira", action="store_true", help="create a fancy small link"
    )
    return parser.parse_args()


def SvnInfo(file):
    """ calls svn info on the given file and creates a dictionary from the output"""
    SEP = ":"
    svninfoCommand = 'svn info "' + file + '"'

    try:
        stdout = (
            subprocess.check_output(svninfoCommand, shell=True)
            .decode(STDOUT_ENCODING)
            .rstrip()
        )
        # store a list of pairs and discard lines that do not contain a pair
        lines = stdout.split("\n")
        pairs = [line.split(SEP, 1) for line in lines if SEP in line]
        infoDic = dict((pair[0].strip(), pair[1].strip()) for pair in pairs)
        return infoDic
    except subprocess.CalledProcessError:
        warnMsg = "Error: {} not found in repository".format(file)
        print(warnMsg, file=sys.stderr)
        print(
            "Probably the path you specified pointed to a local file, not a versioned one."
        )
        exitScript(1)


def exitScript(exitCode):
    if exitCode > 0:
        input("Press Enter to close window")
    sys.exit(exitCode)


def main():
    try:
        args = parseArguments()
        svnInfoDic = SvnInfo(args.path)
        url = svnInfoDic["URL"]
        revision = svnInfoDic["Revision"]
        if args.jira:
            concUrl = buildFancyUrl(url, revision)
        else:
            concUrl = buildUrl(url, revision)
        addToClipBoard(concUrl)
        print("The following URL can now be pasted from your clipboard")
        print(concUrl)
        exitScript(0)
    except Exception as ex:
        print("Unexpected error:", ex)
        exitScript(1)


# All top-level code must be contained in main() function. This prevents
# the execution of this code when the module is imported by another one.
# When called using 'python xxx.py', main() is executed automatically.
if __name__ == "__main__":
    main()
