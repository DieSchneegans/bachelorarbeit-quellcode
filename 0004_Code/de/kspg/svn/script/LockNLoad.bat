@echo off
:: =============================================================================
:: Updates, locks and finally opens the given document.
:: The lock comment is spiced up with your real name
::
:: PRECONDITIONS
:: - you need to be connected to the svn server to acquire locks
:: - your user must be registered in the active directory
:: - if file contains spaces, it has to be put in quotation marks ("my file.doc")
::
:: INTEGRATION INTO FREE COMMANDER
:: - With Free Commander, you can execute this script by pressing Ctrl+x
:: - Navigate to Extras > Programmfavoriten > Bearbeiten...
:: - Add a new group 'SvnScripts' if you don't have one yet
:: - Add a new program 'LockNLoad' to the group
:: - Fill in
::    - Programm   : C:\Spezial\SvnScripts\LockNLoad.bat
::    - Startordner: .
::    - Parameter  : %ActivSel%
:: - Set Tastenkürzle to Ctrl+x
::
:: SVN KEYWORDS
::  $HeadURL: https://svnserver.europe.kspag.de/svn/Projects/Internal2.0/BSW/ToolSupport/Subversion/trunk/04_Software/SvnScripts/src/de/kspg/svn/script/LockNLoad.bat $
::  $LastChangedDate: 2015-10-07 09:51:39 +0200 (Mi, 07 Okt 2015) $
::  $LastChangedBy: CA43420 (Winzer, Wolfgang) $
::  $Revision: 66254 $
:: =============================================================================

set TMPFILE=realname.txt


:: ---------------------------
:: update document
:: ---------------------------
svn update %1

:: ---------------------------
:: check lock status
:: ---------------------------
svn status -u --xml %1 | findstr opaquelocktoken > nul
if errorlevel 1 (
	echo file is free to lock
	goto lock
) 
	
echo file is already locked
svn status -u --xml %1 | findstr owner
svn status -u --xml %1 | findstr comment
svn status -u --xml %1 | findstr created

choice /C ASR /N /M "[A]bort, [S]teal lock or open [R]ead-only?"
if errorlevel 3 (
	echo opening read-only...
	goto open
)
if errorlevel 2 (
	echo stealing...
	set FORCE=--force
	goto lock
)
if errorlevel 1 (
	echo aborting...
	goto end
)

:: user pressed Ctrl+C
exit

:: ---------------------------
:: lock document
:: ---------------------------
:lock
:: look up your real name in active directory 
wmic useraccount where (domain='EUROPE' and name='%USERNAME%') get FullName | findstr /V FullName > %TMPFILE%
:: store result in variable
set /p REALNAME=<%TMPFILE%

:: lock document
svn lock %1 %FORCE% -m "document locked by %REALNAME%"
:: clean up
del %TMPFILE%

:: ---------------------------
:: open document
:: ---------------------------
:open
%1

:end

