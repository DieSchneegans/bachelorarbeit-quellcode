@echo off
SET PYTHONPATH=C:\Projects\_Internal\Konfigurationsmanagement\04_Software\ServerSide\HookScripts\PythonHooks

echo .
echo Testing Usage
echo --------------------------
python BatchExecTimer.py -h

echo .
echo Testing WITHOUT item count
echo --------------------------
python BatchExecTimer.py start
ping localhost > nul
python BatchExecTimer.py lap
ping localhost > nul
python BatchExecTimer.py stop

echo .
echo Testing WITH item count
echo --------------------------
python BatchExecTimer.py start -i 8
ping localhost > nul
python BatchExecTimer.py lap -i 4
ping localhost > nul
python BatchExecTimer.py stop -i 8

echo .
echo Testing misuse
echo --------------------------
