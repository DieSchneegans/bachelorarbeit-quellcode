:: =============================================================================
:: Finds svn:mergeinfo with Non-Inhertable paths in working copy.
::
:: SVN KEYWORDS
::  $HeadURL: https://svnserver.europe.kspag.de/svn/Projects/Internal2.0/BSW/ToolSupport/Subversion/trunk/04_Software/SvnScripts/src/de/kspg/svn/script/FindNonInheritable.bat $
::  $LastChangedDate: 2015-05-05 08:29:19 +0200 (Di, 05 Mai 2015) $
::  $LastChangedBy: CA43420 (Winzer, Wolfgang) $
::  $Revision: 57989 $
:: =============================================================================
@echo off

echo Looking for Non-Inheritable mergeinfo items. Please make sure your
echo current directory is the merge target root directory.
svn propget svn:mergeinfo | findstr *
