@echo off

set REPO=E:\SvnRepositories\Projects
svnadmin lstxns %REPO% > transactions.txt

for /F %%i in (transactions.txt) do (
	echo --- [Transaction %%i ] -----------------------------------------
	svnlook info %REPO% -t %%i
)
