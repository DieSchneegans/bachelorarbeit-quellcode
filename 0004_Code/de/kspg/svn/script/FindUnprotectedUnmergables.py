"""
Finds and protects all files in a subversion repository that are unmergable and
at the same time unprotected against
concurrent write access. The files are output as list to the log, histogrammed
by creator, creation date and by
file extension. The list is also output to csv for further analysis in Excel.
To be more precise an file is added to the list if it fulfills all of the
follwing criteria:
a) file is unmergable (at least not automatically by SVN)
b) file is not part of a tag
c) file does not carry the property svn:needs-lock
The script uses svn.exe rather than pysvn because svn.exe allows processing
each of the million files one by one discarding most immediately whereas pysvn
would first compile a list of length > 1 million. No progress would be visible
and it would be like begging for a memory overrun.
$HeadURL: https://svnserver.europe.kspag.de/svn/Projects/Internal2.0/BSW/ToolSupport/Subversion/trunk/04_Software/SvnScripts/src/de/kspg/svn/script/FindUnprotectedUnmergables.py $ # @IgnorePep8
$LastChangedDate: 2015-04-22 13:24:33 +0200 (Mi, 22 Apr 2015) $
$LastChangedBy: CA43420 (Winzer, Wolfgang) $
$Revision: 57451 $
"""

import os
import locale
import subprocess
import collections
import csv
from de.kspg.svn.stats.ExecTimer import ExecTimer
from de.kspg.svn.stats.Counters import Counters

############
# CONFIG
############

# if True, repository isn't changed
CFG_DRYRUN = False

# if True, the repos from CFG_REPO_URLS_DEV are used instead
# of CFG_REPO_URLS_PROD
CFG_USE_DEV_REPO = False

# The production repository to process
CFG_REPO_URL_PROD = "https://svnserver.europe.kspag.de/svn/projects"

# The development/test respositories to process
CFG_REPO_URL_DEV = "https://localhost/svn/projects"

# specify "" if you want to process the whole repository, "/path/to/folder"
# otherwise
CFG_PROJECT = ""

# if True, more output will be printed (e.g. each file processed)
CFG_VERBOSE = False

# Script will print a status report every n files it parsed
CFG_REPORT_CYCLE = 1000

# This is where the data will stored
CFG_CSV_OUTPUT = "UnprotectedUnmergables.csv"

# delimiter to be used in csv output
CFG_CSV_DELIMITER = ";"

# folder where temporary checkouts happen. Will be deleted periodically
CFG_CHECKOUT_STAGE = "C:\\Temp\\costage"

# separator on target checkout system
PATH_SEP = "\\"

# svn log message to use when committing repairs
CFG_REPAIR_MESSAGE = "Added missing property svn:needs-lock"

# extensions that indicate a non-mergable file. Should be taken from the
# subversion config file.
CFG_NONMERGABLES = [
    "odc",
    "odf",
    "odg",
    "odi",
    "odp",
    "ods",
    "odt",
    "doc",
    "xls",
    "ppt",
    "vsd",
    "docx",
    "docm",
    "dotx",
    "dotm",
    "xlsx",
    "xlsm",
    "xltx",
    "xltm",
    "xlsb",
    "xlam",
    "pptx",
    "pptm",
    "ppsx",
    "ppsm",
    "potx",
    "potm",
    "ppam",
    "sldx",
    "sldm",
    "one",
    "onetoc2",
    "onetmp",
    "onepkg",
    "thmx",
    "xmind",
    "brd",
    "dgn",
    "dwg",
    "dxf",
    "sch",
    "ser",
    "eap",
    "fme",
    "mpp",
    "mpt",
]

# estimate of the number of files and directories in the repository.
# Unfortunately, it is impossible to read this number from a repo in an
# efficient way, listing everything and counting the lines takes
# more than an hour. You can use
# svn list -R https://svnserver.europe.kspag.de/svn/projects | find /C "/"
# or you can run this script once and see how many paths it processed
URLS_IN_REPO = 1466116

############
# FUNCTIONS
############


def main():
    """Sets up timers and statistics, evaluates basic options and processes
    the configured repository.
    """
    print("Script starting")
    if CFG_USE_DEV_REPO:
        repoToProcess = CFG_REPO_URL_DEV
    else:
        repoToProcess = CFG_REPO_URL_PROD
    timer = ExecTimer("paths", URLS_IN_REPO)
    timer.Start()
    statistics = ProcessRepository(repoToProcess, timer)
    timer.Stop(statistics.Get("files"))
    statsHeader = "Statistics for {}".format(repoToProcess)
    statistics.Print(header=statsHeader, showPercentage=False)
    print("Script stopping")


def PrintList(name, listToPrint):
    """Prints a python list in a pretty way."""
    MAX_OUTPUT_LENGHT = 10
    print("----- {0:15} ({1:6}) -----".format(name, listToPrint.__len__()))
    for item in listToPrint[0:MAX_OUTPUT_LENGHT]:
        print(item)
    if len(listToPrint) > MAX_OUTPUT_LENGHT:
        print("...")
    print("")


def PrintSet(name, setToPrint):
    """Prints a python set in a pretty way."""
    print("----- {0:15} ({1:6}) -----".format(name, setToPrint.__len__()))
    for item in setToPrint:
        print(item)
    print("")


def PrintDict(name, dictToPrint):
    """Prints a python dictionary in a pretty way."""
    print("----- {0:20} ({1:6}) -----".format(name, dictToPrint.__len__()))
    for a, b in sorted(dictToPrint.items()):
        print(a, "->", b)
    print("")


def ProcessFile(url, stats, unprotectedDict):
    """Checks whether the given file is an unprotected non-mergable and if so,
    repairs it.
    """
    if CFG_VERBOSE:
        print(url)
    stats.Inc("files")
    if not "/tags/" in url:
        stats.Inc("untagged")
        if IsNonMergable(url):
            stats.Inc("non-mergable")
            if IsUnprotected(url):
                if CFG_VERBOSE:
                    print("found unprotected")
                stats.Inc("unprotected")
                AddUnprotected(unprotectedDict, url)
                RepairUnprotected(url, stats)


def ProcessRepository(repo, timer):
    """Checks all files in the repository for unprotected non-mergables and
    repairs those found.
    """
    encoding = locale.getdefaultlocale()[1]
    stats = Counters(
        [
            "files",
            "untagged",
            "non-mergable",
            "unprotected",
            "repaired",
            "failed repairs",
        ]
    )
    CMD_STRING = "svn list {url} -R".format(url=repo + CFG_PROJECT)
    proc = subprocess.Popen(CMD_STRING, stdout=subprocess.PIPE)
    unprotectedDict = dict()
    for line in proc.stdout:
        path = line.decode(encoding).rstrip()
        url = repo + CFG_PROJECT + "/" + path
        ProcessFile(url, stats, unprotectedDict)
        # intermediary report
        if stats.Get("files") % CFG_REPORT_CYCLE == 0:
            ReportResults(stats, unprotectedDict, isFinal=False)
            timer.Lap(stats.Get("files"))
    ReportResults(stats, unprotectedDict, isFinal=True)
    return stats


def RepairUnprotected(url, stats):
    """Protects the given file by adding the svn:needs-lock property to it.
    This requires a checkout, property modification and commit.
    """
    print("")
    print("Repairing {}".format(url))
    print("-----------------------")
    # it's safer to clean up before just before instead of after using the
    # location.
    # Also, it's nice to be able to see the last item processed after crashes
    BulldozeCheckoutStage()
    try:
        wcpath = CheckoutSingleFile(url)
        PropsetNeedsLock(wcpath)
        CommitRepair(wcpath)
        stats.Inc("repaired")
    except:
        print("Repairing failed")
        stats.Inc("failed repairs")


def CheckoutSingleFile(url):
    """Performs an svn checkout on a single file. Note that svn does not
    support single-file checkouts natively. Returns the file system path of
    the file checked out.
    """
    parentUrl, filename = url.rsplit("/", 1)
    CO_STRING_RAW = 'svn checkout "{url}" {costage} --depth=empty'
    CO_STRING = CO_STRING_RAW.format(url=parentUrl, costage=CFG_CHECKOUT_STAGE)
    wcpath = CFG_CHECKOUT_STAGE + PATH_SEP + filename
    print(CO_STRING)
    subprocess.Popen(CO_STRING).communicate()
    UP_STRING = 'svn update "{path}"'.format(path=wcpath)
    print(UP_STRING)
    subprocess.Popen(UP_STRING).communicate()
    return wcpath


def PropsetNeedsLock(wcpath):
    """Sets the svn property svn:needs-lock on file at the given path.
    The path must belong to a working copy.
    """
    CMD_STRING_RAW = '''svn propset svn:needs-lock '*' "{path}"'''
    CMD_STRING = CMD_STRING_RAW.format(path=wcpath)
    print(CMD_STRING)
    subprocess.check_call(CMD_STRING)


def CommitRepair(wcpath):
    """Commits the repair process to the svn server with a predefined
    message.
    """
    CMD_STRING_RAW = 'svn commit "{path}" --message "{msg}"'
    CMD_STRING = CMD_STRING_RAW.format(path=wcpath, msg=CFG_REPAIR_MESSAGE)
    print(CMD_STRING)
    if not CFG_DRYRUN:
        subprocess.check_call(CMD_STRING)


def BulldozeCheckoutStage():
    """Delete an existing folder at the configured checkout path. This is
    typically the folder from the last repair action. If the folder does not
    exist, the function does nothing, not even complain.
    """
    if os.path.exists(CFG_CHECKOUT_STAGE):
        CMD_STRING = "rmdir /S /Q {}".format(CFG_CHECKOUT_STAGE)
        print(CMD_STRING)
        subprocess.check_call(CMD_STRING, shell=True)


def AddUnprotected(unprotectedDict, url):
    """Memorizes the given url in the list of unprotected non-mergables
    together with some of its properties.
    """
    creation = Creation(url)
    creator = creation[0]
    month = creation[1][0:7]
    unprotectedDict[url] = (FileExtension(url), creator, month)


def ReportHistogram(name, tuples, pos):
    """A histogram is calculated from the data given and printed.
    Parameters
    -------------
    name : str
        The header of the histogram
    tuples : list of tuples
        A table of data, one column of which contains the values to group and
        count
    pos : int
        indicates value column of the tuples by which to group
    """
    valueList = [entry[pos] for entry in tuples]
    histogram = collections.Counter(valueList)
    PrintDict(name, histogram)


def ReportHeader(isFinal):
    """Prints the header of a either an intermediary or a final report."""
    print("")
    print("===================")
    if isFinal:
        print("FINAL REPORT")
    else:
        print("INTERMEDIARY REPORT")
    print("===================")


def ReportResults(stats, DataByPath, isFinal=True):
    """Prints a report of the scripts results (so far).
    The report contains a list of the unprotected non-mergables, statistic data
    like counts and histograms and, last but not least, execution time
    count/prediction.
    """
    ReportHeader(isFinal)
    PrintDict("Unprotected Files", DataByPath)
    tuples = DataByPath.values()
    ReportHistogram("Extensions", tuples, 0)
    ReportHistogram("Creators", tuples, 1)
    ReportHistogram("Creation Months", tuples, 2)
    stats.Print()
    print("")
    CreateCsvReport(DataByPath)


def CreateCsvReport(DataByPath):
    """Writes all unprotected non-mergables to a csv file.
    Parameters
    ----------
    DataByPath : list of dict
        A list of unprotected non-mergables, each list item is a dictionary
        with tuple values: url -> (extension, creator, creation date)
    """
    print("Saving data to {0}".format(CFG_CSV_OUTPUT))
    with open(CFG_CSV_OUTPUT, "w", newline="") as f:
        writer = csv.writer(f, delimiter=CFG_CSV_DELIMITER)
        writer.writerow(["extension", "creator", "creation date", "url"])
        flatlist = [value + (key,) for key, value in DataByPath.items()]
        writer.writerows(flatlist)
        f.close()


def FileExtension(url):
    """Returns the extension of file the given url points to."""
    return url.split(".")[-1]


def IsNonMergable(url):
    """Returns True if the url points to a file that is specified as
    non-mergable by CFG_NONMERGABLES.
    """
    return FileExtension(url) in CFG_NONMERGABLES


def IsUnprotected(url):
    """Returns True if the file pointed to by url does not bear the
    svn:needs-lock property.
    """
    CMD_STRING = 'svn propget svn:needs-lock "{url}"'.format(url=url)
    proc = subprocess.Popen(CMD_STRING, stdout=subprocess.PIPE)
    stdout = proc.communicate()[0].decode()
    strippedOut = stdout.rstrip()
    if CFG_VERBOSE:
        MSG = "svn:needs-lock={} ({} characters)"
        print(MSG.format(strippedOut, len(stdout)))
    return len(strippedOut) == 0


def Creation(url):
    """Returns a tuple (creator, creation date) for the given file.
    The creator is the svn user that added the file to svn, correspondlingy the
    creation date is the date the user did so.
    """
    CMD_STRING = 'svn log "{url}" -q --incremental'.format(url=url)
    proc = subprocess.Popen(CMD_STRING, stdout=subprocess.PIPE)
    stdout = proc.communicate()[0].decode()
    try:
        lastLine = stdout.splitlines()[-1]
        creator = lastLine.split("|")[1].strip()
        date = lastLine.split("|")[2].strip()
    except:
        print("Failed to parse creation from the following stdout:")
        print(stdout)
        creator = "failed parsing"
        date = "failed parsing"
    return creator, date


# all top-level code was moved into a main() function. This prevents
# the execution of this code when the module is imported by another one.
# When called using 'python CaUserConverter.py', main() is executed.
# automatically
if __name__ == "__main__":
    main()
