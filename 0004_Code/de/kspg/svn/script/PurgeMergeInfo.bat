:: =============================================================================
:: Deletes svn:mergeinfo scattered below the specified working copy path.
:: Merginfo is NOT deleted from the root of the specified tree, because
:: this is probably the only location you want to keep mergeinfo in.
::
:: Note: The changes have to be committed afterwards, the script does not do
:: this for you. This is to encourage inspection of the changes before commit 
:: and it allows for individual comments.
::
:: PARAMETERS
::  %1 - Path in WC (without trailing backslash!)
::
:: SVN KEYWORDS
::  $HeadURL: https://svnserver.europe.kspag.de/svn/Projects/Internal2.0/BSW/ToolSupport/Subversion/trunk/04_Software/SvnScripts/src/de/kspg/svn/script/PurgeMergeInfo.bat $
::  $LastChangedDate: 2014-04-29 13:47:47 +0200 (Di, 29 Apr 2014) $
::  $LastChangedBy: CA43420 (Winzer, Wolfgang) $
::  $Revision: 30807 $
:: =============================================================================
@echo off

IF "%1"=="" GOTO E_MISSING_PARAM1

echo %0: Checking for existing modification of root folder
svn status "%1" -N | findstr /B /C:" M"
IF %ERRORLEVEL%==0 GOTO E_HAS_MODIFICATION
echo %0: OK, no modification found

echo %0: Purging svn:mergeinfo from %1
svn propdel svn:mergeinfo "%1" -R -q
IF NOT %ERRORLEVEL%==0 GOTO E_UNKNOWN

echo %0: Undoing property modification of root folder:
svn revert "%1"
IF NOT %ERRORLEVEL%==0 GOTO E_UNKNOWN

echo %0: The following paths were purged (list might contain existing property modifications):
svn status "%1" | findstr /B /C:" M"
IF NOT %ERRORLEVEL%==0 GOTO E_UNKNOWN
set EXITCODE=0
GOTO END

:E_MISSING_PARAM1
echo ERROR: Please specify a working copy path or respository URL to search!
set EXITCODE=1
GOTO USAGE

:E_HAS_MODIFICATION
echo ERROR: The root folder of the path you specified contains modifications.
echo Those modifications would be lost, so please commit (or revert) them first.
set EXITCODE=2
GOTO END

:E_UNKNOWN
echo ERROR: An unknown error has occured.
set EXITCODE=3
GOTO USAGE

:USAGE
echo Example usage: PurgeMergeInfo.bat C:\projects\myProject

:END
exit /B %EXITCODE%