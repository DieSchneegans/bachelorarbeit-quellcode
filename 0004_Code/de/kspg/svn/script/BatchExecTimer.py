"""
Measures execution time and speed of batch scripts.

It is a wrapper around module ExecTimer.py which deals with two problems
1) python methods of a class can't be called by a batch file easily
2) object is garbage collected between two calls from batch file, state is not retained.

$HeadURL: https://svnserver.europe.kspag.de/svn/Projects/Internal2.0/BSW/ToolSupport/Subversion/trunk/04_Software/SvnScripts/src/de/kspg/svn/script/BatchExecTimer.py $
$LastChangedDate: 2014-04-29 13:28:20 +0200 (Di, 29 Apr 2014) $
$LastChangedBy: CA43420 (Winzer, Wolfgang) $
$Revision: 30805 $
"""

import sys
import pickle
import argparse
from de.kspg.svn.stats.ExecTimer import ExecTimer

CFG_FILENAME = "timer.pkl"

ACTION_HELP = """
Controls the timer state and triggers reporting.
'start' starts the timer and prints the starting time.
'stop' stops the timer and prints the stopping time as well as the measured processing speed if
the number of processed items is known.
'lap' keeps the timer running and generates an intermediary report including the speed (see 'stop')
and additionally a prediciton of the remaining execution time, if total items to
process have been passed at start time.
"""


ITEMS_HELP = """
Allows speed calculation and prediction of remaining time.
Has slightly different meaning depending on action.
START: number of items to process
LAP/STOP: items processed
"""

EPILOG = """
Example usage:
python BatchExecTimer.py start -i 8
"""


def main():
    """
    Constructor
    """
    args = parseArguments()
    # call using function pointer
    globals()[args.action](args.items)

    sys.exit(0)


def parseArguments():
    parser = argparse.ArgumentParser(
        description="Measures execution time and speed of batch scripts.", epilog=EPILOG
    )
    parser.add_argument("action", choices=["start", "lap", "stop"], help=ACTION_HELP)

    parser.add_argument("--items", "-i", type=int, help=ITEMS_HELP)
    return parser.parse_args()


def start(items):
    timer = ExecTimer(totalItems=items)
    timer.Start()
    SaveTimer(timer)


def lap(items):
    timer = LoadTimer()
    timer.Lap(items)


def stop(items):
    timer = LoadTimer()
    timer.Stop(items)
    # TODO probably best to delete file


def SaveTimer(obj):
    with open(CFG_FILENAME, "wb") as f:
        pickle.dump(obj, f, protocol=0)


def LoadTimer():
    with open(CFG_FILENAME, "rb") as f:
        return pickle.load(f)


if __name__ == "__main__":
    main()
