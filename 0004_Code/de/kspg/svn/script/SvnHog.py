# CONFIG VARIABLES
REPO_PARENT_URL = "https://svnserver.europe.kspag.de/svn"
REPO_URL = "https://svnserver.europe.kspag.de/svn/Projects"
REPO_BACKUP_PATH = "Backup/Projects/db/revs/"
OUTPUT_CSV_FILE = "analysis.csv"
OUTPUT_TIME_PLOT = "analysis_time_plot.png"
DAYS_TO_PROCESS = 365

# ARGPARSE-TEXTS
ARGPARSE_HELP_EPILOG = """
Example usage:
python SvnHog.py https://svnserver.europe.kspag.de/svn/Projects;
"""

ARGPARSE_HELP_STARTDATE = """
Specify the earliest day that should be checked.
Format: YYYY-mm-dd
"""

ARGPARSE_HELP_URL = """
Specify a custom URL that should be checked.
"""

# IMPORTS
import pysvn
import yaml
import csv
import datetime
import time
import os
import sys
import numpy as np
import pandas as pd
import argparse
import matplotlib.pyplot as plt
from tabulate import tabulate


#############
# FUNCTIONS #
#############

# Calculate a new DAYS_TO_PROCESS
def setStartDate(startDate):
    today = datetime.datetime.now()
    startTime = datetime.datetime.strptime(startDate, "%Y-%m-%d")
    timeDelta = today.date() - startTime.date()
    DAYS_TO_PROCESS = timeDelta.days


# Get a new stop time for the PySVN queries to Subversion
def getStopTime(daysDelta):
    DaysToProcess = datetime.timedelta(
        daysDelta
    )  # create time delta element from amount of days to process
    Today = datetime.datetime.now()  # get current time
    StopDate = Today - DaysToProcess  # get stop date
    return time.mktime(StopDate.timetuple())  # convert time to correct object for PySVN


# Assign a value to a specific key in a dict, or increase the value if the key is already stored
def assignOrIncrease(store, key, value):
    if key in store:
        store[key] += value  # increase value
    else:
        store[key] = value  # key doesn't exist yet, so assign initial value


# Get a readable timestamp from a given unix timestamp
def getTimestamp(unixTime):
    return datetime.datetime.fromtimestamp(unixTime).strftime("%Y-%m-%d %H:%M:%S")


###############
# MAIN SCRIPT #
###############

# 1. CHECK ARGUMENTS
parser = argparse.ArgumentParser(
    description=sys.modules[__name__].__doc__, epilog=ARGPARSE_HELP_EPILOG
)
parser.add_argument(
    "-s",
    "--startDate",
    dest="startDate",
    type=str,
    help=ARGPARSE_HELP_STARTDATE,
    default="",
    required=False,
)
parser.add_argument(
    "-u",
    "--url",
    dest="url",
    type=str,
    help=ARGPARSE_HELP_URL,
    default="",
    required=False,
)
arguments = parser.parse_args()

if len(arguments.startDate) > 0:
    setStartDate(arguments.startDate)

if len(arguments.url) > 0:
    REPO_URL = arguments.url

# 2. SETUP VARIABLES
SvnClient = pysvn.Client()  # new PySVN Client

# get the repo root URL (helpful for getting subdirectories)
RelativeUrl = REPO_URL[
    len(REPO_PARENT_URL) + 1 :
]  # relative url from the parent folder of the root repo dir
if RelativeUrl.count("/") > 0:
    RootRepoUrl = (
        REPO_PARENT_URL + "/" + RelativeUrl[0 : RelativeUrl.index("/")]
    )  # only get first sub-directoy (aka the main repo folder)
else:
    RootRepoUrl = (
        REPO_PARENT_URL + "/" + RelativeUrl
    )  # the relativeUrl is not going into furhter sub-directories, so take it directly
RemovableUrlPart = REPO_URL[
    len(RootRepoUrl) :
]  # this part should be found in the given changed_files path and then deleted


OutputFile = open(
    OUTPUT_CSV_FILE, "w", newline="", encoding="utf-8"
)  # open output csv file
OutputWriter = csv.writer(
    OutputFile, delimiter=",", quotechar="'", quoting=csv.QUOTE_MINIMAL
)  # configurate csv writer
OutputWriter.writerow(
    [
        "Date",
        "Revision",
        "Author",
        "Size",
        "Message",
        "Number of commit paths",
        "Project",
    ]
)  # write header for csv file

RepoSizeValues = {}  # dict with the ids and sizes of all commits
TempRepoGrowth = 0  # temporary variable to store the value between every commit

StopTime = getStopTime(DAYS_TO_PROCESS)  # get a stop time for PySVN

MainProject = REPO_URL.split("/")[
    -1
]  # get project name by using the last folder name specified in the url

TotalRepoGrowth = (
    0
)  # variable for storing the amount of data stored inside the repo with all commits

HoggingUsers = {}  # dict for all users who have made commits to subversion
HoggingProjects = {}  # dict for all projects which have received commits
HoggingCommits = []  # list for all commits


# 3. GET ALL COMMIT SIZES
CommitSizes = {}
for root, dirs, files in os.walk(
    REPO_BACKUP_PATH
):  # recursively check the passed folder
    for name in files:
        if os.path.isfile(os.path.join(root, name)):
            CommitSizes[name] = os.path.getsize(
                os.path.join(root, name)
            )  # the size of each commit is saved in the CommitSizes dict


# 4. FETCH THE LOGS
print("Fetching the logs for the specified repository...")
RepoLogs = SvnClient.log(
    REPO_URL,
    revision_start=pysvn.Revision(pysvn.opt_revision_kind.head),
    revision_end=pysvn.Revision(pysvn.opt_revision_kind.date, StopTime),
    discover_changed_paths=True,
    strict_node_history=True,
    limit=0,
    peg_revision=pysvn.Revision(pysvn.opt_revision_kind.unspecified),
    include_merged_revisions=False,
)  # get all the logs in the specified timespan
print("Fetched logs for the specified repository!")


# 5. ITERATE THROUGH COMMITS
for LogItem in RepoLogs:
    # check if a commit is valid (ignore, if it's hidden from the current user)
    if "author" not in LogItem:
        print(
            "Warning: Revision "
            + str(LogItem["revision"].number)
            + " can't be analyzed. Missing permissions?"
        )
        continue

    # check if file is existing in local dump (ignore commit if not, because it can't be checked in this case)
    if str(LogItem["revision"].number) not in CommitSizes:
        continue

    # get some helpful values into temporary variables...
    CommitSize = CommitSizes[
        str(LogItem["revision"].number)
    ]  # File size of the commit (in Bytes)
    PathCount = len(
        LogItem["changed_paths"]
    )  # Amount of files/paths modified by the commit

    # assign project name to the new entry
    Project = "undefined"  # fallback name for project field
    for item in LogItem["changed_paths"]:
        if (
            item["path"][1:].count("/") >= 1
        ):  # check if this is a directory (at least 1 slash after parent folder slash)
            if len(RemovableUrlPart) == 0:
                Project = item["path"].split("/")[1]
                break
            elif item["path"].count(RemovableUrlPart + "/") == 1:
                Project = item["path"][len(RemovableUrlPart) :].split("/")[
                    1
                ]  # use the first directory of the first item on the changelist
                break

    # increase the stored absolute Repo size
    TotalRepoGrowth += CommitSize

    # add commit size to user counter
    assignOrIncrease(HoggingUsers, LogItem["author"], CommitSize)

    # add important commit data to hogging commit list
    HoggingCommits.append(
        {
            "revision": LogItem["revision"].number,
            "size": CommitSize,
            "message": LogItem["message"].replace("\n", " ")[0:22],
            "author": LogItem["author"].replace("(", "").replace(")", "")[8:25],
        }
    )

    # add commit size to project (if not in project)
    assignOrIncrease(HoggingProjects, Project, CommitSize)

    # add commit to the RepoGrowthSeries
    RepoSizeValues[pd.to_datetime(LogItem["date"], unit="s")] = CommitSize

    # create entry in csv
    OutputWriter.writerow(
        [
            getTimestamp(LogItem["date"]),
            LogItem["revision"].number,
            LogItem["author"],
            CommitSize,
            LogItem["message"].replace("\n", " "),
            PathCount,
            Project,
        ]
    )


# CREATE SORTED LISTS OF TOP HOGGING USERS, COMMITS AND PROJECTS

HoggingUsersSorted = sorted(HoggingUsers.items(), key=lambda e: e[1], reverse=True)
HoggingCommitsSorted = sorted(HoggingCommits, key=lambda e: e["size"], reverse=True)
HoggingProjectsSorted = sorted(
    HoggingProjects.items(), key=lambda e: e[1], reverse=True
)


# CREATE CONSOLE OUTPUT
print("\n\nSTATISTICS\n==========\n")
print("Commits counted: " + str(len(RepoLogs)))
print("Total Repository Growth: " + str(TotalRepoGrowth) + " Bytes")


print("\n\n")
print("Most Hogging Users")
print("==================\n")
print(
    tabulate(
        HoggingUsersSorted[0:14],
        headers=["Author", "Size (Bytes)"],
        tablefmt="fancy_grid",
    )
)


print("\n\n")
print("Most Hogging Projects")
print("=====================\n")
print(
    tabulate(
        HoggingProjectsSorted[0:20],
        headers=["Project/Sub-Directory", "Size(Bytes)"],
        tablefmt="fancy_grid",
    )
)


print("\n\n")
print("Most Hogging Commits")
print("====================\n")
print(
    tabulate(
        HoggingCommitsSorted[0:20],
        headers={
            "revision": "Revision",
            "size": "Size (Bytes)",
            "message": "Message",
            "author": "Author",
        },
        tablefmt="fancy_grid",
    )
)


# CREATE PLOT
RepoGrowthSeries = pd.DataFrame.from_dict(
    RepoSizeValues, orient="index", columns=["size"]
)  # create data frame from RepoSizeValues
RepoGrowthSeries["cumulated"] = RepoGrowthSeries.loc[::-1, "size"].cumsum()[
    ::-1
]  # create cumulation (reversed, from old to new!)
RepoGrowthSeries.plot.line()  # Create a line type plot
plt.savefig(OUTPUT_TIME_PLOT, dpi=300)  # save the plot
