"""
Finds all files in a repository which miss a certain property and adds it.

The files are output as list to the log, histogrammed
by creator, creation date and by
file extension. The list is also output to csv for further analysis in Excel.

To be more precise a file is added to the list if it fulfills all of the
follwing criteria:
a) file is in configured folder (currently only tag folders can be excluded)
b) file has the configured extension 
c) file does not carry the configured property. The value of the property is not examined!

The script uses svn.exe rather than pysvn because svn.exe allows processing
each of the million files one by one whereas pysvn
would first compile a list of length > 1 million. No progress would be visible
and it would be like begging for a memory overrun.

$HeadURL: https://svnserver.europe.kspag.de/svn/Projects/Internal2.0/BSW/ToolSupport/Subversion/trunk/04_Software/SvnScripts/src/de/kspg/svn/script/AddMissingProperty.py $ # @IgnorePep8
$LastChangedDate: 2020-09-23 08:48:27 +0200 (Mi, 23 Sep 2020) $
$LastChangedBy: CA43420 (Winzer, Wolfgang) $
$Revision: 192549 $
"""

import os
import locale
import subprocess
import collections
import csv
from de.kspg.svn.stats.ExecTimer import ExecTimer
from de.kspg.svn.stats.Counters import Counters

############
# CONFIG
############

# if True, property is added. If False, only the statistics are generated.
CFG_DO_REPAIR = True

# if True, repository isn't changed (commit is skipped). Useful for testing the script.
CFG_DRYRUN = False

# if True, files in tags are left untouched
CFG_SKIP_TAGS = True

# The production repository to process
CFG_REPO_URL_PROD = "https://svnserver.europe.kspag.de/svn/playground"

# specify "" if you want to process the whole repository, "/path/to/folder"
# otherwise
CFG_PROJECT = ""

# if True, more output will be printed (e.g. each file processed)
CFG_VERBOSE = False

# Script will print a status report every n files it parsed
CFG_REPORT_CYCLE = 10000

# This is where the data will stored
CFG_CSV_OUTPUT = "AddMissingProperty.csv"

# delimiter to be used in csv output
CFG_CSV_DELIMITER = ";"

# folder where temporary checkouts happen. Will be deleted periodically
CFG_CHECKOUT_STAGE = "C:\\Temp\\costage"

# separator on target checkout system
PATH_SEP = "\\"

# the property which might be missing and which shall be set
CFG_PROPERTY = "svn:needs-lock"

# the value the missing property shall be set to
# note that for svn:needs-lock you may use ANY value, 'yes' or even 'bla' but NOT '*'
CFG_PROPVAL = "yes"

# svn log message to use when committing repairs
CFG_REPAIR_MESSAGE = "CM: Added missing property {0}".format(CFG_PROPERTY)

# extensions that should have the property.
CFG_EXTENSIONS = [
    "zip",
    "rar",
    "7z",
    "odc",
    "odf",
    "odg",
    "odi",
    "odp",
    "ods",
    "odt",
    "doc",
    "xls",
    "ppt",
    "vsd",
    "docx",
    "docm",
    "dotx",
    "dotm",
    "xlsx",
    "xlsm",
    "xltx",
    "xltm",
    "xlsb",
    "xlam",
    "pptx",
    "pptm",
    "ppsx",
    "ppsm",
    "potx",
    "potm",
    "ppam",
    "sldx",
    "sldm",
    "one",
    "onetoc2",
    "onetmp",
    "onepkg",
    "thmx",
    "mpp",
    "mpt",
    "xmind",
    "mmap",
    "brd",
    "dgn",
    "dwg",
    "dxf",
    "sch",
    "ser",
    "dbc",
    "fme",
    "fig",
]


# estimate of the number of files and directories in the repository.
# Unfortunately, it is impossible to read this number from a repo in an
# efficient way, listing everything and counting the lines takes
# more than an hour. You can use
# svn list -R https://svnserver.europe.kspag.de/svn/projects | find /C "/"
# or you can run this script once and see how many paths it processed
# 2020-09-22: project: 6949484
# 2020-09-22: playground: ???
URLS_IN_REPO = 6949484


############
# FUNCTIONS
############


def main():
    """Sets up timers and statistics, evaluates basic options and processes
    the configured repository.
    """
    print("Script starting")
    repoToProcess = CFG_REPO_URL_PROD  # useful for looping over repos
    timer = ExecTimer("paths", URLS_IN_REPO)
    timer.Start()
    statistics = ProcessRepository(repoToProcess, timer)
    timer.Stop(statistics.Get("files"))
    statsHeader = "Statistics for {}".format(repoToProcess)
    statistics.Print(header=statsHeader, showPercentage=False)
    print("Script stopping")


def PrintList(name, listToPrint):
    """Prints a python list in a pretty way."""
    MAX_OUTPUT_LENGHT = 10
    print("----- {0:15} ({1:6}) -----".format(name, listToPrint.__len__()))
    for item in listToPrint[0:MAX_OUTPUT_LENGHT]:
        print(item)
    if len(listToPrint) > MAX_OUTPUT_LENGHT:
        print("...")
    print("")


def PrintSet(name, setToPrint):
    """Prints a python set in a pretty way."""
    print("----- {0:15} ({1:6}) -----".format(name, setToPrint.__len__()))
    for item in setToPrint:
        print(item)
    print("")


def PrintDict(name, dictToPrint):
    """Prints a python dictionary in a pretty way."""
    print("----- {0:20} ({1:6}) -----".format(name, dictToPrint.__len__()))
    for a, b in sorted(dictToPrint.items()):
        print(a, "->", b)
    print("")


def ProcessFile(url, stats, lackingDict):
    """Checks whether the given file is missing the property and if so,
    repairs it.
    """
    if CFG_VERBOSE:
        print(url)
    stats.Inc("files")
    if not CFG_SKIP_TAGS or not "/tags/" in url:
        stats.Inc("eligible by folder")
        if HasEligibleExtension(url):
            stats.Inc("eligible by extension")

            if LacksConfiguredProperty(url):
                if CFG_VERBOSE:
                    print("found lacking")
                stats.Inc("lacking property")
                AddLacking(lackingDict, url)
                if CFG_DO_REPAIR:
                    RepairProperty(url, stats)


def ProcessRepository(repo, timer):
    """Checks all files in the repository for unprotected non-mergables and
    repairs those found.
    """
    encoding = locale.getdefaultlocale()[1]
    stats = Counters(
        [
            "files",
            "eligible by folder",
            "eligible by extension",
            "lacking property",
            "repaired",
            "failed repairs",
        ]
    )
    CMD_STRING = "svn list {url} -R".format(url=repo + CFG_PROJECT)
    proc = subprocess.Popen(CMD_STRING, stdout=subprocess.PIPE)
    lackingDict = dict()
    for line in proc.stdout:
        path = line.decode(encoding).rstrip()
        url = repo + CFG_PROJECT + "/" + path
        ProcessFile(url, stats, lackingDict)
        # intermediary report
        if stats.Get("files") % CFG_REPORT_CYCLE == 0:
            ReportResults(stats, lackingDict, isFinal=False)
            timer.Lap(stats.Get("files"))
    ReportResults(stats, lackingDict, isFinal=True)
    return stats


def RepairProperty(url, stats):
    """Adds the missing property to the file.
    This requires a checkout, property modification and commit.
    """
    print("")
    print("Repairing {}".format(url))
    print("-----------------------")
    # it's safer to clean up before just before instead of after using the
    # location.
    # Also, it's nice to be able to see the last item processed after crashes
    BulldozeCheckoutStage()
    try:
        wcpath = CheckoutSingleFile(url)
        SetProperty(wcpath)
        CommitRepair(wcpath)
        stats.Inc("repaired")
    except:
        print("Repairing failed")
        stats.Inc("failed repairs")


def CheckoutSingleFile(url):
    """Performs an svn checkout on a single file. Note that svn does not
    support single-file checkouts natively. Returns the file system path of
    the file checked out.
    """
    parentUrl, filename = url.rsplit("/", 1)
    CO_STRING_RAW = 'svn checkout "{url}" {costage} --depth=empty'
    CO_STRING = CO_STRING_RAW.format(url=parentUrl, costage=CFG_CHECKOUT_STAGE)
    wcpath = CFG_CHECKOUT_STAGE + PATH_SEP + filename
    print(CO_STRING)
    subprocess.Popen(CO_STRING).communicate()
    UP_STRING = 'svn update "{path}"'.format(path=wcpath)
    print(UP_STRING)
    subprocess.Popen(UP_STRING).communicate()
    return wcpath


def SetProperty(wcpath):
    """Sets the configured svn property for the file at the given path.
    The path must belong to a working copy.
    """
    CMD_STRING_RAW = '''svn propset {property} {value} "{path}"'''
    CMD_STRING = CMD_STRING_RAW.format(
        property=CFG_PROPERTY, value=CFG_PROPVAL, path=wcpath
    )
    print(CMD_STRING)
    subprocess.check_call(CMD_STRING)


def CommitRepair(wcpath):
    """Commits the repair process to the svn server with a predefined
    message.
    """
    CMD_STRING_RAW = 'svn commit "{path}" --message "{msg}"'
    CMD_STRING = CMD_STRING_RAW.format(path=wcpath, msg=CFG_REPAIR_MESSAGE)
    print(CMD_STRING)
    if not CFG_DRYRUN:
        subprocess.check_call(CMD_STRING)
    else:
        print("Skipped because of dryrun")


def BulldozeCheckoutStage():
    """Delete an existing folder at the configured checkout path. This is
    typically the folder from the last repair action. If the folder does not
    exist, the function does nothing, not even complain.
    """
    if os.path.exists(CFG_CHECKOUT_STAGE):
        CMD_STRING = "rmdir /S /Q {}".format(CFG_CHECKOUT_STAGE)
        print(CMD_STRING)
        subprocess.check_call(CMD_STRING, shell=True)


def AddLacking(lackingDict, url):
    """Memorizes the given url in the list of unprotected non-mergables
    together with some of its properties.
    """
    creation = Creation(url)
    creator = creation[0]
    month = creation[1][0:7]
    lackingDict[url] = (FileExtension(url), creator, month)


def ReportHistogram(name, tuples, pos):
    """A histogram is calculated from the data given and printed.
    Parameters
    -------------
    name : str
        The header of the histogram
    tuples : list of tuples
        A table of data, one column of which contains the values to group and
        count
    pos : int
        indicates value column of the tuples by which to group
    """
    valueList = [entry[pos] for entry in tuples]
    histogram = collections.Counter(valueList)
    PrintDict(name, histogram)


def ReportHeader(isFinal):
    """Prints the header of a either an intermediary or a final report."""
    print("")
    print("===================")
    if isFinal:
        print("FINAL REPORT")
    else:
        print("INTERMEDIARY REPORT")
    print("===================")


def ReportResults(stats, DataByPath, isFinal=True):
    """Prints a report of the scripts results (so far).
    The report contains a list of the unprotected non-mergables, statistic data
    like counts and histograms and, last but not least, execution time
    count/prediction.
    """
    ReportHeader(isFinal)
    PrintDict("Lacking Files", DataByPath)
    tuples = DataByPath.values()
    ReportHistogram("Extensions", tuples, 0)
    ReportHistogram("Creators", tuples, 1)
    ReportHistogram("Creation Months", tuples, 2)
    stats.Print()
    print("")
    CreateCsvReport(DataByPath)


def CreateCsvReport(DataByPath):
    """Writes all lacking files to a csv file.
    Parameters
    ----------
    DataByPath : list of dict
        A list of unprotected non-mergables, each list item is a dictionary
        with tuple values: url -> (extension, creator, creation date)
    """
    print("Saving data to {0}".format(CFG_CSV_OUTPUT))
    with open(CFG_CSV_OUTPUT, "w", newline="") as f:
        writer = csv.writer(f, delimiter=CFG_CSV_DELIMITER)
        writer.writerow(["extension", "creator", "creation date", "url"])
        flatlist = [value + (key,) for key, value in DataByPath.items()]
        writer.writerows(flatlist)
        f.close()


def FileExtension(url):
    """Returns the extension of file the given url points to."""
    return url.split(".")[-1]


def HasEligibleExtension(url):
    """Returns True if the url points to a file that is specified as
    to be processed by CFG_EXTENSIONS.
    """
    return FileExtension(url) in CFG_EXTENSIONS


def LacksConfiguredProperty(url):
    """Returns True if the file pointed to by url does not bear the
    configured property.
    """
    CMD_STRING = 'svn propget {property} "{url}"'.format(property=CFG_PROPERTY, url=url)
    proc = subprocess.Popen(CMD_STRING, stdout=subprocess.PIPE)
    stdout = proc.communicate()[0].decode()
    strippedOut = stdout.rstrip()
    if CFG_VERBOSE:
        MSG = "{}={} ({} characters)"
        print(MSG.format(CFG_PROPERTY, strippedOut, len(stdout)))
    return len(strippedOut) == 0


def Creation(url):
    """Returns a tuple (creator, creation date) for the given file.
    The creator is the svn user that added the file to svn, correspondlingy the
    creation date is the date the user did so.
    """
    CMD_STRING = 'svn log "{url}" -q --incremental'.format(url=url)
    proc = subprocess.Popen(CMD_STRING, stdout=subprocess.PIPE)
    stdout = proc.communicate()[0].decode()
    try:
        lastLine = stdout.splitlines()[-1]
        creator = lastLine.split("|")[1].strip()
        date = lastLine.split("|")[2].strip()
    except:
        print("Failed to parse creation from the following stdout:")
        print(stdout)
        creator = "failed parsing"
        date = "failed parsing"
    return creator, date


# all top-level code was moved into a main() function. This prevents
# the execution of this code when the module is imported by another one.
# When called using 'python CaUserConverter.py', main() is executed.
# automatically
if __name__ == "__main__":
    main()
