:: =============================================================================
:: Finds svn:externals scattered throughout the specified path
:: No working copy is needed
::
:: PARAMETERS
::  %1 - Path in WC or URL of repository folder
::
:: SVN KEYWORDS
::  $HeadURL: https://svnserver.europe.kspag.de/svn/Projects/Internal2.0/BSW/ToolSupport/Subversion/trunk/04_Software/SvnScripts/src/de/kspg/svn/script/FindExternals.bat $
::  $LastChangedDate: 2014-09-23 14:37:30 +0200 (Di, 23 Sep 2014) $
::  $LastChangedBy: CA48140 (Fries, Anton) $
::  $Revision: 47648 $
:: =============================================================================
@echo off

IF "%1"=="" GOTO E_MISSING_PARAM1

echo Looking for svn:externals in %1
svn propget svn:externals %1 -R | findstr /B /V /C:"/"
IF NOT %ERRORLEVEL%==0 GOTO E_UNKNOWN
GOTO SUCCESS

:E_MISSING_PARAM1
echo Please specify a working copy path or respository URL to search!
GOTO USAGE

:E_UNKNOWN
echo An unknown error has occured.
GOTO USAGE

:USAGE
echo Example usage 1: FindMergeInfo.bat C:\projects\myProject
echo Example usage 2: FindMergeInfo.bat https://svnserver/svn/projects/myProject

:SUCCESS