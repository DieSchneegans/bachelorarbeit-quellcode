:: =============================================================================
:: Sets property svn:needs-lock=* for each file found in PATH_TO_WORKING_COPY.
:: Files need to be committed afterwards (setting properties is a versioned 
:: action). You need to have svn.exe installed for this script to work 
:: svn.exe is typically installed with TortoiseSVN.
::
:: PARAMTERS
::  none, use PATH_TO_WORKING_COPY to specify which working (or subfolder 
::  thereof to modify)
::
:: SVN KEYWORDS
::  $HeadURL: https://svnserver/svn/internal/KSPG_Konfigurationsmanagement/trunk/04_Software/CommandLineScripts/UpdateSvnMirror.bat $
::  $LastChangedDate: 2012-09-18 08:05:18 +0200 (Di, 18 Sep 2012) $
::  $LastChangedBy: ca43420 $
::  $Revision: 627 $
:: =============================================================================


set PATH_TO_WORKING_COPY=C:\Daten\Projects\WUP3L_BMW_new

FOR /R %PATH_TO_WORKING_COPY% %%v in (*.7z *.bmp *.gif *.ico *.jpeg *.jpg *.png *.tif *.tiff *.doc *.docm *.docx *.jar *.odc *.odf *.odg *.odi *.odp *.ods *.odt *.pdf *.ppt *.ser *.swf *.vsd *.xls *.xlsb *.xlsm *.xlsx *.zip) do svn propset svn:needs-lock yes "%%~fv"

