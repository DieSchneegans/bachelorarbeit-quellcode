"""
Unit testing for class ExecTimer.

$HeadURL: https://svnserver.europe.kspag.de/svn/Projects/Internal2.0/BSW/ToolSupport/Subversion/trunk/04_Software/SvnScripts/src/de/kspg/svn/stats/ExecTimerTest.py $
$LastChangedDate: 2014-04-29 13:28:20 +0200 (Di, 29 Apr 2014) $
$LastChangedBy: CA43420 (Winzer, Wolfgang) $
$Revision: 30805 $
"""
import unittest
import time
from de.kspg.svn.stats.ExecTimer import ExecTimer


class ExecTimerTest(unittest.TestCase):

    TOLERANCE_SEC = 0.01

    def assertTimePassedEquals(self, timer, seconds):
        self.assertAlmostEqual(
            timer.GetTimePassed(), seconds, delta=ExecTimerTest.TOLERANCE_SEC
        )

    def testStartStop(self):
        timer = ExecTimer("revisions")
        timer.Start()
        time.sleep(2)
        timer.Stop(100)
        self.assertTimePassedEquals(timer, 2)
        self.assertEqual(timer.GetTimeLeft(), 0)
        self.assertEqual(timer.GetItemsTotal(), 100)
        self.assertEqual(timer.GetItemsProcessed(), 100)
        self.assertEqual(timer.GetItemsLeft(), 0)
        self.assertEqual(timer.GetProgressPct(), 100)
        self.assertEqual(timer.GetSpeed(), 50)

    def testNeutralAfterStart(self):
        timer = ExecTimer("revisions")
        timer.Start()
        timer.Report()
        self.assertEqual(timer.GetTimePassed(), 0)
        self.assertEqual(timer.GetTimeLeft(), None)
        self.assertEqual(timer.GetItemsTotal(), None)
        self.assertEqual(timer.GetItemsProcessed(), None)
        self.assertEqual(timer.GetItemsLeft(), None)
        self.assertEqual(timer.GetProgressPct(), None)
        self.assertEqual(timer.GetSpeed(), None)

    def testNeutralAfterStartWithTotal(self):
        timer = ExecTimer("revisions", 37)
        timer.Start()
        timer.Report()
        self.assertEqual(timer.GetTimePassed(), 0)
        self.assertEqual(timer.GetTimeLeft(), None)
        self.assertEqual(timer.GetItemsTotal(), 37)
        self.assertEqual(timer.GetItemsProcessed(), 0)
        self.assertEqual(timer.GetItemsLeft(), 37)
        self.assertEqual(timer.GetProgressPct(), 0)
        self.assertEqual(timer.GetSpeed(), None)

    def testCyclicReport(self):
        timer = ExecTimer("revisions")
        timer.Start()
        time.sleep(1)
        timer.Lap(20)
        self.assertTimePassedEquals(timer, 1)
        self.assertEqual(timer.GetTimeLeft(), None)
        self.assertEqual(timer.GetItemsTotal(), None)
        self.assertEqual(timer.GetItemsProcessed(), 20)
        self.assertEqual(timer.GetItemsLeft(), None)
        self.assertEqual(timer.GetProgressPct(), None)
        self.assertEqual(timer.GetSpeed(), 20)
        time.sleep(1)
        timer.Lap(30)
        self.assertTimePassedEquals(timer, 2)
        self.assertEqual(timer.GetTimeLeft(), None)
        self.assertEqual(timer.GetItemsTotal(), None)
        self.assertEqual(timer.GetItemsProcessed(), 30)
        self.assertEqual(timer.GetItemsLeft(), None)
        self.assertEqual(timer.GetProgressPct(), None)
        self.assertEqual(timer.GetSpeed(), 15)
        time.sleep(1)
        timer.Lap(39)
        self.assertTimePassedEquals(timer, 3)
        self.assertEqual(timer.GetTimeLeft(), None)
        self.assertEqual(timer.GetItemsTotal(), None)
        self.assertEqual(timer.GetItemsProcessed(), 39)
        self.assertEqual(timer.GetItemsLeft(), None)
        self.assertEqual(timer.GetProgressPct(), None)
        self.assertEqual(timer.GetSpeed(), 13)
        time.sleep(1)
        timer.Stop(40)
        self.assertTimePassedEquals(timer, 4)
        self.assertEqual(timer.GetTimeLeft(), 0)
        self.assertEqual(timer.GetItemsTotal(), 40)
        self.assertEqual(timer.GetItemsProcessed(), 40)
        self.assertEqual(timer.GetItemsLeft(), 0)
        self.assertEqual(timer.GetProgressPct(), 100)
        self.assertEqual(timer.GetSpeed(), 10)

    def testTimeOnly(self):
        timer = ExecTimer("lines")
        timer.Start()
        time.sleep(1)
        timer.Lap()
        self.assertTimePassedEquals(timer, 1)
        self.assertEqual(timer.GetTimeLeft(), None)
        self.assertEqual(timer.GetItemsTotal(), None)
        self.assertEqual(timer.GetItemsProcessed(), None)
        self.assertEqual(timer.GetItemsLeft(), None)
        self.assertEqual(timer.GetProgressPct(), None)
        self.assertEqual(timer.GetSpeed(), None)
        time.sleep(1)
        timer.Stop()
        self.assertTimePassedEquals(timer, 2)
        self.assertEqual(timer.GetTimeLeft(), None)
        self.assertEqual(timer.GetItemsTotal(), None)
        self.assertEqual(timer.GetItemsProcessed(), None)
        self.assertEqual(timer.GetItemsLeft(), None)
        self.assertEqual(timer.GetProgressPct(), None)
        self.assertEqual(timer.GetSpeed(), None)

    def testEarlyReport(self):
        """ speed output must be reasonable even on early reports """
        timer = ExecTimer("objects", 10000)
        timer.Start()
        time.sleep(0.01)
        timer.Lap(20)
        self.assertTimePassedEquals(timer, 0.01)
        self.assertAlmostEqual(timer.GetTimeLeft(), 5, delta=0.01)
        self.assertEqual(timer.GetItemsTotal(), 10000)
        self.assertEqual(timer.GetItemsProcessed(), 20)
        self.assertEqual(timer.GetItemsLeft(), 9980)
        self.assertEqual(timer.GetProgressPct(), 0.2)
        self.assertAlmostEqual(timer.GetSpeed(), 2000, delta=1.0)
        timer.Stop(10000)

    def testPrognosis(self):
        timer = ExecTimer("characters", 112)
        timer.Start()
        time.sleep(3)
        timer.Lap(43)
        time.sleep(1)
        timer.Lap(57)
        time.sleep(3)
        timer.Stop(112)

    def testProcessMoreThanTotal(self):
        timer = ExecTimer("tests", 12)
        timer.Start()
        self.assertEqual(timer.GetItemsTotal(), 12)
        self.assertEqual(timer.GetItemsLeft(), 12)
        time.sleep(1)
        timer.Lap(13)
        self.assertEqual(timer.GetItemsTotal(), 13)
        self.assertEqual(timer.GetItemsLeft(), 0)
        self.assertEqual(timer.GetTimeLeft(), 0)
        self.assertEqual(timer.GetProgressPct(), 100)
        time.sleep(1)
        timer.Stop(27)
        self.assertEqual(timer.GetItemsTotal(), 27)
        self.assertEqual(timer.GetItemsLeft(), 0)
        self.assertEqual(timer.GetTimeLeft(), 0)
        self.assertEqual(timer.GetProgressPct(), 100)


if __name__ == "__main__":
    unittest.main()
