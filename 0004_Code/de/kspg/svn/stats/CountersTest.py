"""
$HeadURL: https://svnserver.europe.kspag.de/svn/Projects/Internal2.0/BSW/ToolSupport/Subversion/trunk/04_Software/SvnScripts/src/de/kspg/svn/stats/CountersTest.py $ # @IgnorePep8
$LastChangedDate: 2014-04-29 13:28:20 +0200 (Di, 29 Apr 2014) $
$LastChangedBy: CA43420 (Winzer, Wolfgang) $
$Revision: 30805 $
"""
import unittest
from de.kspg.svn.stats.Counters import Counters
from collections import OrderedDict


class StatsTest(unittest.TestCase):
    """Unit testing for class Counters:"""

    def setUp(self):
        self.stats = Counters()

    def tearDown(self):
        pass

    def test01PrintEmptyStats(self):
        self.stats.Print()
        self.assertEqual(self.stats.Get("Bull"), 0)
        self.assertEqual(self.stats.Get("Horse"), 0)

    def test02PrintSingleStat(self):
        self.assertEqual(self.stats.Get("Cars"), 0)
        self.stats.Inc("Cars")
        self.assertEqual(self.stats.Get("Cars"), 1)
        self.stats.Inc("Cars")
        self.assertEqual(self.stats.Get("Cars"), 2)
        self.stats.Print()

    def test03IncLargeSteps(self):
        self.assertEqual(self.stats.Get("Dogs"), 0)
        self.stats.Inc("Dogs", 7)
        self.assertEqual(self.stats.Get("Dogs"), 7)
        self.stats.Inc("Dogs", 3)
        self.assertEqual(self.stats.Get("Dogs"), 10)
        self.stats.Print()

    def test04PrintMultiStats(self):
        self.assertEqual(self.stats.Get("Cats"), 0)
        self.assertEqual(self.stats.Get("Canaries"), 0)
        self.stats.Inc("Canaries")
        self.assertEqual(self.stats.Get("Cats"), 0)
        self.assertEqual(self.stats.Get("Canaries"), 1)
        self.stats.Inc("Cats")
        self.assertEqual(self.stats.Get("Cats"), 1)
        self.assertEqual(self.stats.Get("Canaries"), 1)
        self.stats.Inc("Canaries")
        self.assertEqual(self.stats.Get("Cats"), 1)
        self.assertEqual(self.stats.Get("Canaries"), 2)
        self.stats.Inc("Cats", 665)
        self.assertEqual(self.stats.Get("Cats"), 666)
        self.assertEqual(self.stats.Get("Canaries"), 2)
        self.stats.Print()

    def test05KeyLengthOfEmptyStats(self):
        self.assertEqual(self.stats._MaxKeyLength(), 0)

    def test06KeyLengthOfSingleStats(self):
        self.stats.Inc("Dogs")
        self.assertEqual(self.stats._MaxKeyLength(), 4)

    def test07KeyLengthOfMultiStats(self):
        self.stats.Inc("Dogs")
        self.stats.Inc("Canaries")
        self.stats.Inc("Donkeys")
        self.assertEqual(self.stats._MaxKeyLength(), 8)

    def test08RetainOrder(self):
        orderedStats = Counters(["F", "D", "Z", "A"])
        refDict = OrderedDict([("F", 0), ("D", 0), ("Z", 0), ("A", 0)])
        self.assertEqual(orderedStats._counters, refDict)

    def test09SetupIsZero(self):
        catStats = Counters(["Cats", "Dogs"])
        self.assertEqual(catStats.Get("Cats"), 0)
        self.assertEqual(catStats.Get("Dogs"), 0)

    def test11JumpStartCounter(self):
        self.stats.Inc("Cats", 11)
        self.assertEqual(self.stats.Get("Cats"), 11)


if __name__ == "__main__":
    # import sys;sys.argv = ['', 'StatsTest.testPrintEmptyStats']
    unittest.main()
