"""
$HeadURL: https://svnserver.europe.kspag.de/svn/Projects/Internal2.0/BSW/ToolSupport/Subversion/trunk/04_Software/SvnScripts/src/de/kspg/svn/stats/Counters.py $ # @IgnorePep8
$LastChangedDate: 2015-03-20 08:37:40 +0100 (Fr, 20 Mrz 2015) $
$LastChangedBy: CA43420 (Winzer, Wolfgang) $
$Revision: 55391 $
"""
from collections import OrderedDict


class Counters(object):
    """
    Manages a bunch of counters. Collecting counters in an object allows us to
    pass them to subfunctions and output the statistics in a formatted way at
    the end of a script.
    The class is pretty generic, it doesn't matter what you want to count or
    how many counters you need. You can either plan the counters during
    construction, allowing you to
    a) output even counters that were never incremented and
    b) define the order in which the counters are output later
    or you can automatically add counters in an ad-hoc way by incrementing them
    for the first time.

    Currently, Counters is not very strict, you can setup a list of counters and
    then (accidently?) add some later. Choosing between setup and ad-hoc mode
    to allow for more safety might be a future feature worth thinking about.
    """

    def __init__(self, counterList=[]):
        """
        Constructor
        """
        self._counters = OrderedDict()
        for counter in counterList:
            self._counters[counter] = 0

    def Inc(self, counter, step=1):
        if counter in self._counters:
            self._counters[counter] = self._counters[counter] + step
        else:
            self._counters[counter] = step

    def Get(self, counter):
        if counter in self._counters.keys():
            return self._counters[counter]
        else:
            return 0

    def Percentage(self, counter):
        total = self.Total()
        if total > 0:
            perc = self._counters[counter] / total
        else:
            perc = 0
        return perc

    def Total(self):
        total = 0
        for key in self._counters.keys():
            total = total + self._counters[key]
        return total

    def Print(self, header=False, showPercentage=True, showTotal=False):
        """ Outputs the value of all configured counters in a nice table.
            The table can include a user defined header and it may
            have an extra column with the percentages of each counter value.
            It may include a total as the last line.
        """
        maxKeyLen = str(self._MaxKeyLength())
        maxValLen = str(self._MaxValueLength())
        outputFormat = "{0:" + maxKeyLen + "}: {1:" + maxValLen + "}"
        sepLine = "-" * (self._MaxKeyLength() + self._MaxValueLength() + 13)

        if header:
            print(header)
            print(sepLine)

        if showPercentage:
            outputFormat = outputFormat + " ({2:>8.3%})"

        for key in self._counters.keys():
            value = self._counters[key]
            perc = self.Percentage(key)
            print(outputFormat.format(key, value, perc))

        if showTotal:
            print(sepLine)
            print(outputFormat.format("total", self.Total(), 1))

    def _MaxKeyLength(self):
        if self._counters.keys():
            return max([len(key) for key in self._counters.keys()])
        else:
            return 0

    def _MaxValueLength(self):
        if self._counters.values():
            return max([len(str(value)) for value in self._counters.values()])
        else:
            return 0

    # TODO implement __iterate__ and stuff to make available the underlying
    # dictionary
