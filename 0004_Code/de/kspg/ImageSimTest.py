"""
Created on 04.12.2014

@author: ca43420
"""
import unittest
from de.kspg.ImageSim import *


class Test(unittest.TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testConvertWlTypesOldSw(self):
        wlTypes = [
            WlType.EMPTY,
            WlType.PSEUDO_EMPTY,
            WlType.PSEUDO_EMPTY_ICS,
            WlType.INVALID,
        ]
        convertedWlTypes = convertWlTypes(wlTypes, WLTYPE_MAP[0])
        self.assertEqual(convertedWlTypes[0], WlType.EMPTY)
        self.assertEqual(convertedWlTypes[1], WlType.EMPTY)
        self.assertEqual(convertedWlTypes[2], WlType.EMPTY)
        self.assertEqual(convertedWlTypes[3], WlType.INVALID)

    def testConvertWlTypesNewSw(self):
        wlTypes = [
            WlType.EMPTY,
            WlType.PSEUDO_EMPTY,
            WlType.PSEUDO_EMPTY_ICS,
            WlType.INVALID,
        ]
        convertedWlTypes = convertWlTypes(wlTypes, WLTYPE_MAP[1])
        self.assertEqual(convertedWlTypes[0], WlType.EMPTY)
        self.assertEqual(convertedWlTypes[1], WlType.INVALID)
        self.assertEqual(convertedWlTypes[2], WlType.EMPTY)
        self.assertEqual(convertedWlTypes[3], WlType.INVALID)

    def testBehaveType11(self):
        wlTypes = [WlType.INVALID, WlType.PSEUDO_EMPTY, WlType.INVALID, WlType.INVALID]
        self.assertEqual(
            determineBehaviour(wlTypes, Software.OLD), Behaviour.DESTRUCTION
        )
        self.assertEqual(
            determineBehaviour(wlTypes, Software.NEW), Behaviour.MARCHTHROUGH
        )

    def testBehaveType12(self):
        wlTypes = [WlType.INVALID, WlType.EMPTY, WlType.INVALID, WlType.INVALID]
        self.assertEqual(
            determineBehaviour(wlTypes, Software.OLD), Behaviour.DESTRUCTION
        )
        self.assertEqual(determineBehaviour(wlTypes, Software.NEW), Behaviour.TOPSEARCH)

    def testBehaveType21(self):
        wlTypes = [
            WlType.PSEUDO_VALID,
            WlType.PSEUDO_EMPTY,
            WlType.INVALID,
            WlType.INVALID,
        ]
        self.assertEqual(
            determineBehaviour(wlTypes, Software.OLD), Behaviour.CORRUPTION
        )
        self.assertEqual(
            determineBehaviour(wlTypes, Software.NEW), Behaviour.MARCHTHROUGH
        )

    def testBehaveType21ICS(self):
        wlTypes = [
            WlType.PSEUDO_VALID,
            WlType.PSEUDO_EMPTY_ICS,
            WlType.INVALID,
            WlType.INVALID,
        ]
        self.assertEqual(
            determineBehaviour(wlTypes, Software.OLD), Behaviour.CORRUPTION
        )
        self.assertEqual(
            determineBehaviour(wlTypes, Software.NEW), Behaviour.CORRUPTION
        )

    def testBehaveType22(self):
        wlTypes = [WlType.PSEUDO_VALID, WlType.EMPTY, WlType.INVALID, WlType.INVALID]
        self.assertEqual(
            determineBehaviour(wlTypes, Software.OLD), Behaviour.CORRUPTION
        )
        self.assertEqual(
            determineBehaviour(wlTypes, Software.NEW), Behaviour.CORRUPTION
        )

    def testBehaveOldHat(self):
        wlTypes = [WlType.INVALID, WlType.VALID, WlType.INVALID, WlType.EMPTY]
        self.assertEqual(determineBehaviour(wlTypes, Software.OLD), Behaviour.USEOLDHAT)
        self.assertEqual(determineBehaviour(wlTypes, Software.NEW), Behaviour.USEOLDHAT)


if __name__ == "__main__":
    # import sys;sys.argv = ['', 'Test.testConvertWlTypes']
    unittest.main()
