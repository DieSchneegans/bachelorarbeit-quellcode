"""
    Module Couple
"""

###################################################################################################
###                                imports                                                      ###
###################################################################################################
import sys

try:
    # Package create logging messages
    import logging
except ModuleNotFoundError:
    print("logging not installed. Exiting...")
    sys.exit(4)

try:
    # Superclass TokenListComparison
    from CalculationModule.TokenListComparison import TokenListComparison
except ModuleNotFoundError:
    logging.error("CalculationModule.TokenList not installed. Exiting...")
    sys.exit(4)

try:
    # Class SourceFile
    from FileModule.SourceFile import SourceFile
except ModuleNotFoundError:
    logging.error("FileModule.SourceFile not installed. Exiting...")
    sys.exit(4)

###################################################################################################
###                                global variables                                             ###
###################################################################################################

###################################################################################################
###                                public classes                                               ###
###################################################################################################


class Couple(TokenListComparison):
    """
        Manage a couple of files
    """

    def __init__(self, visual:bool, fileOld: SourceFile, fileNew: SourceFile, startThreshold: int, minThreshold: int, thresholdDivisor: int) -> None:
        """
            Constructor
        """
        self.__files = [fileOld, fileNew]

        super().__init__(visual=visual, startThreshold=startThreshold,
                         minThreshold=minThreshold, thresholdDivisor=thresholdDivisor)
        
    def getFileName(self) -> str:
        """
            Return the file name
        """
        return self.__files[0].getFileName()

    def __tokenize(self) -> None:
        """
            Tokenize both files
        """
        if self._visual:
            print("Tokenizing both files")
        
        self._tokens[0] = self.__files[0].tokenize()
        self._tokens[1] = self.__files[1].tokenize()

    def compare(self) -> list:
        """
            Overwrite the super class method
        """
        # Tokenize files
        if not self._tokens[0] or not self._tokens[1]:
            self.__tokenize()

        # Hashing tokens
        self._hash(side=0)
        self._hash(side=1)

        # Remove matches
        self._createMatrix()
        stable, removedMatches = self._matrix.removeMatches(startThreshold=self._startThreshold, minThreshold=self._minThreshold, thresholdDivisor=self._thresholdDivisor)

        return stable, removedMatches

###################################################################################################
###                                private classes                                              ###
###################################################################################################

###################################################################################################
###                                public functions                                             ###
###################################################################################################

###################################################################################################
###                                private functions                                            ###
###################################################################################################

###################################################################################################
###                                main                                                         ###
###################################################################################################
