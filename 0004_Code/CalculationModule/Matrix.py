"""
    Module Matrix
"""

###################################################################################################
###                                imports                                                      ###
###################################################################################################
import sys

try:
    # Package create logging messages
    import logging
except ModuleNotFoundError:
    print("logging not installed. Exiting...")
    sys.exit(4)

try:
    # Function to round up a float
    from math import ceil
except ModuleNotFoundError:
    logging.error("math not installed. Exiting...")
    sys.exit(4)

###################################################################################################
###                                global variables                                             ###
###################################################################################################

###################################################################################################
###                                public classes                                               ###
###################################################################################################


class Matrix():
    """
        Calculate matches into a matrix between two software version lists
    """

    def __init__(self, visual:bool, listOld: list, listNew: list) -> None:
        """
            Constructor
        """
        self.__visual :bool = visual
        
        self.__listOld :list= listOld
        self.__listNew :list= listNew
        
        self.__stable :int= 0
        self.__removedMatches:int = 0
        pass

    def __calcThreshold(self, currThreshold: int, minThreshold: int, thresholdDivisor: int, maxMatchLength: int = -1) -> int:
        """
            Calculate threshold value
        """
        if currThreshold == 1 or currThreshold <= minThreshold or (0 < maxMatchLength and maxMatchLength < minThreshold):
            return 0
        elif maxMatchLength == -1:
            return max(minThreshold, ceil(min(currThreshold, len(self.__listOld), len(self.__listNew))/thresholdDivisor))
        else:
            return max(minThreshold, ceil(min(currThreshold, maxMatchLength, len(self.__listOld), len(self.__listNew))/thresholdDivisor))

    def __calcNextOffset(self, lastOffset: int, lengthOld: int, lengthNew: int, threshold: int) -> int:
        """
            calculates the next offset (defined as new - old) based on the last one, with preference of 
            a) longest diagonal, i.e. abs(offset) is minimal
            b) positive offset
            returns None if all remaining diagonals are shorter than threshold
        """

        # note that diagonals which are shorter than threshold don't need to be searched
        maxPosOffset = lengthNew - threshold
        minNegOffset = threshold - lengthOld

        if lastOffset == 0:
            logging.debug("maxPosOffset={:10}".format(maxPosOffset))
            logging.debug("minNegOffset={:10}".format(minNegOffset))

        if maxPosOffset < 0 or minNegOffset > 0:
            return None

        # note: None or 42 equals 42, -5 or 7 equals -5, None or None equals None
        if lastOffset > 0:
            return self.__calcNextNegOffset(lastOffset, minNegOffset) or self.__calcNextPosOffset(lastOffset, maxPosOffset)
        else:
            return self.__calcNextPosOffset(lastOffset, maxPosOffset) or self.__calcNextNegOffset(lastOffset, minNegOffset)

    def __calcNextPosOffset(self, lastOffset: int, maxPositive: int) -> int:
        """
            calculates the next positive offset based on the sign of lastOffset
            returns None if result diagonal above maxPositive
        """

        if lastOffset > 0:
            nextOffset = lastOffset + 1
        else:
            nextOffset = -lastOffset + 1

        if nextOffset <= maxPositive:
            return nextOffset
        else:
            return None

    def __calcNextNegOffset(self, lastOffset: int, minNegative: int) -> int:
        """
            calculates the next negative offset based on the sign of lastOffset
            returns None if result diagonal below minNegative
        """

        if lastOffset > 0:
            nextOffset = -lastOffset
        else:
            nextOffset = lastOffset - 1

        if nextOffset >= minNegative:
            return nextOffset
        else:
            return None

    def removeMatches(self, startThreshold: int, minThreshold: int, thresholdDivisor: int) -> list:
        """
            Find matches in lists and remove if the length of match is bigger than the current threshold
        """
        if self.__visual:
            print("Starting to search and remove matches")
        
        # Calculate start threshold value
        currThreshold = self.__calcThreshold(
            startThreshold, minThreshold, thresholdDivisor)

        # loop through decreasing thresholds
        while currThreshold >= minThreshold and len(self.__listOld) >= minThreshold and len(self.__listNew) >= minThreshold:

            # Length of tokens
            lengthOld = len(self.__listOld)
            lengthNew = len(self.__listNew)

            logging.info("Threshold: {}, length old version: {}, length new version: {}".format(
                currThreshold, lengthOld, lengthNew))

            # loop through diagonals
            matchLength = 0
            maxMatchLength = -1
            # start with main diagonal because it quickly detects exact file matches
            offset = 0
            while offset is not None:
                # ENABLING THIS LINE WILL SLOW THE SCRIPT DOWN DRAMATICALLY
                # logging.debug("Starting new diagonal with offset {}".format(offset))

                indexOld = max(0, -offset)
                indexNew = max(0, offset)

                while indexOld < lengthOld and indexNew < lengthNew:
                    # ENABLING THIS LINE WILL SLOW THE SCRIPT DOWN DRAMATICALLY
                    # logging.debug("({},{})".format(indexOld, indexNew))

                    valueOld = self.__listOld[indexOld]
                    valueNew = self.__listNew[indexNew]

                    if valueOld is not None and valueOld == valueNew:
                        matchLength += 1
                    else:
                        indexOld, indexNew, lengthOld, lengthNew, matchLength, maxMatchLength = self.__remove(mode=0, indizes=[indexOld, indexNew], lengths=[lengthOld, lengthNew], matchLength=matchLength, maxMatchLength=maxMatchLength, currOffset=offset, currThreshold=currThreshold)                      

                    indexOld += 1
                    indexNew += 1

                # end of diagonal reached, so check if we have a match that can be removed
                indexOld, indexNew, lengthOld, lengthNew, matchLength, maxMatchLength = self.__remove(mode=1, indizes=[indexOld, indexNew], lengths=[lengthOld, lengthNew], matchLength=matchLength, maxMatchLength=maxMatchLength, currOffset=offset, currThreshold=currThreshold)

                # Calculate next offset
                offset = self.__calcNextOffset(
                    offset, lengthOld, lengthNew, currThreshold)

            # Calculate next threshold
            currThreshold = self.__calcThreshold(
                currThreshold, minThreshold, thresholdDivisor, maxMatchLength)

        return self.__stable, self.__removedMatches

    def __remove(self, mode:int, indizes:list, lengths:list, matchLength:int, maxMatchLength, currOffset:int, currThreshold:int):
        """
            Remove a match from the lists
            Choose:
            mode = 0 - Replace with NoneType
            mode = 1 - No replace with NoneType
        """
        if matchLength > 0:
            # reached end of match
            if matchLength >= currThreshold:
                if self.__visual:
                    print("deleted match ({},{}) of length {} at end of diagonal {}".format(indizes[0] - matchLength, indizes[1] - matchLength, matchLength, currOffset))
        
                if mode == 0:
                    # Remove match from old version
                    self.__listOld[indizes[0] - matchLength : indizes[0]] = [None]
        
                    # Remove match from new version
                    self.__listNew[indizes[1] - matchLength : indizes[1]] = [None]
                    
                    # Get new lengths of lists
                    lengths[0] -= (matchLength - 1)
                    lengths[1] -= (matchLength - 1) 
        
                else:
                    # Remove match from old version
                    del self.__listOld[indizes[0] - matchLength: indizes[0]]
        
                    # Remove match from new version
                    del self.__listNew[indizes[1] - matchLength: indizes[1]]     
                    
                    # Get new lengths of lists
                    lengths[0] -= matchLength
                    lengths[1] -= matchLength       
        
                # Add stable and step
                self.__stable += matchLength
                self.__removedMatches += 1
        
                # Correction of next element
                indizes[0] -= matchLength
                indizes[1] -= matchLength
        
            # update maximal match length, removed or not
            if maxMatchLength < matchLength:
                maxMatchLength = matchLength

            matchLength = 0
        
        return indizes[0], indizes[1], lengths[0], lengths[1], matchLength, maxMatchLength

###################################################################################################
###                                private classes                                              ###
###################################################################################################

###################################################################################################
###                                public functions                                             ###
###################################################################################################

###################################################################################################
###                                private functions                                            ###
###################################################################################################

###################################################################################################
###                                main                                                         ###
###################################################################################################
