"""
    Module StewPot
"""

###################################################################################################
###                                imports                                                      ###
###################################################################################################
import sys

try:
    # Package create logging messages
    import logging
except ModuleNotFoundError:
    print("logging not installed. Exiting...")
    sys.exit(4)

try:
    # Superclass
    from CalculationModule.TokenListComparison import TokenListComparison
except ModuleNotFoundError:
    logging.error(
        "CalculationModule.TokenListComparison not installed. Exiting...")
    sys.exit(4)

###################################################################################################
###                                global variables                                             ###
###################################################################################################

###################################################################################################
###                                public classes                                               ###
###################################################################################################


class StewPot(TokenListComparison):
    """
        Manage a stew pot of tokens
    """

    def __init__(self, visual:bool, startThreshold: int, minThreshold: int, thresholdDivisor: int) -> None:
        """
            Constructor
        """
        super().__init__(visual=visual, startThreshold=startThreshold,
                         minThreshold=minThreshold, thresholdDivisor=thresholdDivisor)

    def compare(self) -> list:
        """
            Overwrite the super class method
        """
        # Remove matches
        self._createMatrix()
        stable, removedMatches = self._matrix.removeMatches(startThreshold=self._startThreshold, minThreshold=1, thresholdDivisor=self._thresholdDivisor)

        return stable, removedMatches, self.__calcAdd(), self.__calcDelete()

    def __calcAdd(self) -> int:
        """
            Calculate the added part
        """
        add = 0
        for currToken in self._tokens[1]:
            if currToken is not None:
                add += 1

        return add

    def __calcDelete(self) -> int:
        """
            Calculate the deleted part
        """
        delete = 0
        for currToken in self._tokens[0]:
            if currToken is not None:
                delete += 1

        return delete

    def extend(self, side: int, tokenList: list, hashing:bool=False) -> None:
        """
            Extend a list of tokens to the stew pot
            Choose:
            side = 0 - old software version
            side = 1 - new software version
        """
        if list:
            # Hash list if hash mode activate
            if hashing:
                tokenList = self._hash(2, tokenList)
            
            self._tokens[side].extend(tokenList)
            self._tokens[side].append(None)


###################################################################################################
###                                private classes                                              ###
###################################################################################################

###################################################################################################
###                                public functions                                             ###
###################################################################################################

###################################################################################################
###                                private functions                                            ###
###################################################################################################

###################################################################################################
###                                main                                                         ###
###################################################################################################
