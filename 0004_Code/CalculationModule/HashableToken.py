"""
    Module TokenWithHashing
"""

###################################################################################################
###                                imports                                                      ###
###################################################################################################
import sys

try:
    # Package create logging messages
    import logging
except ModuleNotFoundError:
    print("logging not installed. Exiting...")
    sys.exit(4)

try:
    # Class to calculate the stability index
    from sctokenizer.token import Token, TokenType
except ModuleNotFoundError:
    logging.error("sctokenizer.token not installed. Exiting...")
    sys.exit(4)

###################################################################################################
###                                global variables                                             ###
###################################################################################################

###################################################################################################
###                                public classes                                               ###
###################################################################################################


class HashableToken(Token):
    """
        Token with comparing method and hashing method
    """

    def __init__(self, token_value:str, token_type:TokenType, line:int, column:int) -> None:
        """
            Constructor
        """
        super().__init__(token_value, token_type, line, column)

    def __eq__(self, other:object) -> bool:
        return self.token_type == other.token_type and self.token_value == other.token_value

    def __hash__(self) -> int:
        return hash((self.token_type, self.token_value))

###################################################################################################
###                                private classes                                              ###
###################################################################################################

###################################################################################################
###                                public functions                                             ###
###################################################################################################

###################################################################################################
###                                private functions                                            ###
###################################################################################################

###################################################################################################
###                                main                                                         ###
###################################################################################################
