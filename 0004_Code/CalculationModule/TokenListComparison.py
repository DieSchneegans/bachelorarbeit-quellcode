"""
    Module TokenListComparison
"""

###################################################################################################
###                                imports                                                      ###
###################################################################################################
import sys

try:
    # Package create logging messages
    import logging
except ModuleNotFoundError:
    print("logging not installed. Exiting...")
    sys.exit(4)

try:
    # Class Matrix
    from CalculationModule.Matrix import Matrix
except ModuleNotFoundError:
    logging.error("CalculationModule.Matrix not installed. Exiting...")
    sys.exit(4)

###################################################################################################
###                                global variables                                             ###
###################################################################################################

###################################################################################################
###                                public classes                                               ###
###################################################################################################


class TokenListComparison:
    """
        Manage a lists of tokens (two softwareversions)
    """

    def __init__(self, visual: bool, startThreshold: int, minThreshold: int, thresholdDivisor: int) -> None:
        """
            Constructor
        """
        self._visual = visual
        
        self._tokens = [[],[]]

        self._matrix = None
        self._startThreshold = startThreshold
        self._minThreshold = minThreshold
        self._thresholdDivisor = thresholdDivisor

    def _hash(self, side:int, tokenList:list = None) -> list:
        """
            Hashing token list
        """
        
        
        indicesTokens = []
        if not tokenList:
            if self._visual:
                print("Hashing token list {}".format(side))
            # Hash exist token list
            for token in self._tokens[side]:
                indicesTokens.append(token.__hash__())
            self._tokens[side] = indicesTokens
        else:
            # Hash external list
            for token in tokenList:
                indicesTokens.append(token.__hash__())
        
        return indicesTokens

    def compare(self) -> None:
        """
            Compare both tokens lists and calculate S, A, D, M
        """
        pass

    def _createMatrix(self) -> None:
        """
            Create a matrix obejct
        """
        self._matrix = Matrix(visual=self._visual, listOld=self._tokens[0], listNew=self._tokens[1])

    def getList(self, side: int = 0) -> list:
        """
            Return a token list side
        """
        return self._tokens[side]


###################################################################################################
###                                private classes                                              ###
###################################################################################################

###################################################################################################
###                                public functions                                             ###
###################################################################################################

###################################################################################################
###                                private functions                                            ###
###################################################################################################

###################################################################################################
###                                main                                                         ###
###################################################################################################
