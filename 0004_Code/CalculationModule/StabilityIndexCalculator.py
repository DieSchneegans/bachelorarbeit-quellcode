"""
    Module StabilityIndex
"""

###################################################################################################
###                                imports                                                      ###
###################################################################################################
import sys

try:
    # Package create logging messages
    import logging
except ModuleNotFoundError:
    print("logging not installed. Exiting...")
    sys.exit(3)
    
try:
    # Path class
    from pathlib import Path
except ModuleNotFoundError:
    logging.error("pathlib not installed. Exiting...")
    sys.exit(3)

try:
    # Class to handle a root directory
    from FileModule.SourceBase import SourceBase
except ModuleNotFoundError:
    logging.error("FileModule.SourceBase not installed. Exiting...")
    sys.exit(3)

try:
    # Class to handle file couples
    from CalculationModule.Couple import Couple
except ModuleNotFoundError:
    logging.error("CalculationModule.Couple not installed. Exiting...")
    sys.exit(3)

try:
    # Class to handle file couples
    from CalculationModule.StewPot import StewPot
except ModuleNotFoundError:
    logging.error("CalculationModule.StewPot not installed. Exiting...")
    sys.exit(3)

try:
    # Class to get a timer for execution measurement
    from de.kspg.svn.stats.ExecTimer import ExecTimer
except ModuleNotFoundError:
    logging.error("de.kspg.svn.stats.ExecTimer not installed. Exiting...")
    sys.exit(3)

###################################################################################################
###                                global variables                                             ###
###################################################################################################

###################################################################################################
###                                public classes                                               ###
###################################################################################################


class StabilityIndexCalculator:
    """
        Calculate stability index
    """

    def __init__(self, visual:bool, startThreshold:int=256, thresholdDivisor:int=4, minThreshold:int=16) -> None:
        """
            Constructor
        """

        # Modi
        self.__visual : bool = visual

        # Values to calculate
        self.__add :int = 0
        self.__delete : int = 0
        self.__stable :int = 0
        self.__removedMatches:int = 0
        self.__stabilityIndex:int = -1
        self.__contentStability :int= -1
        self.__structureStability :int= -1

        # Threshold calculation
        self.__thresholdDivisor :int= thresholdDivisor
        self.__threshold :int= startThreshold * thresholdDivisor
        self.__thresholdMinimum:int = minThreshold

        # lists
        self.__filesOld :list= []
        self.__filesNew :list= []
        self.__couples :list= []
        self.__stewPot :StewPot = StewPot(visual=visual, startThreshold=startThreshold*thresholdDivisor,
                                 minThreshold=minThreshold, thresholdDivisor=thresholdDivisor)

    def __printPhaseHeader(self, phaseNo:int, phaseName:str) -> None:
        """
            Print a header for a calculation phase
        """
        if self.__visual:
            print()
            print(80*"=")
            print("     PHASE {} - {}".format(phaseNo, phaseName))
            print(80*"=")

    def calcStabilityIndex(self, pathOld:Path=None, pathNew:Path=None) -> None:
        """
            Calculate the stability index
        """
        
        

        ### ----- Start timer ----- ###
        if self.__visual:            
            phaseTimer = 0
            timer = ExecTimer(itemname="calculation phases")
            timer.Start()

        ### ----- Calculation phase 1 ----- ###
        self.__printPhaseHeader(1, "Find files")
        sourceBaseOld = SourceBase(self.__visual, pathOld)
        self.__filesOld = sourceBaseOld.searchFiles()

        if not self.__filesOld:
            logging.error(
                "No files found. Please check your path of the old software version!")
            sys.exit(2)
        else:
            logging.info("Number of old software version files: {}".format(
                len(self.__filesOld)))

        sourceBaseNew = SourceBase(self.__visual, pathNew)
        self.__filesNew = sourceBaseNew.searchFiles()

        if not self.__filesNew:
            logging.error(
                "No files found. Please check your path of the new software version!")
            sys.exit(2)
        else:
            logging.info("Number of new software version files: {}".format(
                len(self.__filesNew)))

        ### ----- Lap timer ----- ###
        if self.__visual:
            phaseTimer += 1
            timer.Lap(phaseTimer)

        ### ----- Calculation phase 2 ----- ###
        self.__printPhaseHeader(
            2, "Find file couples and add rest files into a list")
        self.__findCouples(pathOld, pathNew)
        self.__filesToRest(0, self.__filesOld)
        self.__filesToRest(1, self.__filesNew)

        couplesLength = len(self.__couples)
        logging.info("Number of file couples: {}".format(couplesLength))
        logging.info("Tokens in rest list old/new: {}/{} new".format(
            len(self.__stewPot.getList(0)), len(self.__stewPot.getList(1))))

        ### ----- Lap timer ----- ###
        if self.__visual:
            phaseTimer += 1
            timer.Lap(phaseTimer)

        ### ----- Calculation phase 3 ----- ###
        self.__printPhaseHeader(
            3, "Compare file couples with offset-jump-threshold-method")
        
        if self.__visual:
            couplesTimer = ExecTimer(
                itemname="file couples", totalItems=couplesLength)
            couplesTimer.Start()
        
        counter = 0
        for couple in self.__couples:
            counter += 1
            
            if self.__visual:
                print("-"*80)
                print("Processing file couple {}/{}: {}".format(counter,
                                                                couplesLength, couple.getFileName()))

            # Compare couple
            stable, removedMatches = couple.compare()
            self.__stable += stable
            self.__removedMatches += removedMatches

            # Add rest to stewpot
            self.__stewPot.extend(side=0, tokenList=couple.getList(side=0))
            self.__stewPot.extend(side=1, tokenList=couple.getList(side=1))
            
            if self.__visual:
                print("Added {}/{} tokens to old/new rest list which have now length {}/{}".format(len(couple.getList(side=0)), len(couple.getList(side=1)), len(self.__stewPot.getList(side=0)), len(self.__stewPot.getList(side=1))))
                couplesTimer.Lap(counter)

        if self.__visual:
            couplesTimer.Stop() 

        ### ----- Lap timer ----- ###
        if self.__visual:
            phaseTimer += 1
            timer.Lap(phaseTimer)

        ### ----- Calculation phase 4 ----- ###
        self.__printPhaseHeader(4, "Compare rest list")
        stable, removedMatches, added, deleted = self.__stewPot.compare()
        self.__stable += stable
        self.__removedMatches += removedMatches
        self.__add = added
        self.__delete = deleted

        ### ----- Lap timer ----- ###
        if self.__visual:
            phaseTimer += 1
            timer.Lap(phaseTimer)

        ### ----- Calculation phase 5 ----- ###
        self.__printPhaseHeader(5, "Calculate stability")
        self.__calcContentStability()
        self.__calcStructureStability()
        self.__calcOverallStability()

        ### ----- Stop timer ----- ###
        if self.__visual:
            phaseTimer += 1
            timer.Stop(phaseTimer)
            print("--------------------")

    def __calcContentStability(self) -> None:
        """
            Calculate the content stability
        """
        logging.debug("StabilityIndex - _calcContentStability")

        if self.__contentStability == -1:
            self.__contentStability = self.__stable / \
                (self.__stable+self.__add+self.__delete)

    def __calcStructureStability(self) -> None:
        """
            Override method from super class
        """
        if self.__structureStability == -1:
            self.__structureStability = 1 - ((self.__removedMatches-len(self.__couples))/max(
                1, min(self.__stable + self.__delete, self.__stable + self.__add)))

    def __calcOverallStability(self) -> None:
        """
            Calculate the overall stability index
        """
        logging.debug("StabilityIndex - _calcOverallStability")

        if self.__stabilityIndex == -1:
            if self.__contentStability == -1:
                self._calcContentStability()

            if self.__structureStability == -1:
                self._calcStructureStability()

            self.__stabilityIndex = self.__contentStability * self.__structureStability

    def __findCouples(self, rootPathOld:Path, rootPathNew:Path) -> None:
        """
            Find file couples in all files
            Condition: same relative file path
        """

        ### ----- Find Couples ----- ###
        for fileOld in self.__filesOld:
            # Get relative file path
            pathOld = fileOld.getFilePath()
            relativePathOld = pathOld.relative_to(
                rootPathOld)

            for fileNew in self.__filesNew:
                if fileNew is not None:
                    pathNew = fileNew.getFilePath()
                    relativePathNew = pathNew.relative_to(
                        rootPathNew)

                    # Compare relative paths
                    if relativePathOld == relativePathNew:
                        # Add files to list of couples
                        self.__couples.append(Couple(visual=self.__visual,fileOld=fileOld, fileNew=fileNew, startThreshold=self.__threshold, minThreshold=self.__thresholdMinimum, thresholdDivisor=self.__thresholdDivisor))

                        # Remove file from old version
                        indexOld = self.__filesOld.index(
                            fileOld)
                        self.__filesOld[indexOld] = None

                        # Remove file from new version
                        indexNew = self.__filesNew.index(
                            fileNew)
                        self.__filesNew[indexNew] = None

    def __filesToRest(self, side:int, files:list) -> None:
        """
            Remove all files from file list, tokenize the files and add these tokens to rest list
        """
        fileCounter = 0
        for file in files:
            if file:
                fileCounter += 1
                
                if self.__visual:
                    print("adding file {} to the stew pot".format(file.getFileName()))

                # Tokenize file
                tokens = file.tokenize()

                # Add to stew pot
                self.__stewPot.extend(side=side, tokenList=tokens, hashing=True)

        if self.__visual:
            print("files added to stew pot: {}".format(fileCounter))

    def printStabilityIndex(self) -> None:
        """
            Create a console output of the result via logging
        """
        logging.info("S: {} tokens, A: {} tokens, D: {} tokens, M: {} steps, F: {} couple of files -> StabilityIndex: {} percent (Content Stability: {} percent, Structure Stability: {} percent)".format(
            self.__stable, self.__add, self.__delete, self.__removedMatches, len(self.__couples), int(round(
                self.__stabilityIndex * 100, 0)), int(round(self.__contentStability * 100, 0)), int(round(self.__structureStability * 100, 0))
        ))


###################################################################################################
###                                private classes                                              ###
###################################################################################################

###################################################################################################
###                                public functions                                             ###
###################################################################################################

###################################################################################################
###                                private functions                                            ###
###################################################################################################

###################################################################################################
###                                main                                                         ###
###################################################################################################
