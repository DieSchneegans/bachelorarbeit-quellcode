###################################################################################################
###                                imports                                                      ###
###################################################################################################
import sys
import os
import stat
import errno

try:
    # Package create logging messages
    import logging
except ModuleNotFoundError:
    print("logging not installed. Exiting...")
    sys.exit(3)

try:
    # Module to calculate the stability index
    from CalculationModule.StabilityIndexCalculator import StabilityIndexCalculator
except ModuleNotFoundError:
    logging.error("CalculationModule.StabilityIndex not installed. Exiting...")
    sys.exit(3)

try:
    # Module to interact with the end user
    import CommunicationModule.userinterface as userinterface
except ModuleNotFoundError:
    logging.error(
        "CommunicationModule.userinterface not installed. Exiting...")
    sys.exit(3)

try:
    # Package to handle paths from Windows and Linux
    from pathlib import Path
except ModuleNotFoundError:
    logging.error("pathlib.Path module not installed. Exiting...")
    sys.exit(3)

try:
    # Package to interact with subversion and the subversion server
    from svn.remote import RemoteClient
except ModuleNotFoundError:
    logging.error("svn not installed. Exiting...")
    sys.exit(3)

try:
    # Function to remove a directory with all files
    from shutil import rmtree
except ModuleNotFoundError:
    logging.error("shutil not installed. Exiting...")
    sys.exit(3)


###################################################################################################
###                                global variables                                             ###
###################################################################################################

###################################################################################################
###                                public classes                                               ###
###################################################################################################

###################################################################################################
###                                private classes                                              ###
###################################################################################################

###################################################################################################
###                                public functions                                             ###
###################################################################################################

###################################################################################################
###                                private functions                                            ###
###################################################################################################

###################################################################################################
###                                main                                                         ###
###################################################################################################
def main():
    """
        Main function
    """

    # Parse console arguments
    args = userinterface.parseArguments()
    pathOld = args.oldVersion
    pathNew = args.newVersion
    debug = args.debug
    visual = args.visual
    svn = args.svn
    startThreshold = args.threshold
    thresholdDivisor = args.divisor
    minThreshold = args.minThreshold

    # Initialize the logging interface
    userinterface.initialize_logger(debug=debug)

    try:
        logging.debug("Script start...")

        # Checkout the svn folders
        if svn:
            logging.info("SVN Mode activate")
            print("Checkout old software version with path {}".format(pathOld))
            svn_oldVersion = RemoteClient(pathOld)
            pathOld = Path.cwd().joinpath("oldVersion")
            svn_oldVersion.checkout(str(pathOld))
            
            print("Checkout new software version with path {}".format(pathNew))
            svn_newVersion = RemoteClient(pathNew)
            pathNew = Path.cwd().joinpath("newVersion")
            svn_newVersion.checkout(str(pathNew))
            
            print("Checkout successful")

        # Create the object of stability index calculation engine
        stabilityIndex = StabilityIndexCalculator(
            visual=visual, startThreshold=startThreshold, thresholdDivisor=thresholdDivisor, minThreshold=minThreshold)
        
        # Calculation of stability index
        stabilityIndex.calcStabilityIndex(Path(pathOld), Path(pathNew))
        stabilityIndex.printStabilityIndex()

        # Delete checkout svn folders
        if svn:
            rmtree(pathOld, ignore_errors=False,onerror=handleRemoveReadonly)
            print("Remove folder with code of old software version")
            
            rmtree(pathNew, ignore_errors=False,onerror=handleRemoveReadonly)
            print("Remove folder with code of new software version")

        logging.debug("Script stop...")
    except Exception as e:
        logging.error("Anything was wrong! - {}".format(e))
        raise

def handleRemoveReadonly(func, path, exc):
    excvalue = exc[1]
    if func in (os.rmdir, os.unlink, os.remove) and excvalue.errno == errno.EACCES:
        os.chmod(path, stat.S_IRWXU| stat.S_IRWXG| stat.S_IRWXO) # 0777
        func(path)
    else:
        logging.error("Can not removed: {}".format(path))

# All top-level code must be contained in main() function. This prevents
# the execution of this code when the module is imported by another one.
# When called using 'python xxx.py', main() is executed automatically.
if __name__ == "__main__":
    main()
