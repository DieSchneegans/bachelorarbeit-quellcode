"""
    Module SourceBase
"""

###################################################################################################
###                                imports                                                      ###
###################################################################################################
import sys

try:
    # Package to create logging messages
    import logging
except ModuleNotFoundError:
    print("logging not installed. Exiting...")
    sys.exit(3)
    
try:
    # Class with operations for files
    from FileModule.SourceFile import SourceFile
except ModuleNotFoundError:
    logging.error("FileModule.SourceFile.SourceFile not installed. Exiting...")
    sys.exit(3)

try:
    # Class to handle paths
    from pathlib import Path
except ModuleNotFoundError:
    logging.error("pathlib.Path module not installed. Exiting...")
    sys.exit(3)

try:
    # Class to measure the execution time
    from de.kspg.svn.stats.ExecTimer import ExecTimer
except ModuleNotFoundError:
    logging.error("de.kspg.svn.stats.ExecTimer not installed. Exiting...")
    sys.exit(3)

###################################################################################################
###                                global variables                                             ###
###################################################################################################

###################################################################################################
###                                public classes                                               ###
###################################################################################################


class SourceBase:
    """
        Operation for a source base
    """

    def __init__(self, visual:bool, rootPath:Path="") -> None:
        """
            Constructor
        """

        self.__visual = visual
        self.__rootPath = rootPath
        self.__files = []

    def getFiles(self) -> list:
        """
            Return all founded files
        """
        return self.__files

    def __addFile(self, path:Path) -> None:
        """
            Create SourceFile object and save this in a list
        """
        sourceFile = SourceFile(path)
        self.__files.append(sourceFile)

    def searchFiles(self) -> list:
        """
            Search all files in folder tree
        """
        logging.debug("SourceBase - searchFiles")

        path = Path(self.__rootPath)
        
        # Is only file
        if path.is_file():
            if path.suffix == ".c" or path.suffix == ".h":
                self.__addFile(path)
            else:
                logging.error("The inserted file is not allowed. The file has {} as suffix. Supported suffixes: .c, .h".format(path.suffix))
        
        else:
            # C-Files
            for filePath in path.glob("**/*.c"):
                self.__addFile(filePath)
    
            # H-Files
            for filePath in path.glob("**/*.h"):
                self.__addFile(filePath)
                
        return self.__files

    def tokenizeFiles(self) -> list:
        """
            Tokenize all files of this source base
        """
        if self.__visual:
            timer = ExecTimer("files", len(self.__files))
            print(
                "--------------------\n tokenizeFiles\n--------------------"
            )
            timer.Start()

        counter = 0
        tokens = []
        for file in self.__files:
            counter += 1

            file.readFile()
            tokens.extend(file.tokenize())

            if not (counter % 5) and self.__visual is None:
                timer.Lap(counter)

        if self.__visual:
            timer.Stop(counter)
            print("--------------------")

        return tokens

    def joinFiles(self) -> str:
        """
            Read all files and create a big string with the whole content
        """
        content = []
        for file in self.__files:
            content.append(file.readFile())

        return "".join(content)


###################################################################################################
###                                private classes                                              ###
###################################################################################################

###################################################################################################
###                                public functions                                             ###
###################################################################################################

###################################################################################################
###                                private functions                                            ###
###################################################################################################

###################################################################################################
###                                main                                                         ###
###################################################################################################
