"""
    Module SourceFile
"""

###################################################################################################
###                                imports                                                      ###
###################################################################################################
import sys

try:
    # Package to create logging messages
    import logging
except ModuleNotFoundError:
    print("logging not installed. Exiting...")
    sys.exit(3)

try:
    # Function from the tokenizer to tokenize a string and class of types of the token values
    from sctokenizer import tokenize_str, TokenType
except ModuleNotFoundError:
    logging.error("sctokenizer not installed. Exiting...")
    sys.exit(3)

try:
    # Subclass of Token
    from CalculationModule.HashableToken import HashableToken
except ModuleNotFoundError:
    logging.error("CalculationModule.HashableToken not installed. Exiting...")
    sys.exit(3)
    
try:
    from pathlib import Path
except ModuleNotFoundError:
    logging.error("pathlib not installed. Exiting...")
    sys.exit(3)

###################################################################################################
###                                global variables                                             ###
###################################################################################################

###################################################################################################
###                                public classes                                               ###
###################################################################################################


class SourceFile:
    """
        Operation for files
    """

    def __init__(self, path:Path="") -> None:
        """
            Create object variable
        """
        self.__path :Path= path
        self.__text :str= None
        self.__tokens :list= None

    def getFilePath(self) -> Path:
        """
            Return the file path
        """
        return self.__path
    
    def getFileName(self) -> str:
        """
            Return the file name
        """
        return self.__path.name

    def readFile(self) -> str:
        """
            Read the file text and return this
            If the file text is already read then return the file text without reading
        """
        if not self.__text:
            file = self.__open()
            self.__text = self.__read(file)
            self.__close(file)

        return self.__text

    def __open(self, mode:str="r"):
        """
            Open the file
        """
        if not self.__path.is_file():
            logging.error(
                "The path is not from a file. Please check your path")
            sys.exit(4)

        return open(self.__path, mode)

    def __close(self, file) -> None:
        """
            Close the file
        """
        file.close()

    def __read(self, file) -> str:
        """
            Read the file
        """
        return file.read()

    def overwriteFile(self, text:str) -> None:
        """
            Overwrite the file text with a new text
        """
        file = self.__open("w")
        self.__write(file, text)
        self.__close(file)
        file = self.__open("r")
        self.__text = self.__read(file)
        self.__close(file)

    def appendFile(self, text:str) -> None:
        """
            Append the file text with a new text
        """
        file = self.__open("a")
        self.__write(file, text)
        self.__close(file)
        file = self.__open("r")
        self.__fileText = self.__read(file)
        self.__close(file)

    def __write(self, file, text:str) -> None:
        """
            Write the file
        """
        file.write(text)

    def tokenize(self) -> list:
        """
            Tokenize the file text
        """
        if not self.__text:
            self.readFile()

        if self.__tokens is None:
            self.__tokens = tokenize_str(source_str=self.__text, lang="c")
            self.__removeComments()

            index = 0
            while index < len(self.__tokens):
                self.__tokens[index] = HashableToken(
                    self.__tokens[index].token_value, self.__tokens[index].token_type, self.__tokens[index].line, self.__tokens[index].column)

                index += 1

        return self.__tokens

    def __removeComments(self) -> None:
        """
            Remove CommentSymbols from token list
        """
        i = 0
        while i < len(self.__tokens):
            if self.__tokens[i].token_type == TokenType.COMMENT_SYMBOL:
                self.__tokens.pop(i)
                i -= 1
            i += 1


###################################################################################################
###                                private classes                                              ###
###################################################################################################

###################################################################################################
###                                public functions                                             ###
###################################################################################################

###################################################################################################
###                                private functions                                            ###
###################################################################################################

###################################################################################################
###                                main                                                         ###
###################################################################################################
